cmake_minimum_required(VERSION 2.8.12)

find_package(SofaFramework REQUIRED)

sofa_add_plugin(plugins/ContactMapping ContactMapping)
sofa_add_plugin(plugins/Anatomy Anatomy)

sofa_install_git_version( "sofa-anatomy-statistics" ${CMAKE_CURRENT_SOURCE_DIR} )

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidAffineFem

def createSceneAndController(rootNode):
    model = SofaPython.sml.Model(SofaPython.Tools.localPath( __file__, "skin_vertebra.xml"))
    
    global scene
    scene = Anatomy.sml.rigidAffineFem.ScenePrepare(rootNode, model, SofaPython.Tools.localPath( __file__, "data_sml_rigidAffineFem"))
    
    # images
    scene.param.voxelSize=4e-3
    scene.param.coarseningFactor=1
    scene.param.exportShapeFunction=False

    # nodes
    scene.param.nodeDensity = 200
    scene.param.samplingMethod='global'
    scene.param.sampleSkinSurfaceRatherThanVolume = False

    # fem
    scene.param.femCoarseningFactor=4
    scene.param.mapBoundaryFEM=True
    scene.param.gridType = 'branching'

    # material
    scene.param.GPDensity = 1
    scene.param.GPType = "332"
    scene.param.GPMinNumber = 1

    # visu
    rootNode.createObject('VisualStyle', displayFlags='showBehaviorModels showVisual')
    rootNode.createObject('BackgroundSetting',color='1 1 1')
    scene.param.showRigid=True
    scene.param.showRigidScale=0.1
    scene.param.showAffine=True
    scene.param.showAffineScale=0.05
    scene.param.showHexa=True
    scene.param.showRigidVertex=True
    scene.param.showRigidVertexScale=5e-3
    scene.param.showGPScale=0 #3e-3

    scene.createScene()

def bwdInitGraph(node):
    scene.export()
    return node
import os.path

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidAffine
import Anatomy.Tools

def createSceneAndController(node):
    
    model = SofaPython.sml.Model(SofaPython.Tools.localPath( __file__, "cylinder.xml"))
    
    global scene
    scene = Anatomy.sml.rigidAffine.SceneSimu(node, model, SofaPython.Tools.localPath( __file__, "data_sml"))

    scene.param.showRigid=True
    scene.param.showRigidScale=0.1
    scene.param.showOffset=True
    scene.param.showOffsetScale=0.05
    
    scene.param.GPType = "332"

    scene.createScene()
    node.createObject("VisualStyle", displayFlags="showVisualModels hideCollisionModels showBehaviorModels")
    node.createObject('CompliantAttachButtonSetting' )
    node.createObject('CompliantImplicitSolver', name='odesolver',stabilization=1)
    node.createObject('SequentialSolver', name="numsolver", iterations =250, precision=1e-14)
    #node.createObject('MinresSolver', name='numsolver', iterations='250', precision='1e-14')
    # node.createObject('LDLTSolver', name='numsolver')
    node.findData('gravity').value='0 0 0'

    node.createObject('BackgroundSetting',color='1 1 1')
    
    scene.nodes["dofAffine"].createObject("FixedConstraint", indices=2)
    
    #scene.dagValidation() # not mandatory, silent if no pb

    return node

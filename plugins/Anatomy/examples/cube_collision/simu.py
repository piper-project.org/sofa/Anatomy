import os.path

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidAffine

def createScene(node):
    
    model = SofaPython.sml.Model(os.path.join(os.path.dirname(__file__), "cube_collision.xml"))
    scene = Anatomy.sml.rigidAffine.SceneSimu(node, model, "data_sml")

    scene.param.showRigid=True
    scene.param.showRigidScale=0.02
    scene.param.showOffset=True
    scene.param.showOffsetScale=0.02
    
    scene.param.GPType = "332"
    
    scene.param.collisionIsSymmetric = True
    
    scene.createScene()
    
    node.createObject('CompliantAttachButtonSetting' )
    node.createObject('CompliantImplicitSolver', name='odesolver',stabilization=1)
    node.createObject('SequentialSolver', name="numsolver", iterations =250, precision=1e-14)
    node.findData('gravity').value='0 0 -10'
    
    scene.rigids["vertebra_1"].node.createObject('FixedConstraint')
    
    #scene.joints["vertebra_arch_joint"].addDamper(0.01)
    
    scene.dagValidation() # not mandatory, silent if no pb

    return node
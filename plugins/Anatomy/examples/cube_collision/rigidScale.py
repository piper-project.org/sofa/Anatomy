import Sofa

import SofaPython.sml
import Anatomy.sml.rigidScale

def createScene(node):
    
    model = SofaPython.sml.Model(SofaPython.Tools.localPath( __file__, "cube_collision.xml"))
    scene = Anatomy.sml.rigidScale.SceneArticulatedRigidScale(node, model)
    
    # settings
    scene.param.voxelSize = 0.005

    # visual settings
    scene.param.showRigid=True
    scene.param.showRigidScale=0.05
    scene.param.showOffset=True
    scene.param.showOffsetScale=0.01
    scene.param.showRigidDOFasSphere=False
    
    scene.createScene()
    
    node.createObject('CompliantAttachButtonSetting' )
    node.createObject('CompliantImplicitSolver', stabilization=1)
    node.createObject('SequentialSolver', iterations=50, precision=1E-15, iterateOnBilaterals=1)
    node.createObject('LDLTResponse', schur=0)
    node.findData('gravity').value='0 0 0'
    
    scene.bones["vertebra_1"].rigidNode.createObject('FixedConstraint', fixAll=0, indices='0')
    scene.bones["vertebra_1"].scaleNode.createObject('FixedConstraint', fixAll=0, indices='0')
    
    return node
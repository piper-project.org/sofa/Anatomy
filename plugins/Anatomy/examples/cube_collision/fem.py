import os.path

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidFem
from SofaPython.Tools import listToStr as concat

def createScene(rootNode):
    
    model = SofaPython.sml.Model(os.path.join(os.path.dirname(__file__), "cube_collision.xml"))
    scene = Anatomy.sml.rigidFem.Scene(rootNode, model)

    scene.param.showRigid=True
    scene.param.showRigidScale=0.02
    scene.param.showOffset=True
    scene.param.showOffsetScale=0.02

    scene.param.voxelSize=20E-3
    scene.param.coarseningFactor=2
    scene.param.showHexa =True
    
    scene.param.behaviorLaw = 'hooke'
    scene.param.gridType='branching'

    scene.param.contactCompliance=1E-5
    
    scene.param.collisionIsSymmetric = True
    
    scene.createScene()

    rootNode.findData('gravity').value='0 0 -10'
    rootNode.findData('dt').value='0.01'
    
#    rootNode.createObject('EulerImplicitSolver', name='odesolver')
#    rootNode.createObject('CGLinearSolver', name='numsolver', iterations='25', tolerance='1e-14', threshold='1e-14')

    rootNode.createObject('CompliantImplicitSolver', name='odesolver',stabilization=1)
    rootNode.createObject('SequentialSolver', name="numsolver", iterations =250, precision=1e-14)

    scene.rigids["vertebra_1"].node.createObject('FixedConstraint')

#    scene.dagValidation() # not mandatory, silent if no pb

    return rootNode


import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0

import SofaBasics 1.0

SofaSceneInterface {
    id: root
    
    toolpanel: ColumnLayout {
        enabled: sofaScene.ready
        anchors.fill: parent
        
//         Button {
//             text: "reset display"
//             onClicked: { 
//                 updateDisplayGToH(); 
//                 updateDisplayWToB(); 
//             }
//         }
        
        function updateTargetWToA() {
            sofaScene.sofaPythonInteractor.call(
                "script", "setTarget", "W_to_A",
                [translationWToA.vx, translationWToA.vy, translationWToA.vz, 
                rotationWToA.vx*Math.PI/180, rotationWToA.vy*Math.PI/180, rotationWToA.vz*Math.PI/180]
                //                [rotationWToA.vx*Math.PI/180]
//                 [translationWToA.vx, translationWToA.vy]
            );
                // translationWToA.vy, translationWToA.vz]);

        }
        
        function updateDisplayWToA() {
            var target = sofaScene.sofaPythonInteractor.call("script", "getTarget", "W_to_A");
            translationWToA.vx = target[0];
            translationWToA.vy = target[1];
            translationWToA.vz = target[2];
            rotationWToA.vx = target[3]*180/Math.PI;
            rotationWToA.vy = target[4]*180/Math.PI;
            rotationWToA.vz = target[5]*180/Math.PI;
        }
        
        Component.onCompleted: {
            updateDisplayWToA();
            updateDisplayWToB();
        }

        Connections {
            target: sofaScene
            onReadyChanged: { 
                if (ready) { 
                    updateDisplayWToA(); 
                    updateDisplayWToB(); 
                } 
            }
        }
        
        GroupBox {
            Layout.fillWidth: true
            title: "W to A - 3 tanslations, 3 rotations"
            ColumnLayout {
                anchors.fill: parent
                Vector3DSpinBox {
                    id: translationWToA
                    Layout.fillWidth: true
                    decimals: 1
                    stepSize: 0.1
                    onVxChanged: updateTargetWToA()
                    onVyChanged: updateTargetWToA()
                    onVzChanged: updateTargetWToA()
                }
                Vector3DSpinBox {
                    id: rotationWToA
                    Layout.fillWidth: true
                    decimals: 0
                    stepSize: 10
                    onVxChanged: updateTargetWToA()
                    onVyChanged: updateTargetWToA()
                    onVzChanged: updateTargetWToA()
                }
//                 Button {
//                     text: "Update"
//                     onClicked: updateTargetWToA()
//                 }
            }
        }
        
        function updateTargetWToB() {
            sofaScene.sofaPythonInteractor.call(
                "script", "setTarget", "W_to_B",
                [translationWToB.value]);
        }
        
        function updateDisplayWToB() {
            var target = sofaScene.sofaPythonInteractor.call("script", "getTarget", "W_to_B");
            translationWToB.value = target[0];
//             rotationGToJ.value = target[1]*180/Math.PI;
        }
        GroupBox {
            Layout.fillWidth: true
            title: "W to B - 1 tanslation"
            ColumnLayout {
                anchors.fill: parent
                SpinBox {
                    id: translationWToB
                    Layout.fillWidth: true
                    minimumValue: -1000
                    maximumValue: 1000
                    decimals: 1
                    stepSize: 0.1
                    onValueChanged: updateTargetWToB()
                }
//                 SpinBox {
//                     id: rotationGToJ
//                     Layout.fillWidth: true
//                     minimumValue: -1000
//                     maximumValue: 1000
//                     decimals: 0
//                     stepSize: 10
//                     onValueChanged: updateTargetWToB()
//                 }
            }   
        }
    }
}

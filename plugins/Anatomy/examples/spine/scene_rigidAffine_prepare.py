import os.path

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidAffine


def createSceneAndController(rootNode):
    
    model = SofaPython.sml.Model(SofaPython.Tools.localPath( __file__, "spine.xml"))

    global scene
    scene = Anatomy.sml.rigidAffine.ScenePrepare(rootNode, model, SofaPython.Tools.localPath( __file__, "data_sml"))
    
    scene.param.voxelSize=5E-3
    scene.param.coarseningFactor=2
    
    scene.param.nodeDensity = 200
    scene.param.GPDensity = 1
    scene.param.GPType = "332"
    
    scene.param.samplingMethod='global'

    rootNode.createObject('VisualStyle', displayFlags='showBehaviorModels showVisual')
    scene.param.showRigid=True
    scene.param.showRigidScale=0.05
    
    scene.param.showAffine=True
    scene.param.showAffineScale=0.02
    
    scene.createScene()
    
    #scene.dagValidation() # not mandatory, silent if no pb
    
    return rootNode

def bwdInitGraph(node):
    scene.export()
    return node
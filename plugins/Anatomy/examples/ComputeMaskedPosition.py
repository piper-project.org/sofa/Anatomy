import Sofa

def createScene(rootNode):
    rootNode.createObject("RequiredPlugin", pluginName="Compliant")
    rootNode.createObject("RequiredPlugin", pluginName="Anatomy")
    
    dofNode = rootNode.createChild("dofs")
    dofNode.createObject("MechanicalObject", template="Vec3", name="dofs", position="1 2 3 5 6 7")
    
    maskNode = dofNode.createChild("mask")
    maskNode.createObject("MechanicalObject", template="Vec1", name="dofs")
    maskNode.createObject("MaskMapping", name="mapping", dofs="1 0 1 0 1 1")
    
    targetNode = maskNode.createChild("target")
    targetNode.createObject("MechanicalObject", template="Vec1", name="dofs")
    targetNode.createObject("DifferenceFromTargetMapping", name="mapping", targets="1.5 3.5 6.5 7.5")
    
    targetFullPositionNode = targetNode.createChild("targetFullPosition")
    targetFullPositionNode.createObject("ComputeMaskedPosition", template="Vec3,Vec1", name="computeTargetFullPosition",
                                        position="@../../../dofs.position",
                                        mask="@../../mapping.dofs", maskPosition="@../mapping.targets")
    targetFullPositionNode.createObject("MechanicalObject", template="Vec3", name="dofs", position="@computeTargetFullPosition.output")
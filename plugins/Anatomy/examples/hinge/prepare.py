import os.path

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidAffine

def createSceneAndController(rootNode):
    
    model = SofaPython.sml.Model(os.path.join(os.path.dirname(__file__), "hinge.sml"))
    global scene
    scene = Anatomy.sml.rigidAffine.ScenePrepare(rootNode, model, "data_sml")
    
    scene.param.voxelSize=4E-3
    scene.param.coarseningFactor=1
    
    scene.param.nodeDensity = 0
    scene.param.GPDensity = 1
    scene.param.GPType = "332"
    
    scene.param.samplingMethod='global'
    scene.param.sampleSkinSurfaceRatherThanVolume = False

    scene.param.showRigid=True
    scene.param.showRigidScale=0.02
    scene.param.showAffine=True
    scene.param.showAffineScale=0.02
    
    scene.createScene()
    
    scene.dagValidation() # not mandatory, silent if no pb
    
    return rootNode

def bwdInitGraph(node):

    scene.export()
    
    return node
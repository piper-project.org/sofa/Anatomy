import os.path

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidAffine

def createScene(node):
    
    model = SofaPython.sml.Model(os.path.join(os.path.dirname(__file__), "hinge.sml"))
    scene = Anatomy.sml.rigidAffine.SceneSimu(node, model, "data_sml")

    scene.param.showRigid=True
    scene.param.showRigidScale=0.02
    scene.param.showOffset=True
    scene.param.showOffsetScale=0.02
    
    scene.param.GPType = "332"
    
    scene.param.contactCompliance = 1E-5
    
    scene.createScene()
    
    node.createObject('CompliantAttachButtonSetting' )
    node.createObject('CompliantImplicitSolver', name='odesolver',stabilization=1)
    node.createObject('LDLTSolver', name='numsolver')
    node.findData('gravity').value='-10 0 0'
    
    scene.rigids["femur"].node.createObject('FixedConstraint')
    scene.slidingContacts["Surface_link__tibia__femur"].compliance.isCompliance='0'
    
    scene.dagValidation() # not mandatory, silent if no pb

    return node
import os.path

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidFem
from SofaPython.Tools import listToStr as concat

def createScene(rootNode):
    
    model = SofaPython.sml.Model(os.path.join(os.path.dirname(__file__), "hinge.sml"))
    scene = Anatomy.sml.rigidFem.Scene(rootNode, model)

    scene.param.voxelSize=5E-3
    scene.param.coarseningFactor=2
    scene.param.showHexa =True
    
    scene.param.behaviorLaw = 'hooke'
    scene.param.gridType='branching'

    scene.param.contactCompliance=1E-5
    scene.createScene()

    rootNode.findData('gravity').value='-10 0 0'
    rootNode.findData('dt').value='0.01'
    
#    rootNode.createObject('EulerImplicitSolver', name='odesolver')
#    rootNode.createObject('CGLinearSolver', name='numsolver', iterations='25', tolerance='1e-14', threshold='1e-14')

    rootNode.createObject('CompliantImplicitSolver', name='odesolver',stabilization=1)
    rootNode.createObject('LDLTSolver', name='numsolver')
#    rootNode.createObject('CgSolver', name='numsolver', iterations='30', precision='1e-10' )

    scene.rigids["femur"].node.createObject('FixedConstraint')
    scene.slidingContacts["Surface_link__tibia__femur"].compliance.isCompliance='0'

#    scene.dagValidation() # not mandatory, silent if no pb

    return rootNode


import os.path
import timeit

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidAffineFem


def createSceneAndController(rootNode):
    
    global executionTime
    executionTime = timeit.default_timer()
    
    model = SofaPython.sml.Model(SofaPython.Tools.localPath( __file__, "left_leg.xml"))

    global scene
    scene = Anatomy.sml.rigidAffineFem.ScenePrepare(rootNode, model, SofaPython.Tools.localPath( __file__, "data_sml_rigidAffineFem"))

    # images
    scene.param.voxelSize=5E-3
    scene.param.coarseningFactor=1
    scene.param.exportShapeFunction=False

    # nodes
    scene.param.nodeDensity = {"default": 200}
    scene.param.samplingMethod='global'

    # fem
    scene.param.femCoarseningFactor=6
    scene.param.mapBoundaryFEM=True
    scene.param.gridType = 'branching'

    # material
    scene.param.GPDensity = 1
    scene.param.GPType = "332"
    scene.param.GPMinNumber = 1

    # visu
    rootNode.createObject('VisualStyle', displayFlags='showBehaviorModels showVisual')
    rootNode.createObject('BackgroundSetting',color='1 1 1')
    scene.param.showRigid=True
    scene.param.showRigidScale=0.1
    scene.param.showAffine=True
    scene.param.showAffineScale=0.05
    scene.param.showHexa=True
    scene.param.showRigidVertex=True
    scene.param.showRigidVertexScale=5e-3
    scene.param.showGPScale=0 #3e-3

    scene.createScene()

    # scene.dagValidation() # not mandatory, silent if no pb

    return rootNode

def bwdInitGraph(node):
    scene.export()
    print "prepare timing:", timeit.default_timer()-executionTime
    return node
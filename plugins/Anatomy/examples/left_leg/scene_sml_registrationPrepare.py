import os.path

import Sofa

import SofaPython.sml
import Anatomy.sml.affineRegistration

def createSceneAndController(rootNode):
    
    model = SofaPython.sml.Model(SofaPython.Tools.localPath( __file__, "left_leg.xml"))
    global scene
    scene = Anatomy.sml.affineRegistration.ScenePrepare(rootNode, model, SofaPython.Tools.localPath( __file__, "data_sml_registration"))
    
    scene.param.voxelSize=10E-3
    scene.param.nodeDensity = 200
    scene.param.GPDensity = 10000
    scene.param.GPType = "331"

    scene.param.showRigid=True
    scene.param.showRigidScale=0.1
    
    scene.createScene()
    
    scene.dagValidation() # not mandatory, silent if no pb
    
    return rootNode

def bwdInitGraph(node):

    scene.export()
    
    return node
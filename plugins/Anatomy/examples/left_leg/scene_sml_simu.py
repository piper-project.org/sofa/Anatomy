import os.path

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidAffine
import Anatomy.Tools

def createSceneAndController(node):
    
    model = SofaPython.sml.Model(SofaPython.Tools.localPath( __file__, "left_leg.xml"))
    global scene
    scene = Anatomy.sml.rigidAffine.SceneSimu(node, model, SofaPython.Tools.localPath( __file__, "data_sml"))

    scene.param.loadShapeFunction = True # required to map the big_toe

    scene.param.showRigid=True
    scene.param.showRigidScale=0.1
    scene.param.showOffset=True
    scene.param.showOffsetScale=0.05
    
    scene.param.collisionNbClosestVertex = 10
    scene.param.collisionOffset = 0E-3
    scene.param.collisionCompliance=1E-15
    ## deactivating contacts (to test bilateral and static solvers)
    # for c in scene.model.surfaceLinks.values():
    #     if "sliding" in c.tags or "collision" in c.tags:
    #         del scene.model.surfaceLinks[c.id]

    scene.createScene()
    node.createObject("VisualStyle", displayFlags="showVisualModels hideCollisionModels showBehaviorModels")
    node.createObject('CompliantAttachButtonSetting' )
    node.createObject('CompliantImplicitSolver', name='odesolver',stabilization=1)
    # node.createObject('CompliantPseudoStaticSolver', name='odesolver', iterations="1")
    node.createObject('SequentialSolver', name="numsolver", iterations =250, precision=1e-14)
    #node.createObject('MinresSolver', name='numsolver', iterations='250', precision='1e-14')
    # node.createObject('LDLTSolver', name='numsolver')
    node.gravity = [0,0,0]
    scene.rigids["l_femur"].node.createObject('FixedConstraint')
    scene.collisionContacts["Collision__l_femur__lig"].ignoreInitialPenetratedVertex()
    
    scene.addMappedPoint("big_toe", [0.057, 0.013, 0.134], ["skin"])

    node.createObject('BackgroundSetting',color='1 1 1')
    
    # scene.collisionContacts["Collision__l_femur__l_tibia"].node.createObject("DataEngineMonitor", name="l_femur__l_tibia", engines="@ClosestPointEngine @projectionToMeshEngine @closestKeepNSmallestEngine", printLog=True) ## engines debugging
    
    #scene.dagValidation() # not mandatory, silent if no pb

    return node

def onEndAnimationStep(dt):
    print "max cartesian velocity:", Anatomy.Tools.getMaxCartesianVelocity2(rigidDof=scene.nodes["dofRigid"].getObject("dofs"), affineDof=scene.nodes["dofAffine"].getObject("dofs"))
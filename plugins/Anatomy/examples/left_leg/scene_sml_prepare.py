import os.path
import timeit

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidAffine


def createSceneAndController(rootNode):
    
    global executionTime
    executionTime = timeit.default_timer()
    
    model = SofaPython.sml.Model(SofaPython.Tools.localPath( __file__, "left_leg.xml"))

    global scene
    scene = Anatomy.sml.rigidAffine.ScenePrepare(rootNode, model, SofaPython.Tools.localPath( __file__, "data_sml"))
    
    scene.param.voxelSize=4E-3
    scene.param.coarseningFactor=2
    
    scene.param.nodeDensity = {"default": 200}
    scene.param.GPDensity = 1
    scene.param.GPType = "332"
    
    scene.param.samplingMethod='local'
    scene.param.sampleSurfaceRatherThanVolume = set("skin")
    
    scene.param.exportShapeFunction = True

    rootNode.createObject('VisualStyle', displayFlags='showBehaviorModels showVisual')
    scene.param.showRigid=False
    scene.param.showRigidScale=0.1
    
    scene.param.showAffine=False
    scene.param.showAffineScale=0.05
    scene.param.showLabelImage = False
    scene.param.showShapeFunctionImage = True
    
    scene.createScene()
    
    #scene.dagValidation() # not mandatory, silent if no pb
    
    return rootNode

def bwdInitGraph(node):
    scene.export()
    print "prepare timing:", timeit.default_timer()-executionTime
    return node

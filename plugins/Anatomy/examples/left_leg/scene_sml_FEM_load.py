import os.path

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidFem

usePrefactorizedSystem = True

def createScene(rootNode):
    
    model = SofaPython.sml.Model(SofaPython.Tools.localPath( __file__, "left_leg.xml"))
    scene = Anatomy.sml.rigidFem.Scene(rootNode, model)
    
    scene.param.showRigid=True
    scene.param.showRigidScale=0.1

    scene.param.voxelSize=30E-3
    scene.param.coarseningFactor=1
    scene.param.showHexa =True
    
    scene.param.behaviorLaw = 'hooke'
    scene.param.gridType='branching'
    
    scene.param.collisionNbClosestVertex = 10
    
    scene.loadScene("data_sml_rigidFem")
    
    rootNode.findData('gravity').value='0 0 0'
    rootNode.findData('dt').value='0.1'
    
    rootNode.createObject('BackgroundSetting',color='1 1 1')
    
    #    rootNode.createObject('EulerImplicitSolver', name='odesolver')
    #    rootNode.createObject('CGLinearSolver', name='numsolver', iterations='25', tolerance='1e-14', threshold='1e-14')
    
    rootNode.createObject('CompliantImplicitSolver', name='odesolver',stabilization=0)
    rootNode.createObject('SequentialSolver', name="numsolver", iterations =250, precision=1e-14)
#    rootNode.createObject('LDLTSolver', name='numsolver')
    #rootNode.createObject('CgSolver', name='numsolver', iterations='30', precision='1e-10' )

    scene.rigids["l_femur"].node.createObject('FixedConstraint')
    
    scene.dagValidation() # not mandatory, silent if no pb
    
    return rootNode



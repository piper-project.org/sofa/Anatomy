import os.path
import math

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidAffine
import Anatomy.Constraint

def createScene(node):
    
    model = SofaPython.sml.Model(SofaPython.Tools.localPath( __file__, "left_leg.xml"))
    scene = Anatomy.sml.rigidAffine.SceneSimu(node, model, SofaPython.Tools.localPath( __file__, "data_sml"))
    
    scene.param.contactCompliance = 1e-3

    scene.param.showRigid=True
    scene.param.showRigidScale=0.1
    scene.param.showOffset=True
    scene.param.showOffsetScale=0.05
    
    scene.param.GPType = "332"
    
    scene.createScene()
    
    node.createObject('CompliantAttachButtonSetting' )
    node.createObject('CompliantImplicitSolver', name='odesolver',stabilization=1)
    node.createObject('MinresSolver', name='numsolver', iterations='250', precision='1e-14')
    node.findData('gravity').value='0 0 0'
    scene.rigids["l_femur"].node.createObject('FixedConstraint')

    node.createObject('VisualStyle', displayFlags='showBehaviorModels showVisual')
    node.createObject('BackgroundSetting',color='1 1 1')
    
    node.createObject('RequiredPlugin', pluginName="Anatomy")
    
    kneeFlexion = Anatomy.Constraint.RigidRigidRelativeRotation("Knee_flexion", scene.rigids["l_femur"], scene.rigids["l_tibia"], [0,0,0,0,1,0])
    kneeFlexion.setTarget([math.radians(90)])
    
    #scene.dagValidation() # not mandatory, silent if no pb

    return node
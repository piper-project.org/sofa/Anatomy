import os.path

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidAffineFem

def createScene(rootNode):
    
    model = SofaPython.sml.Model(SofaPython.Tools.localPath( __file__, "left_leg.xml"))
    scene = Anatomy.sml.rigidAffineFem.SceneSimu(rootNode, model, SofaPython.Tools.localPath( __file__, "data_sml_rigidAffineFem"))

    scene.param.loadShapeFunction=False

    # material
    scene.param.behaviorLaw = 'hooke' # 'projective'
    scene.param.strainMeasure="Corotational" # 'Green'
    scene.param.useStrainOffset = False

    scene.param.behaviorLaw_FEM = 'projective' # 'hooke'
    scene.param.strainMeasure_FEM="Corotational" # 'Green'
    scene.param.useStrainOffset_FEM = False

    # collision
    scene.param.collisionNbClosestVertex = 10

    # visu
    rootNode.createObject("VisualStyle", displayFlags="showVisualModels hideCollisionModels showBehaviorModels")
    rootNode.createObject('BackgroundSetting',color='1 1 1')
    scene.param.showRigid=True
    scene.param.showRigidScale=0.1
    scene.param.showOffset=True
    scene.param.showOffsetScale=0.05

    scene.param.showHexa=True
    scene.param.unidirectionalCoupling = True

    scene.createScene()

    scene.rigids["l_femur"].node.createObject('FixedConstraint')

    rootNode.createObject('CompliantAttachButtonSetting' )
    rootNode.gravity='0 0 -5'
    rootNode.dt=0.05

    # solver
    if scene.param.unidirectionalCoupling:
        scene.nodes['Frames'].createObject('CompliantImplicitSolver', name='odesolver',stabilization=1)
        scene.nodes['Frames'].createObject('SequentialSolver', name="numsolver", iterations =25, precision=1e-14)

        # scene.nodes['FEM'].createObject('CompliantImplicitSolver', name='odesolver',stabilization=1)
        # scene.nodes['FEM'].createObject('CompliantPseudoStaticSolver', name='odesolver',iterations=1)
        scene.nodes['FEM'].createObject('ConstantCompliantPseudoStaticSolver', name='odesolver',iterations =5)
        # scene.nodes['FEM'].createObject('EulerImplicit', name='odesolver')
        # scene.nodes['FEM'].createObject('LDLTSolver', name="numsolver")
        scene.nodes['FEM'].createObject('MinresSolver', name="numsolver", iterations =25, precision=1e-14)
        # scene.nodes['FEM'].createObject('CGLinearSolver', name='odesolver',iterations="25", threshold="1E-5", tolerance="1E-5")
    else:
        rootNode.createObject('CompliantImplicitSolver', name='odesolver',stabilization=1)
        rootNode.createObject('SequentialSolver', name="numsolver", iterations =25, precision=1e-14)
        #rootNode.createObject('MinresSolver', name='numsolver', iterations='250', precision='1e-14')


    #scene.dagValidation() # not mandatory, silent if no pb

    return rootNode
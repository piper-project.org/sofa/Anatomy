<?xml version="1.0" ?>
<!DOCTYPE sml SYSTEM "sml.dtd">
<sml name="cutting">
    <units length="dm" mass="kg" time="s"/>
    <!--Meshes-->
    <mesh id="Cylinder_mesh">
        <source format="obj">./obj/cutting_Cylinder_mesh.obj</source>
    </mesh>
    <mesh id="Plane_mesh">
        <source format="obj">./obj/cutting_Plane_mesh.obj</source>
    </mesh>
    <!-- Sml solid -->
    <solid id="Cylinder">
        <tag>skin</tag>
        <position>0.000000 0.000000 0.000000 0.000000 0.000000 0.000000 1.000000</position>
        <mesh id="Cylinder_mesh"/>
        <mesh id="Plane_mesh" simulation="False">
            <tag>cut</tag>
        </mesh>
    </solid>
</sml>

import os.path
import timeit

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidAffine


def createSceneAndController(rootNode):
    
    global executionTime
    executionTime = timeit.default_timer()
    
    model = SofaPython.sml.Model(SofaPython.Tools.localPath( __file__, "cutting.sml"))

    global scene
    scene = Anatomy.sml.rigidAffine.ScenePrepare(rootNode, model, SofaPython.Tools.localPath( __file__, "data_sml"))
    
    scene.param.voxelSize=10E-3
    scene.param.coarseningFactor=1
    
    scene.param.nodeDensity = 60
    scene.param.GPDensity = 1
    scene.param.GPType = "332"
    
    scene.param.samplingMethod='global'
    scene.param.sampleSkinSurfaceRatherThanVolume = False

    rootNode.createObject('VisualStyle', displayFlags='showBehaviorModels showVisual')
    scene.param.showRigid=True
    scene.param.showRigidScale=0.1
    
    scene.param.showAffine=True
    scene.param.showAffineScale=0.05
    
    scene.createScene()
    
    #scene.dagValidation() # not mandatory, silent if no pb
    
    return rootNode

def bwdInitGraph(node):
    scene.export()
    print "prepare timing:", timeit.default_timer()-executionTime
    return node
import os.path

import Sofa

import SofaPython.sml
import Anatomy.sml.rigidAffine
import Anatomy.Tools

def createScene(node):
    
    model = SofaPython.sml.Model(SofaPython.Tools.localPath( __file__, "cutting.sml"))
    global scene
    scene = Anatomy.sml.rigidAffine.SceneSimu(node, model, SofaPython.Tools.localPath( __file__, "data_sml"))

    scene.param.showRigid=True
    scene.param.showRigidScale=0.1
    scene.param.showOffset=True
    scene.param.showOffsetScale=0.05
    
    scene.param.GPType = "332"
    
    scene.param.collisionNbClosestVertex = 10

    ## deactivating contacts (to test bilateral and static solvers)
    # for c in scene.model.surfaceLinks.values():
    #     if "sliding" in c.tags or "collision" in c.tags:
    #         del scene.model.surfaceLinks[c.id]

    scene.createScene()
    node.createObject("VisualStyle", displayFlags="showVisualModels hideCollisionModels showBehaviorModels")
    node.createObject('CompliantAttachButtonSetting' )
    node.createObject('CompliantImplicitSolver', name='odesolver',stabilization=1)
    # node.createObject('CompliantPseudoStaticSolver', name='odesolver', iterations="1")
    node.createObject('SequentialSolver', name="numsolver", iterations =250, precision=1e-14)
    #node.createObject('MinresSolver', name='numsolver', iterations='250', precision='1e-14')
    # node.createObject('LDLTSolver', name='numsolver')
    node.findData('gravity').value='0 0 0'
    node.findData('dt').value='0.1'
    scene.nodes["dofAffine"].createObject('FixedConstraint', indices=0)

    node.createObject('BackgroundSetting',color='1 1 1')
    
    #scene.dagValidation() # not mandatory, silent if no pb

    return node

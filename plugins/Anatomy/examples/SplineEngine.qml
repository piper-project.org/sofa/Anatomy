import QtQuick 2.0
import SofaBasics 1.0
import SofaApplication 1.0
import SofaInteractors 1.0
import SofaManipulators 1.0

SofaSceneInterface {
    id: root
    
    // current selected particle
    property int currentIndex: -1
    
    property Component selectSplineKnot: UserInteractor_MoveCamera {

        hoverEnabled: true

        function init() {
            
            moveCamera_init();
            
            addMousePressedMapping(Qt.LeftButton, function(mouse, sofaViewer) {
                var selectableParticle = sofaViewer.pickParticleWithTags(Qt.point(mouse.x + 0.5, mouse.y + 0.5), ["spline"]);
                
                if(selectableParticle && selectableParticle.sofaComponent) {
                    sofaScene.selectedComponent = selectableParticle.sofaComponent;
                    root.currentIndex = selectableParticle.particleIndex;
                }
                else {
                    var selectable = sofaViewer.pickObject(Qt.point(mouse.x + 0.5, mouse.y + 0.5));
                    if(selectable && selectable.manipulator) {
                        sofaScene.selectedManipulator = selectable.manipulator;
                        if(sofaScene.selectedManipulator.mousePressed)
                            sofaScene.selectedManipulator.mousePressed(mouse, sofaViewer);
                        if(sofaScene.selectedManipulator.mouseMoved)
                            setMouseMovedMapping(sofaScene.selectedManipulator.mouseMoved);
                    }
                    else {
                        sofaScene.selectedComponent = null;
                        sofaScene.selectedManipulator = null;
                        root.currentIndex = -1;
                    }
                }
            });

        addMouseReleasedMapping(Qt.LeftButton, function(mouse, sofaViewer) {
            if(sofaScene.selectedManipulator && selectedManipulator.mouseReleased)
                selectedManipulator.mouseReleased(mouse, sofaViewer);

            sofaScene.selectedManipulator = null;

            setMouseMovedMapping(null);
        });
        }
    }
    
    Component {
        id: manipulatorComponent

        Manipulator {
            id: manipulator

            property var sofaScene
            property int index: -1
            
            visible: manipulator.index===root.currentIndex

            Manipulator3D_Translation {
                id: tx
                visible: manipulator.visible
                axis: "x"
                onPositionChanged: manipulator.position = position;
            }

            Manipulator3D_Translation {
                id: ty
                visible: manipulator.visible
                axis: "y"
                onPositionChanged: manipulator.position = position;
            }

            Manipulator3D_Translation {
                id: tz
                visible: manipulator.visible
                axis: "z"
                onPositionChanged: manipulator.position = position;
            }
            
            function getPosition() {
                var positionArray = sofaScene.sofaPythonInteractor.call("knotsController", "getPosition", index);
                position = Qt.vector3d(positionArray[0], positionArray[1], positionArray[2]);
            }

            function setPosition() {
                sofaScene.sofaPythonInteractor.call("knotsController", "setPosition", index, [position.x, position.y, position.z]);
            }
            
            Component.onCompleted: getPosition()

            onPositionChanged: setPosition()
        }
    }
    
    Component.onCompleted: {
        for(var i = 0; i < sofaScene.sofaPythonInteractor.call("knotsController", "getNbKnots"); ++i)
            sofaScene.addManipulator(manipulatorComponent.createObject(root, {sofaScene: sofaScene, index: i, visible: Qt.binding(function () {return this.index === root.currentIndex;})}));
        SofaApplication.addInteractor("Select spline knot", selectSplineKnot);
    }
}
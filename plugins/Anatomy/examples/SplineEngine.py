
import Sofa
import SofaPython.Tools


nbControlPoints = 10
ds=0.01

nbInitPoints = 10

def createScene(rootNode):
    # interactive spline
    controlPoints=list()
    for i in range(0,nbControlPoints):
        controlPoints.append([i%2, 0, i])
    
    rootNode.createObject('RequiredPlugin', pluginName="Anatomy")
    rootNode.createObject('PythonScriptController', name="knotsController", filename=__file__, classname="KnotsController")
    
    spline1Node = rootNode.createChild("spline1")
    global spline1
    spline1 = spline1Node.createObject("SplineEngine", template="Vec3", name="spline", controlPoints=SofaPython.Tools.listListToStr(controlPoints), ds=ds)
    spline1Node.createObject("MeshTopology", name="topology", position="@spline.position", edges="@spline.edges", drawEdges=True)
    
    spline1CtrlpNode = spline1Node.createChild("spline1Ctrlp")
    spline1CtrlpNode.createObject("MechanicalObject", template="Vec3", name="dofs", tags="spline", position="@../spline.controlPoints", showObject=True, drawMode=1, drawObjectScale=0.1)

    # initialized spline
    splineInitNode = rootNode.createChild("spline_init")
    
    initPoints=list()
    for i in range(0,nbInitPoints):
        initPoints.append([i%2, 2, i])
    splineInitNode.createObject("MechanicalObject", template="Vec3", name="initPoints", position=SofaPython.Tools.listListToStr(initPoints), showObject=True, drawMode=1, drawObjectScale=0.1, showColor="1 0 0 1")
    
    splineNode = splineInitNode.createChild("spline")
    
    splineNode.createObject("SplineEngine", template="Vec3", name="spline", useBezier=True, initSpline=SofaPython.Tools.listListToStr(initPoints), ds=ds)
    splineNode.createObject("MeshTopology", name="topology", position="@spline.position", edges="@spline.edges", drawEdges=True)
    splineNode.createObject("MechanicalObject", template="Vec3", name="ctrlp", position="@spline.controlPoints", showObject=True, drawMode=1, drawObjectScale=0.1)
    
    return rootNode


class KnotsController(Sofa.PythonScriptController) :
    
    def getNbKnots(self):
        return len(spline1.controlPoints)
    
    def setPosition(self, index, position):
        allPosition = spline1.controlPoints
        allPosition[index]=position
        spline1.controlPoints=SofaPython.Tools.listListToStr(allPosition)
        
    def getPosition(self, index):
        return spline1.controlPoints[index]
import Sofa

import Compliant.Rigid
import Anatomy.SceneTools

def createScene(rootNode):
    rootNode.createObject('RequiredPlugin', pluginName="Compliant")
    A = Compliant.Rigid.Frame([1,2,3,0,0,0,1])
    print "A", str(A)
    nodeA = rootNode.createChild("A")
    dofsA = A.insert(nodeA, name="dofs")
    dofsA.showObject = True
    dofsA.showObjectScale = 1 
    
    nodeB = nodeA.createChild("B")
    dofsB = Anatomy.SceneTools.insertAttachedFrame(nodeB, attachedToRigid="@/A/dofs", index=0, frame=[5,6,7,0,0,0,1], isAbsolute=False)
    dofsB.showObject = True
    dofsB.showObjectScale = 1
    
    nodeC = nodeA.createChild("C")
    dofsC = Anatomy.SceneTools.insertAttachedFrame(nodeC, attachedToRigid="@/A/dofs", index=0, frame=[5,6,7,0,0,0,1], isAbsolute=True)
    dofsC.showObject = True
    dofsC.showObjectScale = 1
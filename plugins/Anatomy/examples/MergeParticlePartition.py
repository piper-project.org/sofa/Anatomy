import Sofa

def createScene(rootNode):
    rootNode.createObject("RequiredPlugin", pluginName="Anatomy")
    
    A = rootNode.createChild("A")
    A.createObject('MechanicalObject', template="Vec1d", position="1 2 3")
    
    B = rootNode.createChild("B")
    B.createObject('MechanicalObject', template="Vec1d", position="7 8 9")
    
    C = A.createChild("C")
    B.addChild(C)
    C.createObject('MechanicalObject', template="Vec1d")
    C.createObject('MergeParticlePartition', name="merge", partitionSize=2, setIndices1="3 4 5", setIndices2="0 1 2")
    C.createObject('SubsetMultiMapping', template="Vec1d,Vec1d", name="mapping", input="@/A/. @/B/.", output="@.", indexPairs="@merge.outIndexPairs")
    
    C_missing = A.createChild("C_missing")
    B.addChild(C_missing)
    C_missing.createObject('MechanicalObject', template="Vec1d")
    C_missing.createObject('MergeParticlePartition', name="merge_missing", partitionSize=2, setIndices1="4 5 6", setIndices2="0 1 2")
    C_missing.createObject('SubsetMultiMapping', template="Vec1d,Vec1d", name="mapping", input="@/A/. @/B/.", output="@.", indexPairs="@merge_missing.outIndexPairs")
    
    C_duplicated = A.createChild("C_duplicated")
    B.addChild(C_duplicated)
    C_duplicated.createObject('MechanicalObject', template="Vec1d")
    C_duplicated.createObject('MergeParticlePartition', name="merge_duplicated", partitionSize=2, setIndices1="3 4 3", setIndices2="0 1 2")
    C_duplicated.createObject('SubsetMultiMapping', template="Vec1d,Vec1d", name="mapping", input="@/A/. @/B/.", output="@.", indexPairs="@merge_duplicated.outIndexPairs")
    
    
    
    
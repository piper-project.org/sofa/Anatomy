import os.path
#from lxml import etree
import xml.etree.ElementTree as etree

class AnatomyModelReader:
    """ Convenient class to get information from anatomyModel.xml files.
    Entities name follow FMA (Foundation Model for Anatomy)
    TODO: support simple reading without schema
    check to replace {http://www.inria.fr} with {*}, it *should* work
    """

    def setAnatomyModel(self, anatomyModelFile, anatomyModelXsd=None):
        self.entities = {}
        if not anatomyModelXsd is None:
            with open(anatomyModelXsd,'r') as f:
                anatomyModelSchema = etree.XMLSchema(etree.parse(f))

        with open(anatomyModelFile,'r') as f:
            self.rootPath = os.path.dirname(os.path.abspath(anatomyModelFile))
            anatomyModelTree = etree.parse(f)
            if not anatomyModelXsd is None:
                anatomyModelSchema.assertValid(anatomyModelTree)
            for entity in anatomyModelTree.iter('{http://www.inria.fr}entity'):
                if entity.find('{http://www.inria.fr}name')== None:
                    continue
                name = entity.find('{http://www.inria.fr}name').text
                e = {}
                e['mesh'] = entity.find('{http://www.inria.fr}mesh').text
                e['texture'] = entity.find('{http://www.inria.fr}texture').text
                e['roi']={}
                for roi in entity.findall('{http://www.inria.fr}roi'):
                    e['roi'][roi.find('{http://www.inria.fr}name').text]=roi.find('{http://www.inria.fr}vertices').text
                self.entities[name]=e
        #print self.entities

    def check(self):
        """ check existance of mesh and texture files
        """
        ok = True
        for k in self.entities.iterkeys():
            if not os.path.exists(self.getMesh(k)):
                print k, "mesh not found", self.getMesh(k)
                ok = False
            if not os.path.exists(self.getTexture(k)):
                print k, "texture not found", self.getTexture(k)
                ok = False
        return ok

    def hasEntity(self, name):
        return name in self.entities

    def getMesh(self, name):
        """mesh full path
        TODO: add support for different file format (obj, x3d,...)
        """
        return os.path.join(self.rootPath, self.entities[name]['mesh'])

    def getTexture(self, name):
        """texture full path
        """
        return os.path.join(self.rootPath, self.entities[name]['texture'])

    def getRoi(self, name, roi):
        """return roi list of vertices
        None if it does not exist
        """
        if roi in self.entities[name]['roi']:
            return self.entities[name]['roi'][roi]
        else:
            return None

import os.path 

import xml.etree.ElementTree as etree

import Anatomy.Tools

class Landmark:
    def __init__(self, mesh, indices, position=None):
        # mesh from where the landmark is extracted
        self.mesh=mesh
        # list of indices defining the landmark
        self.indices=indices
        # 3D position of the landmark [x y z]
        self.position=position

    def __str__(self):
        return str([self.mesh,self.indices,self.position])

class LandmarkSet:
    """ A class to store and process anatomical landmarks
    """
    def __init__(self):
        self.landmarks={}
        self.meshPath=""

    def setMeshPath(self, path):
        self.meshPath = path

    def add(self, name, mesh, indices, position=None):
        """ Add landmark name, as vertices in index list from mesh
        """
        self.landmarks[name] = Landmark(os.path.join(self.meshPath, mesh), indices, position)

    def fillPositions(self, doFitSphere=True, doReplacePosition=False):
        cacheVertices={}
        for name,landmark in self.landmarks.iteritems() :
            if landmark.position is None or doReplacePosition:
                if not landmark.mesh in cacheVertices:
                    meshFormat = os.path.splitext(landmark.mesh)[1][1:]
                    if meshFormat=="obj":
                        vertices = {i:item for i,item in enumerate(Anatomy.Tools.readObjVertices(landmark.mesh))}
                    elif meshFormat=="vtu":
                        vertices = Anatomy.Tools.readVtuIdVertices(landmark.mesh)
                    elif meshFormat=="vtm":
                        vertices = Anatomy.Tools.readVtmIdVertices(landmark.mesh)
                    else:
                        assert False, "LandmarkSet::fillPositions Unknown mesh format"+meshFormat
                    cacheVertices[landmark.mesh]=vertices
                else:
                    vertices = cacheVertices[landmark.mesh]

                if len(landmark.indices) is 1:
                    if landmark.indices[0] in vertices:
                        landmark.position = vertices[landmark.indices[0]]
                    else:
                        print "WARNING: LandmarkSet::fillPositions cannot get position, landmark:", name
                elif (len(landmark.indices) > 1) and doFitSphere:
                    vs=[]
                    for i in landmark.indices:
                        if i in vertices:
                            vs.append(vertices[i])
                        else:
                            print "WARNING: LandmarkSet::fillPositions cannot get position, landmark:", name,"vertex:",i
                    if len(vs) > 3:
                        self.landmarks[name].position = Anatomy.Tools.fitSphere(vs)[0]
                    else:
                        print "WARNING: LandmarkSet::fillPositions not enough points to fit a sphere, landmark:", name


    def hasLandmark(self, name):
        return name in self.landmarks

    def getLandmark(self, name):
        """ Get landmark name
        """
        return self[name]

    def __getitem__(self,name):
        assert name in self.landmarks, "invalid landmark: "+name
        return self.landmarks[name]

    def __str__(self):
        strDict={}
        for n,l in self.landmarks.iteritems():
            strDict[n]=str(l)
        return str(strDict)

def readXmlDataSet(xmlDataSetFile):
    """ Read landmarks from an xml file and return a LandmarkSet
    <dataset> and <roi> tags are used
    """
    resources={}
    ls=LandmarkSet()
    basePath = os.path.dirname(xmlDataSetFile)
    if not os.path.exists(xmlDataSetFile):
        print "ERROR: Landmark.readXmlDataSet: file not found", xmlDataSetFile
        return None
    with open(xmlDataSetFile,'r') as f:
        dataSetTree = etree.parse(f)
        # get resources
        for r in dataSetTree.iter('resource'):
            resources[r.find('name').text] = r.find('path').text
        # set resources full path
        for [r,p] in resources.iteritems():
            resources[r] = os.path.abspath(os.path.join(basePath,p))
            if not os.path.exists(resources[r]):
                print "WARNING: readXmlDataSet - resource:", r, "- file does not exists:", p
        for roi in dataSetTree.iter('roi'):
            name = roi.find('name').text
            mesh = resources[roi.attrib["resource"]]
            indices = map(int,roi.find('vertices').text.split())
            ls.add(name, mesh, indices)

    return ls



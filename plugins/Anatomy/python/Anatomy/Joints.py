import os.path
from numpy import *
import math

import Compliant.StructuralAPI
from SofaPython import Quaternion

import Anatomy.Tools
from Anatomy.CS_Bones import *

def eulerZXYToQuat(z,x,y):
    """ Convert euler angles to quaternion
    rotation z around Z axis, then x around X, and finally y around Y (moving axis)
    """
    qz = Quaternion.axisToQuat([0,0,1],z)
    qx = Quaternion.axisToQuat([1,0,0],x)
    qy = Quaternion.axisToQuat([0,1,0],y)
    return Quaternion.prod(qz, Quaternion.prod(qx,qy))

# joints database
DB={
"Right_hip":dict(type=Compliant.StructuralAPI.BallAndSocketRigidJoint, bone1=["Right_hip_bone", "Hip_bone"], offset1=CS_Right_Pelvis_Hip, bone2=["Right_femur"], offset2=CS_Right_Femur_Hip),
"Left_hip":dict(type=Compliant.StructuralAPI.BallAndSocketRigidJoint, bone1=["Left_hip_bone", "Hip_bone"], offset1=CS_Left_Pelvis_Hip, bone2=["Left_femur"], offset2=CS_Left_Femur_Hip),
"Right_glenohumeral":dict(type=Compliant.StructuralAPI.BallAndSocketRigidJoint, bone1=["Right_scapula"], offset1=CS_Right_Scapula, bone2=["Right_humerus"], offset2=CS_Right_Humerus),
"Left_glenohumeral":dict(type=Compliant.StructuralAPI.BallAndSocketRigidJoint, bone1=["Left_scapula"], offset1=CS_Left_Scapula, bone2=["Left_humerus"], offset2=CS_Left_Humerus),
}

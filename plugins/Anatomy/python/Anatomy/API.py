import SofaPython.Tools
import BranchingImage.API

def createJointBiasImage(parentNode, name, inputBiasImage, jointCenters, sphereRadius, biasInSphere):
    """ create a JointBiasImage component, returns the corresponding BranchingImage.API.Image
    """
    print "createJointBiasImage:", jointCenters, sphereRadius, biasInSphere
    inputImagePath = inputBiasImage.getImagePath()
    bi = BranchingImage.API.Image(parentNode, name, imageType=inputBiasImage.imageType)
    bi.node.createObject("JointBiasImage", template=inputBiasImage.template(), name="jointBias",
                         inputBiasImage="@"+inputImagePath+".image", imageTransform="@"+inputImagePath+".transform",
                         jointCenters=SofaPython.Tools.listListToStr(jointCenters), sphereRadius=sphereRadius, biasInSphere=biasInSphere )

    bi.image = bi.node.createObject("ImageContainer", template=bi.template, name="branchingImage", branchingImage="@jointBias.outputBiasImage", transform="@"+inputImagePath+".transform", drawBB="false")
    return bi

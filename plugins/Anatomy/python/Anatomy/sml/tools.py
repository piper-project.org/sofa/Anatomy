import os
import math
import sys

from SofaPython.Tools import listToStr as concat
import Sofa
import SofaPython.sml
import SofaPython.units

import ContactMapping.sml
import SofaImage.API
import SofaImage.Tools
import BranchingImage.API
import Flexible.API
import Anatomy.API
import Compliant.Frame

printLog = True

def sampleFromSolid_Image(parentNode, solid, nbNodes, image, fixedPosition=list()):
    """ sample a solid given a density and the image associated with the solid
    """
    Sofa.msg_info('Anatomy.sml','sampling '+str(nbNodes)+' nodes in '+solid.name+' volume')
    sampler = SofaImage.API.Sampler(parentNode)
    sampler.addImageSampler(image,nbNodes, fixedPosition=fixedPosition)
    return sampler if nbNodes != 0 else None

def sampleFromSolid_Mesh(parentNode, solid, nbNodes, image):
    """ sample a solid given a density and the meshes associated with the solid
        use image where meshes have been already loaded
        @todo: use geodesic distances by providing edges to MeshSampler ?
    """
    Sofa.msg_info('Anatomy.sml','sampling '+str(nbNodes)+' nodes on '+solid.name+' surface')

    # merge meshes
    args=dict()
    i=1
    for mesh in solid.mesh:
        if solid.meshAttributes[mesh.id].simulation is True:
            if mesh.id in image.meshes:
                m = image.meshes[mesh.id]
                args["position"+str(i)]="@"+SofaPython.Tools.getObjectPath(m.mesh)+".position"
#                args["triangles"+str(i)]="@"+SofaPython.Tools.getObjectPath(m.mesh)+".triangles"
#                args["quads"+str(i)]="@"+SofaPython.Tools.getObjectPath(m.mesh)+".quads"
                i+=1
    if i==1:
        return None

#    sample merged mesh
    if i==2:
        sampler = parentNode.createObject('MeshSampler', number=nbNodes, position=args['position1'])
    else :
        merge =  parentNode.createObject('MergeMeshes', name='mergedMesh', nbMeshes=len(args), **args )
        sampler = parentNode.createObject('MeshSampler', number=nbNodes, position="@mergedMesh.position")
    return sampler

def sampleFromModel_solidPerSolid(scene, additionalPosition=dict()):
    """ sample each solid according to a density and merge all samples
        compute offsets in label image
    """
    samplePaths = dict()
    i=1
    for solid in scene.model.getSolidsByTags(scene.param.softTissueTags):
        nbNodes = solid.getValueByTag(scene.param.numberOfNodes)
        if nbNodes is None:
            nbNodes = int( math.ceil(SofaPython.units.density_from_SI( solid.getValueByTag(scene.param.nodeDensity) ) * solid.volume ))
        if nbNodes > 0:
            if len(scene.param.sampleSurfaceRatherThanVolume & solid.tags)>0:
                s = sampleFromSolid_Mesh(scene.Images[solid.id].node, solid, nbNodes, scene.Images[solid.id])    #  sample surface mesh
                sPath = '@'+SofaPython.Tools.getObjectPath(s)+'.outputPosition' if s is not None else None
            else:
                s = sampleFromSolid_Image(scene.Images[solid.id].node, solid, nbNodes, scene.Images[solid.id])   #  sample volume image
                sPath = '@'+SofaPython.Tools.getObjectPath(s.sampler)+'.position' if s is not None else None
            if s is not None:
                samplePaths["position"+str(i)] = sPath
                if scene.labelImage.prefix=="Branching":
                    scene.cells.append(scene.Images[solid.id].node.createObject('BranchingCellOffsetsFromPositions', name='cell', template=scene.labelImage.template(), src=scene.labelImage.getImagePath(), position = sPath, labels=concat(scene.entitiesProp.entities[solid.id].allLabels())) )
                i+=1
    for solidId, positionList in additionalPosition.iteritems():
        if not solidId in scene.Images:
            Sofa.msg_warning("Anatomy.sml", "No image for solid {0}, additionnal affine positions are ignored".format(solidId))
            continue
        if len(positionList) == 0:
            continue
        meshObject = scene.Images[solidId].node.createObject('Mesh', name="additionalPosition", position=SofaPython.Tools.listListToStr(positionList))
        sPath = '@'+SofaPython.Tools.getObjectPath(meshObject)+'.position'
        samplePaths["position"+str(i)] = sPath
        if scene.labelImage.prefix=="Branching":
            scene.cells.append(scene.Images[solidId].node.createObject('BranchingCellOffsetsFromPositions', name='cell2', template=scene.labelImage.template(), src=scene.labelImage.getImagePath(), position = sPath, labels=concat(scene.entitiesProp.entities[solidId].allLabels())) )
        i+=1

    # create the Affine node, only if there is Affine
    if i == 1: # no mesh to sample
        scene.nodes["dofAffine"] = None
        return

    # merge nodes
    scene.createChild(scene.node, "dofAffine")
    merge = scene.nodes["dofAffine"].createObject('MergeMeshes', name='mergedSamples', nbMeshes=len(samplePaths), **samplePaths)
    scene.AffineDof = Flexible.API.AffineDof(scene.nodes["dofAffine"])
    scene.AffineDof.addMechanicalObject(src=merge, showObject=scene.param.showAffine, showObjectScale=SofaPython.units.length_from_SI(scene.param.showAffineScale))



def sampleFromModel_global(scene, fixedPosition, image):
    """ Sample young modulus image, given fixedPositions (rigid nodes).
    The additional positions are appended to the sampled positions.
    TODO: do we substract the number of additional positions to nbNodes ? for now no
    """
    if len(scene.param.sampleSurfaceRatherThanVolume)>0:
        Sofa.msg_warning('Anatomy.sml',"Parameter sampleSurfaceRatherThanVolume ignored for samplingMethod=='global'")
    nbNodes = 0
    for solid in scene.model.getSolidsByTags(scene.param.softTissueTags):
        nbNodesSolid = solid.getValueByTag(scene.param.numberOfNodes)
        if nbNodesSolid is None:
            nbNodesSolid = SofaPython.units.density_from_SI( solid.getValueByTag(scene.param.nodeDensity) ) * solid.volume
        nbNodes+=nbNodesSolid
    nbNodes = int(math.ceil(nbNodes))

    if printLog:
        Sofa.msg_info('Anatomy.sml','sampling '+str(nbNodes)+' nodes')

    # create the Affine node, only if there are nodes
    if nbNodes == 0 :
        scene.nodes["dofAffine"] = None
        return

    # sample the required nodes
    sampler = SofaImage.API.Sampler(scene.node, "dofAffine")
    scene.nodes["dofAffine"] = sampler.node
    sampler.addImageSampler(image, nbNodes, fixedPosition=fixedPosition)
    scene.AffineDof = Flexible.API.AffineDof(scene.nodes["dofAffine"])
    scene.AffineDof.addMechanicalObject(src=sampler.sampler, showObject=scene.param.showAffine, showObjectScale=SofaPython.units.length_from_SI(scene.param.showAffineScale))


def insertImageFromSolid(parentNode, solid, entitiesProp, voxelSize, density, youngModulus, voidMeshes=list()):
    """ build a regular image from sml solid, and add values in entity properties.
        all vertex groups are handled in different rois. Meshes without vertex group are closed with value "closing".
        inside value != surface value        @todo: add different youngModulus in surface layer
        all meshes in voidMeshes are painted black
     """
    im = SofaImage.API.Image(parentNode, solid.name, imageType="ImageUC") # unsigned char sufficient to cover all labels ? cf. warning in entitiesProp._generateLabel

    # add prop
    prop = entitiesProp.addEntity(solid.id, density=density, youngModulus=youngModulus)

    for mesh in solid.mesh:
        if solid.meshAttributes[mesh.id].simulation is True:
            # parse rois:
            roiIndices=list()
            roiValue = list()
            for name,item in mesh.group.iteritems():
                if 'toBePainted' in item.tags:
                    roiIndices.append(item.index)
                    roiValue.append(entitiesProp.addROI(solid.id, name))
            # add mesh
            if len(roiValue)!=0:
                im.addMeshLoader(mesh.source, value=prop.roi["surface"], insideValue=prop.roi["inside"], roiValue=roiValue, roiIndices=roiIndices, offset=solid.position, name=mesh.id)
            else:
                im.addMeshLoader(mesh.source, value=prop.roi["surface"], insideValue=prop.roi["inside"], closingValue=prop.roi["closing"], offset=solid.position, name=mesh.id)

    for m in voidMeshes:
        im.addExternMesh(m, value=0, insideValue=0)
    if len(voidMeshes):  # add the surface back to prevent from wrong mapping of the original surface mesh
        for mesh in solid.mesh:
            if solid.meshAttributes[mesh.id].simulation is True:
                # parse rois:
                roiIndices=list()
                roiValue = list()
                for name,item in mesh.group.iteritems():
                    if 'toBePainted' in item.tags:
                        roiIndices.append(item.index)
                        roiValue.append(entitiesProp.entities[solid.id].roi[name])
                # add mesh
                if len(roiValue)!=0:
                    im.addMeshLoader(mesh.source, value=prop.roi["surface"], roiValue=roiValue, roiIndices=roiIndices, name=mesh.id+'_surf')
                else:
                    im.addMeshLoader(mesh.source, value=prop.roi["surface"], closingValue=prop.roi["closing"], name=mesh.id+'_surf')
    # add cuts
    for mesh in solid.mesh:
        if not solid.meshAttributes[mesh.id].simulation is True:
            if 'cut' in solid.meshAttributes[mesh.id].tags:
                print mesh.id
                im.addMeshLoader(mesh.source, value='0', name=mesh.id+'_surf')


    im.addMeshToImage(voxelSize)
    # im.addViewer()
    # im.addClosingVisual(color=[1.,0.,0.,1.])
    # im.addMeshVisual()
    return im




def insertBehavior(solid, parentNode, density, nbMin=1,  type="331", shapeFunction=None, dofRigidNode=None, dofAffineNode=None, labelImage=None, labels=None, **kwargs):
    """ insert behavior model and sample Gauss points
    density in local model units
    """
    nb = int(math.ceil(solid.volume*density))
    if nb<nbMin:
        nb=nbMin
    Sofa.msg_info('Anatomy.sml','sampling '+str(nb)+' Gauss Points in '+solid.name)
    behavior = Flexible.API.Behavior(parentNode, 'behavior_'+solid.name, type=type, labelImage=labelImage, labels=labels)
    behavior.addGaussPointSampler(shapeFunction, nb, **kwargs)
    behavior.addMechanicalObject(dofRigidNode, dofAffineNode)
    return behavior

def loadBehavior(solid, parentNode, directory, dofRigidNode=None, dofAffineNode=None, labelImage=None, labels=None, **kwargs):
    """ insert behavior model and load Gauss points
    """
    if labelImage is None:
        behavior = Flexible.API.Behavior(parentNode, 'behavior_'+solid.name)
    else:
        behavior = Flexible.API.Behavior(parentNode, 'behavior_'+solid.name, labelImage=labelImage, labels=labels)
    behavior.read(directory=directory, **kwargs)
    behavior.addMechanicalObject(dofRigidNode, dofAffineNode)
    return behavior

def insertFEMBehavior(name, mesh, parentNode, dofFEMNode=None, **kwargs):
    """ insert behavior model and sample Gauss points
    """
    behavior = Flexible.API.Behavior(parentNode, 'behavior_'+name, type="331")
    behavior.addTopologyGaussPointSampler(mesh, **kwargs)
    behavior.addMechanicalObject(dofAffineNode=dofFEMNode)
    return behavior

def insertDeformable(solid, parentNode, dofRigidNode=None, dofAffineNode=None, labelImage=None, labels=None, useGlobalIndices=False, color=[1,1,1,1]):
    """ insert deformables + visual for each mesh of this solid
    """
    collisions=dict()
    visuals=dict()

    for mesh in solid.mesh:
        if solid.meshAttributes[mesh.id].collision is True:
            deformable = Flexible.API.Deformable(parentNode,'collision_'+mesh.name)
            deformable.loadMesh(mesh.source, offset = solid.position, triangulate=True)
            deformable.addMechanicalObject()
            deformable.addMapping(dofRigidNode,dofAffineNode, labelImage=labelImage, labels=labels, useGlobalIndices=useGlobalIndices)
            if solid.meshAttributes[mesh.id].visual is True:
                visuals[mesh.id] = deformable.addVisual(color)
            collisions[mesh.id] = deformable
        elif solid.meshAttributes[mesh.id].visual is True:
            deformable = Flexible.API.Deformable(parentNode,'visual_'+mesh.name)
            # deformable.loadMesh(mesh.source, offset = solid.position, triangulate=True)
            # deformable.addVisual(color)
            deformable.loadVisual(mesh.source, offset = solid.position, color=color) # to improve quad shading, we use a triangulated version instead
            deformable.addMapping(dofRigidNode,dofAffineNode, labelImage=labelImage, labels=labels, useGlobalIndices=useGlobalIndices, isMechanical=False)
            visuals[mesh.id] = deformable

    return (collisions,visuals)

def insertSpring(solid, parentNode, dofRigidNode=None, dofAffineNode=None, labelImage=None, indexLabelPairs=None, useGlobalIndices=False, radius=0.01, color=[1,1,1,1]):
    """ insert deformables + visual for each mesh of this solid
        @todo: change color according to extension
    """
    collisions=dict()
    visuals=dict()
    for mesh in solid.mesh:
        if solid.meshAttributes[mesh.id].collision is True:
            deformable = Flexible.API.Deformable(parentNode,'collision_'+mesh.name)
            deformable.loadMesh(mesh.source, offset = solid.position, triangulate=True)
            deformable.addMechanicalObject()
            deformable.addMapping(dofRigidNode,dofAffineNode, labelImage=labelImage, labels=indexLabelPairs, useGlobalIndices=useGlobalIndices, useIndexLabelPairs=True)
            # if solid.meshAttributes[mesh.id].visual is True:
            #     visuals[mesh.id] = deformable.addVisualCylinder(radius, color) # this component only works under dofs
            #     # visuals[mesh.id] =deformable.addVisual(color) # edge link does not work..
            collisions[mesh.id] = deformable
        # el
        if solid.meshAttributes[mesh.id].visual is True:
            deformable = Flexible.API.Deformable(parentNode,'visual_'+mesh.name)
            if printLog:
                Sofa.msg_info('Anatomy.sml','insertSpring: add visual_'+mesh.name)
            deformable.loadMesh(mesh.source, offset = solid.position, triangulate=True)
            deformable.loadVisualCylinder(mesh.source, offset = solid.position, radius=radius, color=color)
            deformable.addMapping(dofRigidNode,dofAffineNode, labelImage=labelImage, labels=indexLabelPairs, useGlobalIndices=useGlobalIndices, useIndexLabelPairs=True, isMechanical=False)
            visuals[mesh.id] = deformable

    return (collisions,visuals)

def insertSpringBehavior(collision, material, enableLimits=False, assemble=True):
    """ insert a (biphasic) spring as a compliance on total extensions
        @todo: using total extensions instead of individual edge ones
    """
    # enode = collision.node.createChild('extension')
    # enode.createObject('MechanicalObject', template="Vec1")
    # enode.createObject('EdgeSetTopologyContainer', edges="@"+SofaPython.Tools.getObjectPath(collision.topology)+".edges")
    # enode.createObject('DistanceMapping')
        # <UniformCompliance name="ucomp" template="Vec1d" compliance="1" dampingRatio="0."  isCompliance="0" />

    youngModulus = SofaPython.units.elasticity_from_SI(material['youngModulus'])*SofaPython.units.area_from_SI(material['section'])
    if 'viscosity' in material:
        viscosity = SofaPython.units.elasticity_from_SI(material['viscosity'])*SofaPython.units.area_from_SI(material['section'])*SofaPython.units.time_from_SI(1)
    else:
        viscosity = 0

    if enableLimits and 'strainLimit' in material and 'youngModulus_hard' in material:
        strainlimit = material['strainLimit']
        youngModulus_hard = SofaPython.units.elasticity_from_SI(material['youngModulus_hard'])*SofaPython.units.area_from_SI(material['section'])
    else:
        strainlimit = 0
        youngModulus_hard = 0

    collision.node.createObject('BarycentricShapeFunction', template="ShapeFunctiond")
    fnode = collision.node.createChild('F')
    fnode.createObject('TopologyGaussPointSampler', name="sampler", inPosition="@"+SofaPython.Tools.getObjectPath(collision.topology)+".position", showSamplesScale="0", method="0", order="1")
    fnode.createObject('MechanicalObject', template="F311")
    fnode.createObject('LinearMapping', template="Vec3d,F311", assemble=assemble)
    enode = fnode.createChild('E')
    enode.createObject('MechanicalObject', name='strain',template="E311")
    enode.createObject('CorotationalStrainMapping', template="F311,E311", geometricStiffness="true", assemble=assemble)
    if youngModulus!=0:
        enode.createObject('HookeForceField', template="E311", youngModulus=youngModulus, poissonRatio="0", viscosity=viscosity, isCompliance='0', assemble=assemble) # suppose small stiffness
    if strainlimit!=0:
        enode2 = enode.createChild('limits')
        enode2.createObject('MechanicalObject', template="E311")
        enode2.createObject('RelativeStrainMapping', template="E311,E311", offset=strainlimit,  inverted="1", assemble=assemble)
        enode2.createObject('HookeForceField', template="E311", youngModulus=youngModulus_hard, poissonRatio="0", viscosity=viscosity, isCompliance='1', assemble=assemble) # suppose high stiffness -> compliance
        enode2.createObject('UnilateralConstraint')

    return enode

def computeVolumes(scene,tags):
    """ compute volumes of each tagged solid.
        volume is stored in mesh units
        Treat skin as a special case (substract the volume of others structures divided by the number of skin solids
        @todo: fix this as it is not exact (internal structures not in the skin, interior of skull, etc.)
    """

    totalNonSkinVolume = 0
    tagsNotSkin = tags - {"skin"}
    for solid in scene.model.getSolidsByTags(tagsNotSkin):
        if solid.massInfo is None:
            solid.volume = SofaPython.sml.computeRigidMassInfo(solid, 1.).mass
        else:
            solid.volume = solid.massInfo.mass / solid.massInfo.density
        totalNonSkinVolume += solid.volume

    skinSolids = scene.model.getSolidsByTags({"skin"})
    if len(skinSolids):
        totalNonSkinVolume/=len(skinSolids)
        for solid in skinSolids:
            if solid.massInfo is None:
                solid.volume = SofaPython.sml.computeRigidMassInfo(solid, 1.).mass - totalNonSkinVolume
            else:
                solid.volume = solid.massInfo.mass / solid.massInfo.density - totalNonSkinVolume

     # ouput volumes
#     for solid in scene.model.getSolidsByTags(tags):
#         Sofa.msg_info('Anatomy.sml',solid.name+" volume ="+str(solid.volume))




def insertImagesFromSolids(scene,tags,voxelSize):
    """ insert image for each tagged solid.
        Treat skin as a special case (removing of internal structures)
    """
    scene.createChild(scene.node, "image")

    # we tag a ROI to be painted, if they are used in an 'attached' surfaceLink
    for c in scene.model.getSurfaceLinksByTags({"attached"}):
        for i in xrange(2):
            roi = c.surfaces[i].group
            if not roi is None and not roi in {"surface", "inside", "closing"} :
                c.surfaces[i].mesh.group[roi].tags.add("toBePainted")

    tagsNotSkin = tags - {"skin"}

    for solid in scene.model.getSolidsByTags(tagsNotSkin):
        scene.Images[solid.id] = insertImageFromSolid(scene.nodes["image"], solid, scene.entitiesProp, SofaPython.units.length_from_SI(voxelSize), scene.material.density(scene.getMaterial(solid.id)), scene.material.youngModulus(scene.getMaterial(solid.id)) )

    voidMeshes = list()
    for im in scene.Images.values() :
        for m in im.meshes.values():
            voidMeshes.append(m.mesh)

    for solid in scene.model.getSolidsByTags({"skin"}):
        scene.Images[solid.id] = insertImageFromSolid(scene.nodes["image"], solid, scene.entitiesProp, SofaPython.units.length_from_SI(voxelSize), scene.material.density(scene.getMaterial(solid.id)), scene.material.youngModulus(scene.getMaterial(solid.id)), voidMeshes )


def connectImages(scene):
    """ Compute label connections in images given 'attached' links
        Treat skin as a special case (connected to all non-skin labels, and skin closings) when scene.param.autoConnectSkin is True
    """
    # attached contact
    for c in scene.model.getSurfaceLinksByTags({"attached"}):
        if c.surfaces[0].solid.id in scene.Images and c.surfaces[1].solid.id in scene.Images :
            scene.entitiesProp.connect( entity1 = c.surfaces[0].solid.id, entity2 = c.surfaces[1].solid.id, roi1 = c.surfaces[0].group , roi2 = c.surfaces[1].group )

    # Fat connected to everything else
    # warning: skins from different solids are only connected on closings
    for sid in scene.Images.keys():
#        Sofa.msg_info('Anatomy.sml',"solid: {0} roi: {1}".format(scene.model.solids[sid].name, scene.entitiesProp.entities[sid].roi))
        if "skin" in scene.model.solids[sid].tags:
            for n in scene.entitiesProp.entities.keys():
                if sid == n: # do not self connect
                    continue
                if "skin" in scene.model.solids[n].tags:
                    scene.entitiesProp.connect( entity1 = sid, entity2 =n, roi1 = "closing"  )
                elif scene.param.autoConnectSkin :
                    scene.entitiesProp.connectAll(sid, n)

                    # connect the internal fat only
                    # for roi2 in scene.entitiesProp.entities[n].roi.keys():
                    #     scene.entitiesProp.connect( entity1 = sid, entity2 =n, roi1 = "inside" , roi2 = roi2 )

#    Sofa.msg_info('Anatomy.sml',"getConnectionString: {0}".format(scene.entitiesProp.getConnectionString()))


def mergeImages(scene,tags,name,coarseningFactor=1, fuseLabels=False):
    """ compute an image by merging tagged images, given connections stored in entitiesProp
        subsampling can be done
    """
    imgs=list()
    for solid in scene.model.getSolidsByTags(tags):
        if solid.id in scene.Images:
            imgs.append(scene.Images[solid.id])
    if len(imgs)==0:
        return None

    im = BranchingImage.API.Image(scene.nodes["image"], name, imageType=imgs[0].imageType)
    im.fromImages(imgs , scene.entitiesProp.getConnectionString())
    if fuseLabels:
        # fuse labels to avoid superimposed labels from the same solids
        im.fromTransferFunction(im,scene.entitiesProp.getFusionMap())
    if coarseningFactor>1:
        # subsample branching image
        im.fromSubsampledImage(im,coarseningFactor)

    # merge into regular image ?
    # im = SofaImage.API.Image(scene.nodes["image"], name, imageType=imgs[0].imageType)
    # im.fromImages(imgs)

    return im


def computeSamplerBiasImage(scene,image):
    """ return biased image based on joints
    """
    jointCenters = list()
    for joint in scene.model.genericJoints.values():
        jointCenters.append(joint.offsets[0].value[0:3])

    if printLog:
        Sofa.msg_info('Anatomy.sml','insertSamplerBiasImage: jointCenters=', jointCenters)

    return Anatomy.API.createJointBiasImage(image.node, "bias", image,
        jointCenters,
        SofaPython.units.length_from_SI(scene.param.biasAffineSamplingWithJointsRadius),
        scene.param.biasAffineSamplingWithJointsBias)



def insertRegistrationFromModel(scene):
    """ Insert registration components
        Work in progress..
    """

    # collect the different sources of each target
    # @todo: allow a single source to have various targets ?
    # @todo: move some of these to Registration.API ?
    scene.registrations = dict()
    for r in scene.model.getSurfaceLinksByTags({"registration"}):

        source = scene.getCollision(r.surfaces[0].solid.id,r.surfaces[0].mesh.id)
        if not source:
            Sofa.msg_error('Anatomy.sml',r.surfaces[0].mesh.id+" should be a collision model")
            continue

        # add source sub mesh if necessary
        if r.surfaces[0].group:
            subset = Flexible.API.Deformable(source.node,'subset_'+r.surfaces[0].group)
            subset.subsetFromDeformable(source,r.surfaces[0].mesh.group[r.surfaces[0].group].index)
            source = subset

        key = (r.surfaces[1].solid.id,r.surfaces[1].mesh.id,r.surfaces[1].group) if r.surfaces[1].group else (r.surfaces[1].solid.id,r.surfaces[1].mesh.id)
        if key in scene.registrations:
            scene.registrations[key]['sources'].append(source)
        else:
            scene.registrations[key] = { 'sources':[source] , 'target':None}
            scene.registrations[key]['target'] = scene.getCollision(r.surfaces[1].solid.id,r.surfaces[1].mesh.id)

            # add target mesh if not existing
            if not scene.registrations[key]['target']:
                if not "Targets" in scene.nodes:
                    scene.createChild(scene.node, "Targets")
                target = Flexible.API.Deformable(scene.nodes["Targets"],r.surfaces[1].mesh.name)
                target.loadMesh(r.surfaces[1].mesh.source, offset = r.surfaces[1].solid.position,triangulate=True)
                target.loadVisual(r.surfaces[1].mesh.source, offset = r.surfaces[1].solid.position,color=scene.param.color["target"])
                target.addNormals()
                scene.registrations[key]['target'] = target

            # @todo: add target sub mesh if necessary

            # add normals
            if not scene.registrations[key]['target'].normals:
                scene.registrations[key]['target'].addNormals()

    for key,r in scene.registrations.iteritems():

        target = r['target']

        # get single source or merge several sources
        if len(r['sources']) == 1:
            source = r['sources'][0]
            source.addNormals()
        else:
            # merge sources
            source = Flexible.API.Deformable(scene.nodes["dof"],'source')
            source.fromDeformables(r['sources'])
            source.addNormals()
            r['sources'] = [ source ]

#        node = source.node.createChild("registration")
#        target.node.addChild(node)
#        node.createObject('MechanicalObject', template="Vec1d", name="dofs")
#        node.createObject('DistanceToMeshMultiMapping', template="Vec3d,Vec1d", name="mapping", input="@"+source.node.getPathName()+"/. @"+target.node.getPathName()+"/.", output="@.",  normals2="@"+SofaPython.Tools.getObjectPath(target.normals)+".normals",
##                            roi1=concat(object1Roi), roi2=concat(object2Roi),
#                            printLog=False, restLengths='0')
#        node.createObject('UniformCompliance', name='compliance', compliance=1./scene.param.ICPstiffness, isCompliance='0')

#        node = target.node.createChild("registration")
#        source.node.addChild(node)
#        node.createObject('MechanicalObject', template="Vec1d", name="dofs")
#        node.createObject('DistanceToMeshMultiMapping', template="Vec3d,Vec1d", name="mapping", input="@"+target.node.getPathName()+"/. @"+source.node.getPathName()+"/.", output="@.",  normals2="@"+SofaPython.Tools.getObjectPath(source.normals)+".normals",
##                            roi1=concat(object1Roi), roi2=concat(object2Roi),
#                            printLog=False, restLengths='0')
#        node.createObject('UniformCompliance', name='compliance', compliance=1./scene.param.ICPstiffness, isCompliance='0')


        # @todo add forcefield on target side in case of simulated target
        targetPositions = target.dofs if target.dofs else target.topology
        source.node.createObject('ClosestPointRegistrationForceField', name='ICP', template='Vec3d',
                                sourceTriangles='@'+source.topology.name+'.triangles' , sourceNormals='@'+source.normals.name+'.normals',
                                position='@'+SofaPython.Tools.getObjectPath(targetPositions)+'.position' ,  triangles='@'+SofaPython.Tools.getObjectPath(target.topology)+'.triangles', normals='@'+SofaPython.Tools.getObjectPath(target.normals)+'.normals',
                                stiffness=scene.param.ICPstiffness, blendingFactor=scene.param.blendingFactor, normalThreshold=scene.param.normalThreshold, outlierThreshold=scene.param.outlierThreshold,
                                rejectOutsideBbox='0', rejectBorders='1',
                                cacheSize='4',  damping='0', drawColorMap='0')

    # if "Targets" in scene.nodes:
    #     scene.nodes["Targets"].createObject('VisualStyle', displayFlags='showVisual showWireframe')


def insertSlidingContactsFromModel(scene):
    """
    add sliding contacts, given following parameters
    - contactCompliance
    - slidingContactRejectBoundary

    fixed parameters:
    - restlength = 0  -> no gap
    - useTangentPlaneProjectionMethod = True  -> approximate method
    - isCompliance=False ->  penalty method
    """
    for c in scene.model.getSurfaceLinksByTags({"sliding"}):
        scene.slidingContacts[c.id] = ContactMapping.sml.insertContact(c,scene,compliance=scene.param.contactCompliance, isCompliance=False, useTangentPlaneProjectionMethod=True, rejectBorder=scene.param.slidingContactRejectBoundary, parallel = scene.param.parallel)

def insertCollisionContactsFromModel(scene):
    """
    add collision contacts, given following parameters
    - collisionOffset -> to enforce a gap between objects
    - "symmetric" tag in contact -> to have object1->object2 and object2->object1 constraints
    - collisionNbClosestVertex -> to reduce the number of contacts

    fixed parameters:
    - useTangentPlaneProjectionMethod = False  -> exact method using point->primitive closest points
    - bilateral=False, isCompliance=True ->  unilateral constraint method with null compliance
    """
    contactArgs = { "bilateral": False,
                    "isCompliance": True,
                    "compliance": scene.param.collisionCompliance,
                    "restLength": SofaPython.units.length_from_SI(scene.param.collisionOffset),
                    "N":scene.param.collisionNbClosestVertex,
                    "useTangentPlaneProjectionMethod":False,
                    "useBV":True,
                    "parallel": scene.param.parallel
                    }
    if scene.param.collisionOffset > 0:
        contactArgs["offsetBV"]=SofaPython.units.length_from_SI(scene.param.collisionOffset)

    for c in scene.model.getSurfaceLinksByTags({"collision"}):
        if "symmetric" in c.tags:
            (scene.collisionContacts[c.id],scene.collisionContacts[c.id+"_inverted"]) = ContactMapping.sml.insertContact(c, scene, symmetricContact=True, **contactArgs)
        else:
            scene.collisionContacts[c.id] = ContactMapping.sml.insertContact(c, scene, symmetricContact=False, **contactArgs)


def addMeshExporters(scene,dir,ExportAtEnd=False):
    """ add obj Exporters for each visual model of the scene
    """
    if not os.path.exists(dir):
        os.makedirs(dir)
    for sid,s in scene.visuals.iteritems():
        for mid,m in s.iteritems():
            # WARNING: meshes shared accross solids will be exported once
            filename = os.path.join(dir, os.path.basename(scene.model.meshes[mid].source))
            e = m.node.createObject('ObjExporter',name='ObjExporter',filename=filename, printLog=True,exportAtEnd=ExportAtEnd)
            scene.meshExporters.append(e)

def getSpringAttachments(solid, scene):
    # returns a list of (spring point index,image label) pairs
    indexLabelPairs = []
    for c in scene.model.getSurfaceLinksByTags({"attached"}):
        if c.surfaces[0].solid.id == solid.id and not c.surfaces[0].group is None:
            roi = c.surfaces[0].mesh.group[c.surfaces[0].group].index
            labels = scene.entitiesProp.entities[c.surfaces[1].solid.id].allLabels()
            for r in roi:
                for l in labels:
                    indexLabelPairs.append( r )
                    indexLabelPairs.append( l )
    return indexLabelPairs


def insertLandmarksFromModel(scene):
    """ map landmarks group
    """
    if not hasattr(scene, 'Landmarks'):
        scene.Landmarks=dict()
    for sid,s in scene.model.solids.iteritems():
        for m in s.mesh:
            for gid,g in m.group.iteritems():
                if "landmark" in g.tags:
                    if gid in scene.Landmarks:
                        Sofa.msg_error("Anatomy.sml", "landmark "+gid+" already defined")
                    else :
                        col = scene.getCollision(sid,m.id)
                        if not col is None:
                            ldm = Flexible.API.Deformable(col.node,gid)
                            ldm.subsetFromDeformable(col,g.index)
                            scene.Landmarks[gid]=ldm
                        else:
                            Sofa.msg_error("Anatomy.sml", "landmark "+gid+" cannot be added. Mesh "+m.id+" is not a collision model")


def insertImagesFromModel(scene,addViewers=False, offset=None, scale=None ):
    """ insert regular images stored in sml model
        medical images are in mm ->  use scale=SofaPython.units.length_from_SI(1E-3))
    """
    if not hasattr(scene, 'SmlImages'): #warning: use a specific name, not to be mixed with rasterized solids
        scene.SmlImages=dict()
    imnode = scene.createChild(scene.node,'SmlImages')
    for id,i in scene.model.images.iteritems():
        if id in scene.SmlImages:
            Sofa.msg_error("Anatomy.sml", "image "+id+" already defined")
        else:
            imageType = None
            if i.format=="mhd":
                imageType=SofaImage.Tools.getImageType(i.source)
            if imageType is None:
                if printLog:
                    Sofa.msg_warning("Anatomy.sml", "imageType not known, use ImageS by default")
                imageType="ImageS"
            im = SofaImage.API.Image(imnode,id,imageType)
            im.addContainer(filename=os.path.basename(i.source), directory=os.path.dirname(os.path.abspath(i.source)))

            if not offset is None or not scale is None: # modify transform ?
                (dim,sc,off) = SofaImage.Tools.getImageTransform(i.source,scale)
                if not offset is None :
                    transformed = Compliant.Frame.Frame(offset) * Compliant.Frame.Frame(off)
                else :
                    transformed = Compliant.Frame.Frame(off)
                im.image.transform=SofaImage.Tools.transformToData(sc,transformed.offset())

            if addViewers:
                im.addViewer()
            scene.SmlImages[id]=im




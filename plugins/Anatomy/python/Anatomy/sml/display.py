import sys

import piper.anatomyDB

import Sofa
import SofaPython.Tools
import SofaPython.units
import SofaPython.sml
import SofaPython.MeshLoader
import Anatomy.Tools
import Anatomy.sml.joint

printLog=True

class Scene(SofaPython.sml.SceneDisplay):
    """ Creates a scene to display an anatomy model
    """
    def __init__(self,parentNode,model):
        SofaPython.sml.SceneDisplay.__init__(self,parentNode,model)
        
        self.frameFactory = piper.anatomyDB.FrameFactory.instance()
        
        # initialize common anatomical tag color
        self.param.colorByTag["bone"]="0.8 0.8 0.8 1."
        
        self.param.ignoreUnknownLandmark = True

        self.param.showLandmark=False
        self.param.showLandmarkScale=0.01 # m
        self.param.showLandmarkColor="0.8 0. 0. 1."
        
        self.param.showAnatomicalFrame=False
        self.param.showAnatomicalFrameScale=0.05 # m
        
    def createScene(self):
        SofaPython.sml.SceneDisplay.createScene(self)
        model=self.model
        meshCache = dict()
        if self.param.showLandmark or self.param.showAnatomicalFrame:
            self.frameFactory.landmark().clear()
            Anatomy.sml.joint.addAnatomicalLandmarkToFrameFactory(self.model, ignoreUnknownLandmark=self.param.ignoreUnknownLandmark)
            if self.param.showLandmark:
                # display landmarks using a MechanicalObject, not really meant for
                landmarks=[]
                for name in self.frameFactory.landmark().keys():
                    landmarks.append(self.frameFactory.landmark().get(name).flatten().tolist())
                landmarkNode = self.createChild(self.node, "landmark")
                landmarkNode.createObject("MechanicalObject", template="Vec3", name="landmark", position=SofaPython.Tools.listListToStr(landmarks), showObject=True, drawMode=1, showColor=self.param.showLandmarkColor, showObjectScale=SofaPython.units.length_from_SI(self.param.showLandmarkScale))

        if self.param.showAnatomicalFrame:
            # display frames using a MechanicalObject, not really meant for
            anatomicalFrames = []
            for frameName in self.frameFactory.registeredFrameList():
                try:
                    frame = self.frameFactory.computeFrame(frameName)
                except:
                    Sofa.msg_warning("Anatomy.sml.display","frame {0} cannot be computed - error: {1}".format(frameName, sys.exc_value))
                else:
                    anatomicalFrames.append(frame.toVec().flatten().tolist())
                    if printLog:
                        Sofa.msg_info("Anatomy.sml.display","frame {0} - position: {1}".format(frameName,anatomicalFrames[-1]))
            frameNode = self.createChild(self.node, "anatomical_frames")
            frameNode.createObject("MechanicalObject", template="Rigid3", name="anatomical_frames", position=SofaPython.Tools.listListToStr(anatomicalFrames), showObject=True, showObjectScale=SofaPython.units.length_from_SI(self.param.showAnatomicalFrameScale))
            

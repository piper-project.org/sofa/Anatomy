import os
import shutil
import math
import sys

import SofaPython.Tools
import SofaPython.units
from SofaPython.Tools import listToStr as concat
import BranchingImage.API
import Flexible.API
import Compliant.sml
import ContactMapping.sml
import SofaImage.API

import Anatomy.Tools

import Sofa
import tools
import check

printLog = True
Flexible.API.printLog=printLog

class ScenePrepare(Compliant.sml.SceneArticulatedRigid):
    """ Builds a (sub)scene from an anatomy model
    precompute shapeFunction, gauss points, nodes, and mass
    """
    def __init__(self, parentNode, model, dataDir):

        Compliant.sml.SceneArticulatedRigid.__init__(self, parentNode, model)

        check.solidAnatomicalTags(model)

        # load default anatomy material
        self.material.load(os.path.join(os.path.dirname(__file__),"..", "material.json"))
        self.setMaterialByTag("ligament", "ligament")
        self.setMaterialByTag("muscle", "muscle")
        self.setMaterialByTag("bone", "bone")
        self.setMaterialByTag("skin", "fat")
        self.setMaterialByTag("flesh", "flesh")
        self.setMaterialByTag("ligament_1d", "ligament_1d")
        self.setMaterialByTag("muscle_1d", "muscle_1d")

        # create export directory (removing any existing one, to be sure no file are here from a previous export)
        self.dataDir=dataDir
        if os.path.exists(dataDir):
            shutil.rmtree(dataDir)
        os.makedirs(dataDir)

        # variables
        self.entitiesProp = Anatomy.Tools.EntityProperties()

        self.Images=dict()
        self.behaviors=dict()
        self.collisions=dict()
        self.visuals=dict()

        # frames
        self.labelImage=None
        self.youngImage=None
        self.densityImage=None
        self.shapeFunction=None
        self.AffineDof=None
        self.AffineMass=None
        self.cells=list()

        # FEM
        self.femImage=None
        self.sampler=None
        self.mappedFEMDof=None
        self.freeFEMDof=None
        self.FEMDof=None

        # parameters
        self.param.voxelSize = 5E-3 # m
        self.param.color = {"default":[.8,.8,.8,1], "bone":[.8,.8,.8,1],  "flesh":[1.,0.5,0.5,0.5], "skin":[1.,0.75,0.75,0.5], "fat":[1.,0.75,0.75,0.5], "ligament":[.2,.2,1,1], "muscle":[1,.2,.2,1] }

        # frames
        self.param.coarseningFactor = 1 # coarsening wrt. voxelsize for shape function generation
        self.param.autoConnectSkin = True # attach any solid to the flesh

        # absolute number of nodes by tag
        # for small solids or open mesh, the density is not relevant
        # supersede nodeDensity
        self.param.numberOfNodes = {"default": None}
        # nodes density by tag
        self.param.nodeDensity = {"default": 1000} # nb/m^3
        self.param.GPDensity = 2000  # nb/m^3 # TODO: to be coherent change it to a dict by tag
        self.param.GPType = "331"
        self.param.GPMinNumber = 1  # minimum number of Gauss Points per solid

        self.param.samplingMethod = 'global' # 'local'
        # soft tissue solid tags to be sampled on surface
        self.param.sampleSurfaceRatherThanVolume = set()

        self.param.softTissueTags={"ligament","muscle"}
        self.param.boneTags={"bone"}

        self.param.exportShapeFunction = False

        self.param.showAffine = True
        self.param.showAffineScale = 50E-3 # m

        self.param.showRigidVertex = True
        self.param.showRigidVertexScale = 5e-3 # m

        self.param.showMappedHexaVertex = True
        self.param.showMappedHexaVertexScale = 5e-3

        self.param.showGPScale = 2E-3 # m

        # FEM
        self.param.femCoarseningFactor = 2 # coarsening wrt. voxelsize for FEM generation
        self.param.femTags={"skin","flesh"}
        self.param.gridType = 'branching' # 'regular'
        self.param.mapBoundaryFEM=True # if false, only FEM nodes inside 'frame' tissues are mapped; otherwise all nodes belonging to cells containing 'frame' tissues (attached to FEM)

        self.param.showHexa = False

        # springs
        self.param.springTags={"ligament_1d","muscle_1d"}
        self.param.color["ligament_1d"]=self.param.color["ligament"]
        self.param.color["muscle_1d"]=self.param.color["muscle"]
        self.param.showSpringScale= 1E-3 # m



    def createScene(self):

        if not type(self.param.nodeDensity) is dict:
            d = self.param.nodeDensity
            self.param.nodeDensity = {"default": d}
            Sofa.msg_deprecated("Anatomy.sml", "param.nodeDensity should be a dict")
        if hasattr(self.param, "sampleSkinSurfaceRatherThanVolume"):
            Sofa.msg_deprecated("Anatomy.sml", "param.sampleSkinSurfaceRatherThanVolume is replaced by param.sampleSurfaceRatherThanVolume")
            if self.param.sampleSkinSurfaceRatherThanVolume:
                self.param.sampleSurfaceRatherThanVolume.add("skin")

        self.node.createObject("RequiredPlugin", name="Image")
        self.node.createObject("RequiredPlugin", name="BranchingImage")
        self.node.createObject("RequiredPlugin", name="Flexible")
        self.node.createObject("RequiredPlugin", name="Compliant")

        self.param.rigidTags.update(self.param.boneTags) # add bones to compliant tags
        for t in self.param.boneTags:
            self.setMaterialByTag(t, "bone")
        Compliant.sml.SceneArticulatedRigid.createScene(self)

        # solid images
        allTags = self.param.boneTags | self.param.softTissueTags | self.param.femTags
        tools.computeVolumes(self,allTags)
        tools.insertImagesFromSolids(self,allTags, self.param.voxelSize)
        tools.connectImages(self)

        # frame image at original resolution -> use it for FEM mesh splitting and frame sampling
        fineFrameImage = tools.mergeImages(self,self.param.boneTags | self.param.softTissueTags,'Frame_images', 1)

        # FEM images ->  image at original resolution and labels (for mesh splitting)
        fineFemImage = tools.mergeImages(self,self.param.femTags,'FEM_images', 1)

        # FEM images -> coarse image with fused labels (for mesh generation without superimposed hexa from the same solids)
        self.femImage = BranchingImage.API.Image(fineFemImage.node, 'Coarse_image', fineFemImage.imageType)
        self.femImage.fromTransferFunction ( fineFemImage, self.entitiesProp.getFusionMap() )
        self.femImage.fromSubsampledImage(self.femImage,self.param.femCoarseningFactor)
        # self.femImage.addViewer()

        # label image for SF computation
        if self.param.mapBoundaryFEM:         # when mapping boundary hexa, shape function needs to be computed everywhere
            self.labelImage = tools.mergeImages(self,self.param.boneTags | self.param.softTissueTags | self.param.femTags, 'label', self.param.coarseningFactor)
        else:  # only non FEM part. @todo: reuse fineFrameImage?
            self.labelImage = tools.mergeImages(self,self.param.boneTags | self.param.softTissueTags, 'label', self.param.coarseningFactor)

        self.youngImage = BranchingImage.API.Image(self.labelImage.node, "young", imageType="ImageR") # TODO should not be necessary to create an ImageR
        self.youngImage.fromTransferFunction ( self.labelImage, self.entitiesProp.getYoungMap() )
        self.densityImage = BranchingImage.API.Image(self.labelImage.node, "density", imageType="ImageR") # TODO should not be necessary to create an ImageR
        self.densityImage.fromTransferFunction ( self.labelImage, self.entitiesProp.getDensityMap() )
        # self.labelImage.addViewer()

        # build FEM mesh
        self.createChild(self.node, "FEM")
        self.labelImage.node.addChild(self.nodes["FEM"])
        self.femImage.node.addChild(self.nodes["FEM"])
        if self.param.gridType == 'branching':
            self.sampler = SofaImage.API.Sampler(self.nodes["FEM"])
            self.sampler.addImageRegularSampler(self.femImage, showEdges="0", printLog=printLog)
        else : #  @todo: generate directly a regular image, and handle rigid cells
            regularIm = self.femImage.toImage(self.femImage.node,'',conversionType=0)
            self.sampler = SofaImage.API.Sampler(self.nodes["FEM"])
            self.sampler.addImageRegularSampler(regularIm,showEdges="0", printLog=printLog)

        # split FEM mesh in frames/FEM regions
            # label FEM mesh with 'FEM' labels
        self.sampler.node.createObject('MeshLabeler',template=fineFemImage.template(),name="labeler_FEM",image=fineFemImage.getImagePath()+".image",hexahedra=self.sampler.sampler.getLinkPath()+".hexahedra",voxelToHexahedra=self.femImage.getImagePath()+".voxelIndices")
        if self.param.mapBoundaryFEM:
            # label FEM mesh with 'Frame' labels
            fineFrameImage.node.addChild(self.nodes["FEM"])
            self.sampler.node.createObject('ImageMeshIntersection',template=fineFrameImage.template(),name="imageMeshIntersection",computeImage=False,image=fineFrameImage.getImagePath()+".image",transform=fineFrameImage.getImagePath()+".transform",hexahedra=self.sampler.sampler.getLinkPath()+".hexahedra",position=self.sampler.sampler.getLinkPath()+".position")
            self.sampler.node.createObject('MeshLabeler',template=fineFrameImage.template(),name="labeler_Frame",image=fineFrameImage.getImagePath()+".image",hexahedra=self.sampler.sampler.getLinkPath()+".hexahedra",voxelToHexahedra="@imageMeshIntersection.voxelIndices")
            # @todo: change "unsigned char" template if 256 labels are not sufficient
            connect = self.sampler.node.createObject('SelectConnectedLabelsROI',name="connect",template="unsigned char",nbLabels=2,labels1="@labeler_FEM.pointLabels",labels2="@labeler_Frame.pointLabels",connectLabels= self.entitiesProp.getConnectionString())
        else:
            connect = self.sampler.node.createObject('BranchingCellOffsetsFromConnectLabels',template=self.labelImage.template(),name="connect",image=self.labelImage.getImagePath()+".image",transform=self.labelImage.getImagePath()+".transform",pointLabels="@labeler_FEM.pointLabels",position=self.sampler.sampler.getLinkPath()+".position",connectLabels= self.entitiesProp.getConnectionString())
        split = self.sampler.node.createObject('MeshSplittingEngine',name="split",nbInputs="1",position=self.sampler.sampler.getLinkPath()+".position",indices1="@connect.indices")

        # build Frame-based model

        # get a point on each rigid and compute its offset in label image
        rigidVertex=list()
        for solid in self.model.getSolidsByTags(self.param.boneTags):
            p = Anatomy.Tools.transform(Anatomy.Tools.getFirstVertex(solid.mesh[0].source),solid.position) # TODO: do not call again getFirstVertex (it loads the full mesh)
            c = self.Images[solid.id].node.createObject('BranchingCellOffsetsFromPositions', name='cell', template=self.labelImage.template(), image=self.labelImage.getImagePath()+".image", transform=self.labelImage.getImagePath()+".transform", position =concat(p), labels=concat(self.entitiesProp.entities[solid.id].allLabels()))
            rigidVertex.append(p)
            self.cells.append(c)

        # sample affine dofs
        if self.param.samplingMethod=='global':
            # here we need a young image with only 'frame' tissues
            if self.param.mapBoundaryFEM:
                image = BranchingImage.API.Image(fineFrameImage.node, "young", imageType="ImageR")
                image.fromTransferFunction ( fineFrameImage, self.entitiesProp.getYoungMap() )
                tools.sampleFromModel_global(self, rigidVertex, image) # warning: this image is not subsampled by self.param.coarseningFactor (self.youngImage is)
            else:
                tools.sampleFromModel_global(self, rigidVertex, self.youngImage)
        else:
            tools.sampleFromModel_solidPerSolid(self)

        # merge rigid dofs
        if self.rigids and len(self.param.boneTags):
            self.nodes["dofRigid"] = self.insertMergeRigid("dofRigid",self.param.boneTags)
        else :
            self.nodes["dofRigid"] = None

        # display rigid vertices
        if not self.nodes["dofRigid"] is None and self.param.showRigidVertex:
            self.createChild( self.nodes["dofRigid"], "rigidVertex" )
            self.nodes["rigidVertex"].createObject("MechanicalObject", name="displayRigidVertex",
                                                   position=SofaPython.Tools.listListToStr(rigidVertex),
                                                   showObject=True, showObjectScale=SofaPython.units.length_from_SI(self.param.showRigidVertexScale),
                                                   drawMode=1, showColor="1. 0. 0. 1." )

        # "all" frame dofs
        if not self.nodes["dofAffine"] is None and not self.nodes["dofRigid"] is None:      # there are Affines and rigids in the scene
            self.createChild( [self.nodes["dofRigid"],self.nodes["dofAffine"]], "all_Frames" )
            mergedNodes = self.nodes["all_Frames"].createObject("MergeMeshes", name="mergedNodes", nbMeshes="2", position1=SofaPython.Tools.listListToStr(rigidVertex),  position2=self.AffineDof.src)
            mergedNodesPos = mergedNodes.getLinkPath()+".position"
            if printLog:
                Sofa.msg_info('Anatomy.sml',"use Affine and Rigid Dofs")
        elif not self.nodes["dofAffine"] is None:                                           # there are only Affines
            self.nodes["all_Frames"] = self.nodes["dofAffine"]
            mergedNodesPos = self.AffineDof.src
            if printLog:
                Sofa.msg_info('Anatomy.sml',"use Affine Dofs")
        elif not self.nodes["dofRigid"] is None:                                           # there are only Rigids
            self.nodes["all_Frames"] = self.nodes["dofRigid"]
            mergedNodesPos = SofaPython.Tools.listListToStr(rigidVertex)
            if printLog:
                Sofa.msg_info('Anatomy.sml',"use Rigid Dofs")
        else :
            return

        mergedCells = self.nodes["all_Frames"].createObject('MergeVectors',name='cells',nbInputs=len(self.cells), **dict({ 'input'+str(i+1): c.getLinkPath()+'.cell' for i,c in enumerate(self.cells) }) )

        # frame shape function
        self.shapeFunction = Flexible.API.ShapeFunction(self.nodes["all_Frames"])
        self.youngImage.node.addChild(self.shapeFunction.node)  # this fixes the scene graph but not very satisfactory..
        self.shapeFunction.addVoronoi(self.youngImage, mergedNodesPos, '@'+SofaPython.Tools.getObjectPath(mergedCells)+'.output')
        # display shape function
        # self.shapeFunction.shapeFunction.clearData=False
        # self.shapeFunction.node.createObject("BranchingImageToImageConverter", template="BranchingImageD,ImageD", name="converter", inputBranchingImage="@shapeFunction.distances", conversionType="0")
        # self.shapeFunction.node.createObject('ImageViewer',template="ImageD",image="@converter.image",transform="@shapeFunction.transform")

        # frame mass
        if not self.nodes["dofAffine"] is None:
            self.AffineMass = Flexible.API.AffineMass( self.nodes["dofAffine"])
            self.AffineMass.massFromDensityImage(self.nodes["all_Frames"],self.nodes["dofRigid"], self.densityImage)

        # frame tissues
        # @todo: add 'skin' layer
        for solid in self.model.getSolidsByTags(self.param.softTissueTags):
            if printLog:
                Sofa.msg_info('Anatomy.sml',"creating solid "+solid.name)

            # add collisions/visuals
            (self.collisions[solid.id],self.visuals[solid.id]) = tools.insertDeformable(solid, self.nodes["all_Frames"], self.nodes["dofRigid"], self.nodes["dofAffine"],
                                                                                           labelImage=self.labelImage,labels=self.entitiesProp.entities[solid.id].allLabels(),
                                                                                           color=solid.getValueByTag(self.param.color))
            # add behavior
            self.behaviors[solid.id] = tools.insertBehavior(solid, self.nodes["all_Frames"], SofaPython.units.density_from_SI(self.param.GPDensity), self.param.GPMinNumber, self.param.GPType,
                                                                    self.shapeFunction, self.nodes["dofRigid"], self.nodes["dofAffine"],
                                                                    self.labelImage, self.entitiesProp.entities[solid.id].allLabels(),
                                                                    showSamplesScale=SofaPython.units.length_from_SI(self.param.showGPScale), drawMode=1)
            # debug
            # mat = self.getMaterial(solid.id)
            # self.behaviors[solid.id].addHooke( strainMeasure="Corotational", youngModulus=self.material.youngModulus(mat), poissonRatio= self.material.poissonRatio(mat),useOffset=False )


        # frame springs
        for solid in self.model.getSolidsByTags(self.param.springTags):
            if printLog:
                Sofa.msg_info('Anatomy.sml',"creating solid "+solid.name)

            # add collisions/visuals
            indexLabelPairs=tools.getSpringAttachments(solid, self)
            (self.collisions[solid.id],self.visuals[solid.id]) = tools.insertSpring(solid, self.nodes["all_Frames"], self.nodes["dofRigid"], self.nodes["dofAffine"],
                                                                                       labelImage=self.labelImage,indexLabelPairs=indexLabelPairs,
                                                                                       radius=SofaPython.units.length_from_SI(self.param.showSpringScale), color=solid.getValueByTag(self.param.color))
        # partially map FEM nodes
        self.createChild( self.nodes["all_Frames"], "mapped_FEM" )
        self.sampler.node.addChild(self.nodes["mapped_FEM"]) # this fixes the scene graph but not very satisfactory...
        self.mappedFEMDof = Flexible.API.FEMDof(self.nodes["mapped_FEM"])
        self.mappedFEMDof.addMechanicalObject(position=split.getLinkPath()+".position1", drawMode=1, showColor=concat([1, 0.5, 0, 1.]), showObject=self.param.showMappedHexaVertex, showObjectScale=SofaPython.units.length_from_SI(self.param.showMappedHexaVertexScale))
        self.mappedFEMDof.mapping = Flexible.API.insertLinearMapping(self.mappedFEMDof.node, self.nodes["dofRigid"], self.nodes["dofAffine"]) # @todo compute offset during splitting for more accurate mapping , cell=connect.getLinkPath()+".cell")

        # free FEM dofs (@todo implement the case when there is no mapped dofs)
        self.freeFEMDof = Flexible.API.FEMDof(self.nodes["FEM"])
        self.freeFEMDof.addMechanicalObject(position=split.getLinkPath()+".position2")

        # "all" FEM dofs
        self.createChild( [self.mappedFEMDof.node,self.freeFEMDof.node], "all_FEM" )
        self.FEMDof = Flexible.API.FEMDof(self.nodes["all_FEM"])
        self.FEMDof.addMesh(src=self.sampler.sampler.getLinkPath())
        self.FEMDof.addMechanicalObject()
        self.FEMDof.mapping = self.FEMDof.node.createObject('SubsetMultiMapping', name='mapping', input='@'+self.mappedFEMDof.node.getPathName()+' @'+self.freeFEMDof.node.getPathName(), output = '@./', indexPairs=split.getLinkPath()+".indexPairs")

        # display FEM
        if self.param.showHexa:
            self.createChild( self.nodes["all_FEM"],'VisuHexa' )
            self.nodes["VisuHexa"].createObject('VisualModel', color="0.8 0.8 1 1",edges="@../mesh.edges",position="@../mesh.position",useVBO="0")
            self.nodes["VisuHexa"].createObject('IdentityMapping')

        # FEM mass
        totalMass=0
        for solid in self.model.getSolidsByTags(self.param.femTags):
            totalMass += solid.volume * self.material.density(self.getMaterial(solid.id))
        self.FEMDof.addUniformMass(totalMass)

        #  FEM shape function
        self.FEMDof.addShapeFunction()

        #   possibly improves accuracy of integration volume using finer image
        fineVolumes = self.femImage.getImagePath()+".volumes" if self.param.gridType == 'branching' and self.param.femCoarseningFactor>1 else ''

        # fem tissues
        # @todo: add 'skin' layer
        for solid in self.model.getSolidsByTags(self.param.femTags):
            if printLog:
                Sofa.msg_info('Anatomy.sml',"Anatomy.sml creating solid "+solid.name)

            # add collisions/visuals
            if self.param.gridType == 'branching':
                (self.collisions[solid.id],self.visuals[solid.id]) = tools.insertDeformable(solid, self.nodes["all_FEM"], dofAffineNode=self.nodes["all_FEM"], labelImage=self.femImage, labels=self.entitiesProp.entities[solid.id].allLabels(), useGlobalIndices=True, color=solid.getValueByTag(self.param.color))
            else:
                (self.collisions[solid.id],self.visuals[solid.id]) = tools.insertDeformable(solid, self.nodes["all_FEM"], dofAffineNode=self.nodes["all_FEM"], color=solid.getValueByTag(self.param.color))

            # add behavior
            femCells = self.femImage.node.createObject('BranchingCellIndicesFromLabels', name="hexaIndices_"+solid.name, template=self.femImage.template(), labels = concat(self.entitiesProp.entities[solid.id].allLabels()), image=self.femImage.getImagePath()+".image", transform=self.femImage.getImagePath()+".transform")
            self.behaviors[solid.id] = tools.insertFEMBehavior(solid.name, self.FEMDof.mesh, self.nodes["all_FEM"], self.nodes["all_FEM"],
                                                                  fineVolumes=fineVolumes , indices= femCells.getLinkPath()+".cell",
                                                                  showSamplesScale=SofaPython.units.length_from_SI(self.param.showGPScale), drawMode=1)

            # debug
            # mat = self.getMaterial(solid.id)
            # self.behaviors[solid.id].addHooke( strainMeasure="Corotational", youngModulus=self.material.youngModulus(mat), poissonRatio= self.material.poissonRatio(mat),useOffset=False )


        # @todo fem springs ?

        # self.nodes["all_FEM"].createObject('HexahedronFEMForceField',name='ff',youngModulus="20000",poissonRatio='0')

        # export images
        if self.param.exportShapeFunction:
            self.labelImage.addExporter(directory=self.dataDir)
            #self.youngImage.addExporter(directory=self.dataDir)
            #self.densityImage.addExporter(directory=self.dataDir)
            self.shapeFunction.addExporter(directory=self.dataDir)


    def export(self):
        if self.param.exportShapeFunction:
            self.entitiesProp.write(self.dataDir)

        # export dofs
        if not self.nodes["dofAffine"] is None:
            self.AffineDof.write(directory=self.dataDir)
            self.AffineMass.write(directory=self.dataDir)

        if not self.mappedFEMDof is None:
            self.mappedFEMDof.write('mappedFEMDof',directory=self.dataDir)
        if not self.freeFEMDof is None:
            self.freeFEMDof.write('freeFEMDof',directory=self.dataDir)
        if not self.FEMDof is None:
            self.FEMDof.write('FEMDof',directory=self.dataDir)

        # export behavior (shape function values + gradients + Hessian + volume)
        for b in self.behaviors.values():
            b.write(directory=self.dataDir)

        # export deformables (shape function values)
        for solid in self.model.getSolidsByTags(self.param.softTissueTags | self.param.springTags | self.param.femTags):
            for d in self.collisions[solid.id].values() + self.visuals[solid.id].values():
                d.write(directory=self.dataDir)



class SceneSimu(Compliant.sml.SceneArticulatedRigid):
    """ Builds a (sub)scene from an anatomy model
        [tag]bone are simulated as RigidBody
        Compliant joints are setup between the rigids
    """


    def addMeshExporters(self, dir, ExportAtEnd=False):
        Compliant.sml.SceneArticulatedRigid.addMeshExporters(self,dir,ExportAtEnd=ExportAtEnd) # export rigids
        tools.addMeshExporters(self,dir,ExportAtEnd=ExportAtEnd) # export deformables


    def __init__(self, parentNode, model, dataDir):

        Compliant.sml.SceneArticulatedRigid.__init__(self, parentNode, model)

        check.solidAnatomicalTags(model)

        # load default anatomy material
        self.material.load(os.path.join(os.path.dirname(__file__), "..", "material.json"))
        self.setMaterialByTag("ligament", "ligament")
        self.setMaterialByTag("muscle", "muscle")
        self.setMaterialByTag("bone", "bone")
        self.setMaterialByTag("skin", "fat")
        self.setMaterialByTag("flesh", "flesh")
        self.setMaterialByTag("ligament_1d", "ligament_1d")
        self.setMaterialByTag("muscle_1d", "muscle_1d")

        # variables
        self.dataDir = dataDir
        self.entitiesProp=None

        self.slidingContacts = dict()
        self.collisionContacts = dict()
        check.checkContactsModel(self.model)

        self.behaviors=dict()
        self.behaviors_1d=dict()
        self.collisions=dict()
        self.visuals=dict()

        # frames
        self.labelImage=None
        self.AffineDof=None
        self.AffineMass=None

        # FEM
        self.mappedFEMDof=None
        self.freeFEMDof=None
        self.FEMDof=None

        # parameters
        self.param.color = {"default":[.8,.8,.8,1], "bone":[.8,.8,.8,1],  "flesh":[1.,0.5,0.5,0.5], "skin":[1.,0.75,0.75,0.5], "fat":[1.,0.75,0.75,0.5], "ligament":[.2,.2,1,1], "muscle":[1,.2,.2,1] }
        self.param.parallel = False # activate parallel computations for component supporting it

            # sliding contacts
        self.param.contactCompliance = 1e-2 # s^2/kg
        self.param.slidingContactRejectBoundary = True
            # collision contacts
        self.param.collisionCompliance = 1e-14 # compliance value for bone collision constraint
        self.param.collisionOffset = 0 # m collisions are detected when the distance is as small as offset
        self.param.collisionNbClosestVertex = None # number of vertices used to detect and react to a collision, None for all vertices

        self.param.loadShapeFunction = False

            # frames
        self.param.softTissueTags={"ligament","muscle"}
        self.param.boneTags={"bone"}

        self.param.showAffine = True
        self.param.showAffineScale = 50E-3 # m

            # FEM
        self.param.femTags={"skin","flesh"}
        self.param.femPicking=False

        self.param.showHexa = False

            # FEM->frames coupling
        self.param.unidirectionalCoupling = False

            # material
        self.param.behaviorLaw = 'hooke' # 'projective'
        self.param.strainMeasure="Corotational" # 'Green'
        self.param.useStrainOffset = False

        self.param.behaviorLaw_FEM = 'hooke' # 'projective'
        self.param.strainMeasure_FEM="Corotational" # 'Green'
        self.param.useStrainOffset_FEM = False

            # springs
        self.param.springTags={"ligament_1d","muscle_1d"}
        self.param.color["ligament_1d"]=self.param.color["ligament"]
        self.param.color["muscle_1d"]=self.param.color["muscle"]
        self.param.showSpringScale= 1E-3 # m
        self.param.enableSpringLimits = False

        self.param.exportDir=''

    def createScene(self):

        self.node.createObject("RequiredPlugin", name="Flexible")
        self.node.createObject("RequiredPlugin", name="Compliant")
        if len(self.model.getSurfaceLinksByTags({"sliding", "collision"})) > 0:
            self.node.createObject("RequiredPlugin", name="ContactMapping")
        if self.param.loadShapeFunction:
            self.node.createObject("RequiredPlugin", name="Image")
            self.node.createObject("RequiredPlugin", name="BranchingImage")

        self.param.rigidTags.update(self.param.boneTags) # add bones to compliant tags
        for t in self.param.boneTags:
            self.setMaterialByTag(t, "bone")
        # walkaround to prevent insertion of all rigids in the root node
        #Compliant.sml.SceneArticulatedRigid.createScene(self)

        self.createChild(self.node, "Frames")
        # rigids
        for rigidModel in self.model.getSolidsByTags(self.param.rigidTags):
            self.rigids[rigidModel.id] = Compliant.sml.insertRigid(self.nodes["Frames"], rigidModel, density=self.material.density(self.getMaterial(rigidModel.id)) , param=self.param)
        # joints
        for jointModel in self.model.genericJoints.values():
            self.joints[jointModel.id] = Compliant.sml.insertJoint(jointModel, self.rigids, param=self.param)

        # all rigids
        if self.rigids and len(self.param.boneTags):
            self.nodes["dofRigid"] = self.insertMergeRigid("dofRigid",self.param.boneTags)
        else:
            self.nodes["dofRigid"] = None

        # affine frames
        self.createChild(self.nodes["Frames"], "dofAffine")
        self.AffineDof = Flexible.API.AffineDof(self.nodes["dofAffine"])
        self.AffineDof.read(directory=self.dataDir)

        if not self.AffineDof.dof is None: # there are Affines
            self.AffineDof.dof.showObject=self.param.showAffine
            self.AffineDof.dof.showObjectScale=SofaPython.units.length_from_SI(self.param.showAffineScale)
            # Mass
            self.AffineMass = Flexible.API.AffineMass(self.nodes["dofAffine"])
            self.AffineMass.read(directory=self.dataDir)
        else :
            self.nodes["Frames"].removeChild(self.nodes["dofAffine"])
            self.nodes["dofAffine"] = None

        # "all" frame dof
        if not self.nodes["dofAffine"] is None and not self.nodes["dofRigid"] is None:      # there are Affines and rigids in the scene
            self.createChild( [self.nodes["dofRigid"],self.nodes["dofAffine"]], "all_Frames" )
        elif not self.nodes["dofAffine"] is None:                                           # there are only Affines
            self.nodes["all_Frames"] = self.nodes["dofAffine"]
        elif not self.nodes["dofRigid"] is None:                                            # there are only Rigids
            self.nodes["all_Frames"] = self.nodes["dofRigid"]
        else :
            return

        # shape function
        if self.param.loadShapeFunction is True:
            Sofa.msg_info("Load shape function")
            # entities
            self.entitiesProp = Anatomy.Tools.EntityProperties.read(self.dataDir)
            self.shapeFunction = Flexible.API.ShapeFunction(self.nodes["all_Frames"])
            self.shapeFunction.addContainer(directory=self.dataDir)

            # label
            self.labelImage = BranchingImage.API.Image(self.nodes["all_Frames"])
            self.labelImage.addContainer(filename='label.mhd',directory=self.dataDir, name='label')

        # frame soft tissues
        # @todo: add 'skin' layer
        for solid in self.model.getSolidsByTags(self.param.softTissueTags):
            if printLog:
                Sofa.msg_info('Anatomy.sml',"creating solid "+solid.name)

            # add collisions/visuals
            (self.collisions[solid.id],self.visuals[solid.id]) = tools.insertDeformable(solid, self.nodes["all_Frames"], self.nodes["dofRigid"], self.nodes["dofAffine"],color=solid.getValueByTag(self.param.color))
            for d in self.collisions[solid.id].values() + self.visuals[solid.id].values():
                d.read(directory=self.dataDir)

            # add behavior
            mat = self.getMaterial(solid.id)
            self.behaviors[solid.id] = tools.loadBehavior(solid, self.nodes["all_Frames"], self.dataDir, self.nodes["dofRigid"], self.nodes["dofAffine"])
            self.behaviors[solid.id].readWeights(directory=self.dataDir)
            if self.param.behaviorLaw == 'projective':
                self.behaviors[solid.id].addProjective( youngModulus=self.material.youngModulus(mat) )
            else:
                self.behaviors[solid.id].addHooke( strainMeasure=self.param.strainMeasure, youngModulus=self.material.youngModulus(mat), poissonRatio= self.material.poissonRatio(mat),useOffset=self.param.useStrainOffset )


        # frame springs
        for solid in self.model.getSolidsByTags(self.param.springTags):
            if printLog:
                Sofa.msg_info('Anatomy.sml',"creating solid "+solid.name)

            # add collisions/visuals
            (self.collisions[solid.id],self.visuals[solid.id]) = tools.insertSpring(solid, self.nodes["all_Frames"], self.nodes["dofRigid"], self.nodes["dofAffine"], radius=SofaPython.units.length_from_SI(self.param.showSpringScale), color=solid.getValueByTag(self.param.color))
            for d in self.collisions[solid.id].values() + self.visuals[solid.id].values():
                d.read(directory=self.dataDir)

            # add behavior
            mat = self.material.data[self.getMaterial(solid.id)]
            self.behaviors_1d[solid.id]=dict()
            for meshid, c in self.collisions[solid.id].iteritems():
                self.behaviors_1d[solid.id][meshid] = tools.insertSpringBehavior(c, mat, self.param.enableSpringLimits)

        # FEM
        self.createChild( self.nodes["all_Frames"], "mapped_FEM" )
        self.mappedFEMDof = Flexible.API.FEMDof(self.nodes["mapped_FEM"])
        self.mappedFEMDof.read("mappedFEMDof",directory=self.dataDir)
        self.mappedFEMDof.mapping = Flexible.API.insertLinearMapping(self.mappedFEMDof.node, self.nodes["dofRigid"], self.nodes["dofAffine"])
        self.mappedFEMDof.readMapping("mappedFEMDof",directory=self.dataDir)

        self.createChild(self.node, "FEM")
        self.createChild(self.nodes["FEM"], "free_FEM")
        self.freeFEMDof = Flexible.API.FEMDof(self.nodes["free_FEM"])
        self.freeFEMDof.read("freeFEMDof",directory=self.dataDir)

        if self.param.unidirectionalCoupling:
            self.mappedFEMDof.mapping.isMechanical=False
            mappedFEMDofnode = self.createChild( self.nodes["FEM"], "mapped_FEM_copy" )
            mappedFEMDofnode.createObject('MechanicalObject',template="Vec3d",tags='NoPicking', position=self.mappedFEMDof.dof.getLinkPath()+".position")
            mappedFEMDofnode.createObject('FixedConstraint',fixAll=True,showObject=False,drawSize=0)
        else:
            mappedFEMDofnode = self.mappedFEMDof.node

        self.createChild( [mappedFEMDofnode,self.freeFEMDof.node], "all_FEM" )
        self.FEMDof = Flexible.API.FEMDof(self.nodes["all_FEM"])
        self.FEMDof.read("FEMDof",directory=self.dataDir)
        self.FEMDof.mapping = self.FEMDof.node.createObject('SubsetMultiMapping', name='mapping', input='@'+mappedFEMDofnode.getPathName()+' @'+self.freeFEMDof.node.getPathName(), output = '@./')
        self.FEMDof.readMapping("FEMDof",directory=self.dataDir)

        if not self.param.femPicking:
            self.freeFEMDof.dof.tags='NoPicking'
            self.mappedFEMDof.dof.tags='NoPicking'
            self.FEMDof.dof.tags='NoPicking'

        # display FEM
        if self.param.showHexa:
            self.createChild( self.nodes["all_FEM"],'VisuHexa' )
            self.nodes["VisuHexa"].createObject('VisualModel', color="0.8 0.8 1 1",edges="@../mesh.edges",position="@../mesh.position",useVBO="0")
            self.nodes["VisuHexa"].createObject('IdentityMapping')

        #  FEM shape function
        if self.param.loadShapeFunction :
            self.FEMDof.addShapeFunction()

        # fem tissues
        # @todo: add 'skin' layer
        for solid in self.model.getSolidsByTags(self.param.femTags):
            if printLog:
                Sofa.msg_info('Anatomy.sml',"Anatomy.sml creating solid "+solid.name)

            # add collisions/visuals
            (self.collisions[solid.id],self.visuals[solid.id]) = tools.insertDeformable(solid, self.nodes["all_FEM"], dofAffineNode=self.nodes["all_FEM"], color=solid.getValueByTag(self.param.color))
            for d in self.collisions[solid.id].values() + self.visuals[solid.id].values():
                d.read(directory=self.dataDir)

            # add behavior
            mat = self.getMaterial(solid.id)
            self.behaviors[solid.id] = tools.loadBehavior(solid, self.nodes["all_FEM"], self.dataDir, dofAffineNode = self.nodes["all_FEM"])
            self.behaviors[solid.id].readWeights(directory=self.dataDir)
            if self.param.behaviorLaw_FEM == 'projective':
                self.behaviors[solid.id].addProjective( youngModulus=self.material.youngModulus(mat) )
            else:
                self.behaviors[solid.id].addHooke( strainMeasure=self.param.strainMeasure_FEM, youngModulus=self.material.youngModulus(mat), poissonRatio= self.material.poissonRatio(mat),useOffset=self.param.useStrainOffset_FEM )


        # @todo fem springs ?

        # contacts
        tools.insertSlidingContactsFromModel(self)
        tools.insertCollisionContactsFromModel(self)

        # obj exporters
        if len(self.param.exportDir):
            self.addMeshExporters(self.param.exportDir,ExportAtEnd=False)

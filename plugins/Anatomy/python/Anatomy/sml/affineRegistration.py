import tools
import check
import rigidAffine

class ScenePrepare(rigidAffine.ScenePrepare):
    def __init__(self, parentNode, model, dataDir):

        rigidAffine.ScenePrepare.__init__(self, parentNode, model, dataDir)

        self.param.softTissueTags.add("bone")
        self.param.boneTags.remove("bone")

        self.param.samplingMethod = 'global'

        for t in self.param.softTissueTags:
            self.setMaterialByTag(t, "flesh")

    def createScene(self):
        rigidAffine.ScenePrepare.createScene(self)

    def export(self):
        rigidAffine.ScenePrepare.export(self)


class SceneRegistration(rigidAffine.SceneSimu):
    """ Builds an Frame simulation from anatomy model
    [tag]bone are simulated as soft solids
    Material is homogenous (flesh)
    """

    def __init__(self, parentNode, model, dataDir):

        rigidAffine.SceneSimu.__init__(self, parentNode, model, dataDir)

        self.param.softTissueTags.add("bone")
        self.param.boneTags.remove("bone")

        # variables
        self.registrations = dict()
        check.checkRegistrationModel(self.model)

        # parameters
        for m in self.material.data:
            self.material.data[m]['poissonRatio']=0 # clean poisson ratio to enhance scaling
        for t in self.param.softTissueTags:
            self.setMaterialByTag(t, "flesh")
        self.param.behaviorLaw = 'projective' # 'hooke'

        self.param.color["target"]=[.8,.8,1,1]

        self.param.blendingFactor=0
        self.param.normalThreshold=0
        self.param.outlierThreshold=0
        self.param.ICPstiffness=1

    def createScene(self):
        rigidAffine.SceneSimu.createScene(self)

        self.node.createObject("RequiredPlugin", name="Registration")
        self.node.createObject("RequiredPlugin", name="ContactMapping")
        # self.node.getObject('VisualStyle').displayFlags='showBehaviorModels showVisual showForceFields'

        # Registration
        tools.insertRegistrationFromModel(self)

import os, sys, math, random

import numpy

import RigidScale.tools as tools

import SofaPython.Tools
import SofaPython.units
from SofaPython import Quaternion
from SofaPython.Tools import listToStr as listToStr, listListToStr as listListToStr
import SofaPython.mass
import SofaPython.Quaternion
import Flexible.sml, SofaPython.sml

from collections import OrderedDict as odict

from Compliant import Frame as Frame, Tools as Tools, StructuralAPI

import RigidScale.API

import RigidScale.sml
import RigidScale.sml_deprecated as sml_deprecated

## ==============================================================================
## @Description: Registration methods
## ==============================================================================
"""
@Description: Register the bone using the position in the roi, and icp forces
- @param: boneModels: dictionnary of RigidScale.API.ShearlessAffineBody
- @param: smlBoneModel: sml bone model which is required
- @param: targetMeshFile: target mesh file
"""
def registerSurfaceUsingICP(boneModels, smlBoneModels, targetMeshFiles, stiffness=1E-6, damping=0, blendingFactor=0.85):

    if not boneModels or not smlBoneModels:
        return

    # --- data
    registrationNodes = list()

    # --- target node creation
    parentNode = boneModels.values()[0].node.getParents()[0]
    targetNode = parentNode.createChild('target_boundary')

    loaderComponents = list()
    for i, file in enumerate(targetMeshFiles):
        loaderComponent = targetNode.createObject('MeshObjLoader', name='loader_'+str(i), filename=file, triangulate=1)
        loaderComponents.append(loaderComponent)

    data = odict({'position'+str(i+1): listListToStr(item.position) for i, item in enumerate(loaderComponents)})
    data.update(odict({'edges'+str(i+1): listListToStr(item.edges) for i, item in enumerate(loaderComponents)}))
    data.update(odict({'triangles'+str(i+1): listListToStr(item.triangles) for i, item in enumerate(loaderComponents)}))
    data.update(odict({'quads'+str(i+1): listListToStr(item.quads) for i, item in enumerate(loaderComponents)}))
    targetNode.createObject('MergeMeshes', name='loader', nbMeshes=len(loaderComponents), **data)
    targetNode.createObject('MeshTopology', name='topology', src='@loader')
    targetNode.createObject('NormalsFromPoints', name='normalsFromPoints', template='Vec3'+StructuralAPI.template_suffix, position='@topology.position', triangles='@topology.triangles', invertNormals=0)
    targetNode.createObject('OglModel', template='ExtVec3f', name='visual', src='@loader', color='0.5 0.3 0.1 0.2')

    for key, smlBoneModel in smlBoneModels.iteritems():
        # class boneModel
        boneModel = boneModels[key]
        # faces and dof's position
        faces_to_register = []
        positions_to_register = []
        # --- if there is no ROI
        if not len(smlBoneModel.mesh):
            print "Info: no mesh has been found, the registration of boundary surface will not be possible."
            return
        # --- if there is some mesh
        else:
            for mesh in smlBoneModel.mesh:
                #variables
                cpt = 0
                indexTab = []
                indexMap = {}
                (vertexs, normals, texcoords, faces) = tools.loadOBJ(mesh.source)
                # conversion of the face in triangles
                triangles = []
                for f in faces:
                    if len(f[0]) < 3:
                         print "Your mesh may contain some edges"
                    elif len(f[0]) == 3:
                        triangles.append([f[0][0], f[0][1], f[0][2]])
                    elif len(f[0]) == 4:
                        triangles.append([f[0][0], f[0][1], f[0][2]])
                        triangles.append([f[0][0], f[0][2], f[0][3]])
                    elif len(f[0]) > 4:
                        print "Your mesh may contain other polygons (", len(f[0]), len(f[1]), len(f[2]),") than triangle and the quads, these ones are note yet handled"
                # case if it exists ROIs
                if len(mesh.group):
                    for tag, roi in mesh.group.iteritems():
                        # selection of a set of points used to apply the registration force field (This will change the ROI will be available in sml)
                        if "registrationROI" in tag:
                            for i in roi.index:
                                indexTab.append(i)
                                indexMap[i] = cpt
                                positions_to_register.extend(vertexs[i])
                                cpt = cpt + 1
                    for f in triangles:
                        isValid = True
                        for i in f:
                            if i not in indexTab:
                                isValid = False
                        if isValid:
                            faces_to_register.extend([indexMap[f[0]], indexMap[f[1]], indexMap[f[2]]])
                # case with no ROI
                else:
                    for p in vertexs:
                        positions_to_register.extend(p)
                    for f in triangles:
                        faces_to_register.extend(f)

        # if there are no point or face, then we stop here
        if not len(positions_to_register) or not len(faces_to_register): return

        # registration forces
        registrationNode = boneModel.affineNode.createChild('source_boundary')
        registrationNode.createObject('MeshTopology', name='topology', position=listToStr(positions_to_register), triangles=listToStr(faces_to_register))
        registrationNode.createObject('MechanicalObject', template='Vec3'+StructuralAPI.template_suffix, name='dofs', src='@topology')
        registrationNode.createObject('NormalsFromPoints', name='normalsFromPoints', template='Vec3'+StructuralAPI.template_suffix, position='@dofs.position', triangles='@topology.triangles', invertNormals=0)
        registrationNode.createObject('LinearMapping', template='Affine,Vec3'+StructuralAPI.template_suffix)
        registrationNode.createObject('ClosestPointRegistrationForceField', name='ICP', template='Vec3'+StructuralAPI.template_suffix
                                                                                      , sourceTriangles='@topology.triangles', sourceNormals='@normalsFromPoints.normals'
                                                                                      , position='@'+Tools.node_path_rel(registrationNode, targetNode)+'/topology.position', triangles='@'+Tools.node_path_rel(registrationNode, targetNode)+'/topology.triangles', normals='@'+Tools.node_path_rel(registrationNode, targetNode)+'/normalsFromPoints.normals'
                                                                                      , cacheSize=4, blendingFactor=blendingFactor, stiffness=stiffness, damping=damping
                                                                                      , outlierThreshold=0, normalThreshold=0, rejectOutsideBbox=0, drawColorMap='0')
        registrationNodes.append(registrationNode)

    # output
    return registrationNodes

"""
@Description: add attach constraint between a point mapped to register bone with a point added to a new mechanical state and fixed with a constraint
- @param: boneModel: RigidScale.API.ShearlessAffineBody which containt the rigid, the affine and etc
- @param: sourcePoints: source point mapped the affine dofs
- @param: targetPoints: target point corresponding to the source point.
- @param: compliance: compliance of the constraint.
- @param: damping: damping of the constraint.
"""
def addAttachConstraint(boneModel, sourcePoint, targetPoint, compliance=1E-5, damping=0.1):

    if not boneModel:
        return

    if not sourcePoint or not targetPoint:
        return

    # add of a node containing the points mapped to the affine dofs
    attachNode = boneModel.affineNode.createChild('attach_'+str(random.randint(1, 1E7)))
    attachNode.createObject('MechanicalObject', template='Vec3d', name='DOFs', position=listToStr(sourcePoint), rest_position=listToStr(sourcePoint))
    attachNode.createObject('LinearMapping', template='Affine,Vec3d', name='mapping')

    # creation of the target points
    targetNode = attachNode.createChild('target_'+str(random.randint(1, 1E7)))
    targetNode.createObject('MechanicalObject', template='Vec3d', name='DOFs', position=listToStr(targetPoint), rest_position=listToStr(targetPoint))
    targetNode.createObject('FixedConstraint', template='Vec3d', name='constraint', fixAll=1)

    # creation of the constraint
    distanceNode = attachNode.createChild('attach_constraint'); targetNode.addChild(distanceNode)
    distanceNode.createObject('MechanicalObject', template='Vec3d', name='distanceDOFs')
    distanceNode.createObject('DifferenceMultiMapping', template='Vec3d,Vec3d', input='@'+Tools.node_path_rel(distanceNode, targetNode)+' @'+Tools.node_path_rel(distanceNode, attachNode), output='@.', pairs='0 0', showObjectScale="0.005")
    distanceNode.createObject('UniformCompliance', name='constraint', isCompliance=0 if compliance else 1, compliance=compliance, damping=damping if compliance else 0)

    return attachNode

# ==============================================================================
# @Description: Create a sofa scene containing :
# - the articulated system
# - few constraint between the source and the target
# - only the ICP for the registration
"""
- @param: parentNode: the node which will contains the articulated system
- @param: model: sml model
- @param: elasticity: elasticity of each bone model
- @param: targets: list of meshes
"""
# ==============================================================================
class SceneRegistrationWithArticulatedSystemAndICP(sml_deprecated.SceneArticulatedRigidScale):

    def __init__(self, parentNode, model, targets=None):

        sml_deprecated.SceneArticulatedRigidScale.__init__(self, parentNode, model)

        # params
        self.targets = targets

        self.target_translation = [0, 0, 0]
        self.target_rotation = [0, 0, 0]
        self.target_scale = [1, 1, 1]

        self.outputDirectory = None

        self.param.showTransferFunction = True

        self.param.registrationCompliance = 7.5E-4

        self.param.icpStiffness = 0.5
        self.param.icpDamping = 0
        self.param.icpBlendingFactor = 0.85

        self.icpForceComponents = list()    # used for icp transfer
        self.dofConstraintNodes = dict()    # used for rigid transfer
        self.constraintNodes = dict()   # used for classic transfer
        self.attachNodes = list() # used for the attaches between key points

    def createScene(self):

        # new plugin required
        self.node.createObject('RequiredPlugin', pluginName='Registration')

        # parent methods: creation of the articulated system
        sml_deprecated.SceneArticulatedRigidScale.createScene(self)

        # transfer using icp force
        boneModels = self.bones
        smlBoneModels = self.model.solids
        registrationNodes = registerSurfaceUsingICP(boneModels, smlBoneModels, self.targets, self.param.icpStiffness, self.param.icpDamping, self.param.icpBlendingFactor)

        for node in registrationNodes:
            ICPForceComponent = node.getObject('ICP')
            self.icpForceComponents.append(ICPForceComponent)

        # add of exporter
        for smlBoneModel in smlBoneModels:
            if self.outputDirectory:
                filepath = os.path.join(self.outputDirectory, smlBoneModel.name)
                self.visuals[smlBoneModel.name].node.createObject('OBJExporter', name='exporter', filename=filepath, exportAtEnd=1, printLog=1)

    def addAttachConstraint(self, boneName, sourcePoint, targetPoint, compliance=1E-5, damping=0.1):

        if boneName not in self.bones.keys():
            return

        attachNode = addAttachConstraint(self.bones[boneName], sourcePoint, targetPoint, compliance, damping)

        self.attachNodes.append(attachNode)

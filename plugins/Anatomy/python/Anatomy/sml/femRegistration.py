import tools
import check
import rigidFem

class Scene(rigidFem.Scene):
    """ Builds an Hexa FEM simulation from anatomy model
    [tag]bone are simulated as soft solids
    Compliant joints are setup between the rigids
    Material is homogenous (flesh)
    """

    def __init__(self, parentNode, model):

        rigidFem.Scene.__init__(self, parentNode, model)

        self.param.softTissueTags.add("bone")
        self.param.boneTags.remove("bone")

        # variables
        self.registrations = dict()
        check.checkRegistrationModel(self.model)

        # parameters
        for m in self.material.data:
            self.material.data[m]['poissonRatio']=0 # clean poisson ratio to enhance scaling
        for t in self.param.softTissueTags:
            self.setMaterialByTag(t, "flesh")
        self.param.behaviorLaw = 'projective'  # 'hooke'

        self.param.color["target"]=[.8,.8,1,1]

        self.param.blendingFactor=0
        self.param.normalThreshold=0
        self.param.outlierThreshold=0
        self.param.ICPstiffness=1



    def createScene(self):
        rigidFem.Scene.createScene(self)

        self.node.createObject("RequiredPlugin", name="Registration")
        # self.node.getObject('VisualStyle').findData('displayFlags').showForceFields=True

        # Registration
        tools.insertRegistrationFromModel(self)


import Sofa

def solidAnatomicalTags(model):
    """ Check that all solids have a single anatomical tag
    """
    anatomicalTags = {"bone","ligament","muscle","skin","flesh"}
    for solid in model.solids.values():
        if len(solid.tags & anatomicalTags) > 1:
           Sofa.msg_error('Anatomy.sml', "Solid {0} has more than one anatomical tag".format(solid.name))



def checkRegistrationModel(model):
    # check that all sources have collision enabled. Otherwise set it
    for r in model.getSurfaceLinksByTags({"registration"}):
        meshid = r.surfaces[0].mesh.id
        if not r.surfaces[0].solid.meshAttributes[meshid].collision :
            r.surfaces[0].solid.meshAttributes[meshid].collision = True
            Sofa.msg_warning('Anatomy.sml',meshid+' collision enabled to handle registration')

def checkContactsModel(model, tags={"sliding","collision"}):
    # check that all sources have collision enabled
    for r in model.getSurfaceLinksByTags(tags):
        for i in xrange(2):
            meshid = r.surfaces[i].mesh.id
            if not r.surfaces[i].solid.meshAttributes[meshid].collision :
                # r.surfaces[i].solid.meshAttributes[meshid].collision = True
                Sofa.msg_warning('Anatomy.sml',meshid+" collision is not enabled. Contact forces will not be applied.")

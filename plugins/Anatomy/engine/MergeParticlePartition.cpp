/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/

#include "MergeParticlePartition.h"

#include <sstream>
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

SOFA_DECL_CLASS(MergeParticlePartition)
int MergeParticlePartitionClass = core::RegisterObject("")
        .add<MergeParticlePartition>();

MergeParticlePartition::MergeParticlePartition()
    : d_partitionSize( initData(&d_partitionSize, "partitionSize", "number of sets in the partition"))
    , vd_inSetIndices(this, "setIndices",  "particles indices in the set", helper::DataEngineInput)
    , d_outIndexPairs( initData(&d_outIndexPairs, "outIndexPairs", "indexPairs to merge the particles") )
{
    this->addAlias(&d_outIndexPairs, "indexPairs");
}

MergeParticlePartition::~MergeParticlePartition()
{}

/// Parse the given description to assign values to this object's fields and potentially other parameters
void MergeParticlePartition::parse ( sofa::core::objectmodel::BaseObjectDescription* arg )
{
    vd_inSetIndices.parseSizeData(arg, d_partitionSize);
    Inherit1::parse(arg);
}

/// Assign the field values stored in the given map of name -> value pairs
void MergeParticlePartition::parseFields ( const std::map<std::string,std::string*>& str )
{
    vd_inSetIndices.parseFieldsSizeData(str, d_partitionSize);
    Inherit1::parseFields(str);
}

void MergeParticlePartition::init()
{
    addInput(&d_partitionSize);

    vd_inSetIndices.resize(d_partitionSize.getValue());

    addOutput(&d_outIndexPairs);

    setDirtyValue();
}

void MergeParticlePartition::reinit()
{
    update();
}


namespace {

struct IndexTrio {
    unsigned int node; ///< node number
    unsigned int nodeIndex; ///< index in node
    unsigned int setIndex;  ///< index in set
};

bool operator< (const IndexTrio& lhs, const IndexTrio& rhs)
{
    return lhs.setIndex < rhs.setIndex;
}

}

void MergeParticlePartition::update()
{
    cleanDirty();
    vd_inSetIndices.resize(d_partitionSize.getValue());

    helper::vector<IndexTrio> myIndexTrios;
    for (unsigned int i = 0 ; i < d_partitionSize.getValue() ; ++i) {
        IndexTrio it;
        it.node=i;
        SetIndex const& setIndices = vd_inSetIndices[i]->getValue();
        for (unsigned int j = 0 ; j < setIndices.size() ; ++j) {
            it.nodeIndex = j;
            it.setIndex = setIndices[j];
            myIndexTrios.push_back(it);
        }
    }
    // sort myIndexPairs by increasing childIndex
    std::sort(myIndexTrios.begin(), myIndexTrios.end());
    // generate the new indexPairs
    helper::WriteAccessor< Data<SetIndex> > indexPairs(d_outIndexPairs);
    indexPairs.clear();
    indexPairs.reserve(myIndexTrios.size());
    for (unsigned int i=0 ; i < myIndexTrios.size() ; ++i) {
        indexPairs.push_back(myIndexTrios[i].node);
        indexPairs.push_back(myIndexTrios[i].nodeIndex);

        if (i==0 && myIndexTrios[i].setIndex !=0)
            serr << "missing particule 0" << sendl;
        if ( i!=0 && myIndexTrios[i-1].setIndex + 2 == myIndexTrios[i].setIndex)
            serr << "missing particle " << myIndexTrios[i-1].setIndex + 1 << sendl;
        if ( i!=0 && myIndexTrios[i-1].setIndex == myIndexTrios[i].setIndex)
            serr << "duplicated particle " << myIndexTrios[i-1].setIndex << sendl;

    }

    sout << "Created indexPairs from " << d_partitionSize.getValue() << " sets containing " << myIndexTrios.size() << " particles" << sendl;

}

} // namespace engine

} // namespace component

} // namespace sofa

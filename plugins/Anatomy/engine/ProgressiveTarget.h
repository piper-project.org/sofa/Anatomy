/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_ENGINE_PROGRESSIVETARGET_H
#define SOFA_COMPONENT_ENGINE_PROGRESSIVETARGET_H

#include <sofa/core/DataEngine.h>

namespace sofa
{

namespace component
{

namespace engine
{

/**
 * @brief Reach a target value step by step.
 * When no target is given, the current position is used as the target.
 *
 * @author Thomas Lemaire @date 2015
 */
template <class DataTypes>
class ProgressiveTarget : public core::DataEngine
{
public:
    SOFA_CLASS(SOFA_TEMPLATE(ProgressiveTarget, DataTypes), core::DataEngine);

    typedef typename DataTypes::Real Real;
    typedef typename DataTypes::Coord Coord;
    typedef typename DataTypes::VecCoord VecCoord;

    ProgressiveTarget();
    ~ProgressiveTarget();

    /// Update
    void update();

    virtual void init();
    virtual void reinit();

    virtual std::string getTemplateName() const;

    static std::string templateName(const ProgressiveTarget<DataTypes>* = NULL);

protected:

    /// inputs
    /// @{
    Data<VecCoord> d_current; ///< monitor current position
    Data<VecCoord> d_target;
    Data< helper::vector<Real> > d_step; ///< step increment between intermediate targets, if size is 1 the same value used for all particules, for Rigid type, step[0] is for translation, step[1] is for rotation
    /// @}

    /// outputs
    /// @{
    Data<VecCoord> d_output; ///< intermediate target position
    /// @}

};

} // namespace engine

} // namespace component

} // namespace sofa

#endif // SOFA_COMPONENT_ENGINE_PROGRESSIVETARGET_H

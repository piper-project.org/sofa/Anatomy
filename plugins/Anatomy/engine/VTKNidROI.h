/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_ENGINE_VTKNIDROI_H
#define SOFA_COMPONENT_ENGINE_VTKNIDROI_H

#include <sofa/core/DataEngine.h>

namespace sofa
{

namespace component
{

namespace engine
{

/**
 * Output the positions and their indices in the global mesh
 *
 * @author Thomas Lemaire @date 2014
 */
template <class DataTypes>
class VTKNidROI : public core::DataEngine
{
public:
    SOFA_CLASS(SOFA_TEMPLATE(VTKNidROI, DataTypes), core::DataEngine);

    typedef unsigned int Id;
    typedef helper::vector<Id> VecId;
    typedef typename DataTypes::VecCoord VecCoord;

    VTKNidROI();
    ~VTKNidROI();

    /// Update
    void update();

    virtual void init();
    virtual void reinit();

    virtual std::string getTemplateName() const;

    static std::string templateName(const VTKNidROI<DataTypes>* = NULL);

protected:

    /// inputs
    /// @{
    Data<VecCoord> d_position; ///< input positions
    Data<VecId> d_nid; ///< input position node id
    Data<VecId> d_nidROI; ///< roi node id
    /// @}

    /// outputs
    /// @{
    Data<VecId> d_indices; ///< ROI indices
    Data<VecCoord> d_pointsInROI; ///< ROI positions
    /// @}

};

} // namespace engine

} // namespace component

} // namespace sofa

#endif // SOFA_COMPONENT_ENGINE_VTKNIDROI_H

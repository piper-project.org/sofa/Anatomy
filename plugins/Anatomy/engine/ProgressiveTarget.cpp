/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/

#include "ProgressiveTarget.inl"

#include "../initAnatomy.h"

#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

using namespace sofa::defaulttype;

SOFA_DECL_CLASS(ProgressiveTarget)

int ProgressiveTargetClass = core::RegisterObject("Reach target values step by step")
#ifndef SOFA_FLOAT
        .add< ProgressiveTarget<Vec1dTypes> >()
        .add< ProgressiveTarget<Vec3dTypes> >()
        .add< ProgressiveTarget<Rigid3dTypes> >()
#endif //SOFA_FLOAT
#ifndef SOFA_DOUBLE
        .add< ProgressiveTarget<Vec1fTypes> >()
        .add< ProgressiveTarget<Vec3fTypes> >()
//        .add< ProgressiveTarget<Rigid3fTypes> >()
#endif //SOFA_DOUBLE
        ;

#ifndef SOFA_FLOAT
template class SOFA_Anatomy_API ProgressiveTarget<Vec1dTypes>;
template class SOFA_Anatomy_API ProgressiveTarget<Vec3dTypes>;
template class SOFA_Anatomy_API ProgressiveTarget<Rigid3dTypes>;
#endif //SOFA_FLOAT
#ifndef SOFA_DOUBLE
template class SOFA_Anatomy_API ProgressiveTarget<Vec1fTypes>;
template class SOFA_Anatomy_API ProgressiveTarget<Vec3fTypes>;
//template class SOFA_Anatomy_API ProgressiveTarget<Rigid3fTypes>;
#endif //SOFA_DOUBLE

} // namespace engine

} // namespace component

} // namespace sofa

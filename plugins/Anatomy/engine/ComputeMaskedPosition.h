/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_ENGINE_ComputeMaskedPosition_H
#define SOFA_COMPONENT_ENGINE_ComputeMaskedPosition_H

#include <sofa/core/DataEngine.h>

namespace sofa
{

namespace component
{

namespace engine
{

/** Compute full positions using masked value.
 *
 * This engine is designed to work with the sofa::component::mapping::MaskMapping and mapping such as sofa::component::mapping::DifferenceFromTargetMapping.
 *
 * @author Thomas Lemaire @date 2016
 */
template <class DataTypes, class MaskDataTypes>
class ComputeMaskedPosition : public core::DataEngine
{
public:
    SOFA_CLASS(SOFA_TEMPLATE2(ComputeMaskedPosition, DataTypes, MaskDataTypes), core::DataEngine);

    typedef typename DataTypes::Real Real;
    typedef typename DataTypes::Coord Coord;
    typedef typename DataTypes::VecCoord VecCoord;
    typedef helper::ReadAccessor<Data< VecCoord > > raVecCoord;
    typedef helper::WriteOnlyAccessor<Data< VecCoord > > waVecCoord;
    typedef helper::vector< Coord > MaskType;
    typedef helper::ReadAccessor<Data< VecCoord > > raMaskType;
    typedef typename MaskDataTypes::VecCoord MaskDataVecCoord;
    typedef helper::ReadAccessor<Data< MaskDataVecCoord > > raMaskDataVecCoord;

    ComputeMaskedPosition()
        : d_position( initData(&d_position, "position", "source positions") )
        , d_mask( initData(&d_mask, "mask", "positions mask") )
        , d_maskPosition( initData(&d_maskPosition, "maskPosition", "masked positions value") )
        , d_output( initData(&d_output, "output", "updated positions with masked values") )
    {

    }

    ~ComputeMaskedPosition() {}

    virtual void init() {
        addInput(&d_position);
        addInput(&d_mask);
        addInput(&d_maskPosition);
        addOutput(&d_output);

        setDirtyValue();
    }

    virtual void reinit() { update(); }

    /// Update
    void update() {
        raVecCoord position(d_position);
        raMaskType mask(d_mask);
        raMaskDataVecCoord maskPosition(d_maskPosition);

        cleanDirty();

        waVecCoord output(d_output);
        output.resize(position.size());

        std::size_t iMask=0;
        for (std::size_t i=0; i<position.size(); ++i) {
            output[i] = position[i];
            for (std::size_t j=0; j<mask[i].size(); ++j) {
                if (mask[i][j]) {
                    output[i][j]=maskPosition[iMask][0];
                    ++iMask;
                }
            }
        }
    }

    virtual std::string getTemplateName() const {
        return templateName(this);
    }

    static std::string templateName(const ComputeMaskedPosition<DataTypes, MaskDataTypes>* = NULL) {
        return std::string(DataTypes::Name()) + "," + MaskDataTypes::Name();
    }


protected:

    /// inputs
    /// @{
    Data< VecCoord > d_position; ///< full positions
    Data< MaskType > d_mask; ///< each position mask
    Data< MaskDataVecCoord > d_maskPosition; ///< values for the masked positions
    /// @}

    /// outputs
    /// @{
    Data< VecCoord > d_output; ///< updated positions with masked values
    /// @}

};

} // namespace engine

} // namespace component

} // namespace sofa

#endif // SOFA_COMPONENT_ENGINE_ComputeMaskedPosition_H

/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_ENGINE_MERGEPARTICLEPARTITION_H
#define SOFA_COMPONENT_ENGINE_MERGEPARTICLEPARTITION_H

#include <sofa/core/DataEngine.h>

//#include <sofa/component/component.h>
#include <sofa/core/objectmodel/BaseObject.h>
#include <sofa/core/topology/BaseMeshTopology.h>

#include <sofa/helper/vectorData.h>

namespace sofa
{

namespace component
{

namespace engine
{

/**
 * Given the partition of set particles in different nodes, this engine enables to merge them back in a single node.
 * From the indices of each particle in the original set, this engine builds the [parent_i indexInParent_i] list so as to give as an input of a subsetmultimapping.
 *
 * example in MergeParticlePartition.py
 *
 * @author Thomas Lemaire @date 2014
 */
class MergeParticlePartition : public core::DataEngine
{
public:
    SOFA_CLASS(MergeParticlePartition, core::DataEngine);

    typedef core::topology::BaseMeshTopology::SetIndex SetIndex;

protected:
    MergeParticlePartition();
    ~MergeParticlePartition();

    /**
     * Update the particle indexPairs
     */
    void update();

public:

    /// Parse the given description to assign values to this object's fields and potentially other parameters
    void parse ( sofa::core::objectmodel::BaseObjectDescription* arg );

    /// Assign the field values stored in the given map of name -> value pairs
    void parseFields ( const std::map<std::string,std::string*>& str );


    virtual void init();
    virtual void reinit();

protected:

    Data<unsigned int> d_partitionSize; ///< number of nodes in the partition
    helper::vectorData< SetIndex > vd_inSetIndices; ///< input partition
    Data<SetIndex> d_outIndexPairs;   ///< pairs of [parent_i indexInParent_i]

};

} // namespace engine

} // namespace component

} // namespace sofa

#endif // SOFA_COMPONENT_ENGINE_MERGEPARTICLEPARTITION_H

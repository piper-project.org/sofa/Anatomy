#include "JointBiasImage.h"

#include "../initAnatomy.h"

#include <sofa/core/ObjectFactory.h>

#include <BranchingImage/ImageContainer.h>

namespace sofa
{

namespace component
{

namespace engine
{

using namespace sofa::defaulttype;

SOFA_DECL_CLASS(JointBiasImage)

int JointBiasImageClass = core::RegisterObject("")
#ifndef SOFA_FLOAT
    .add<JointBiasImage<BranchingImageD> >()
#endif //SOFA_FLOAT
#ifndef SOFA_DOUBLE
    .add<JointBiasImage<BranchingImageF> >()
#endif //SOFA_DOUBLE
    ;


#ifndef SOFA_FLOAT
template class SOFA_Anatomy_API JointBiasImage<BranchingImageD>;
#endif //SOFA_FLOAT
#ifndef SOFA_DOUBLE
template class SOFA_Anatomy_API JointBiasImage<BranchingImageF>;
#endif //SOFA_DOUBLE

} // namespace engine

} // namespace component

} // namespace sofa

#include <SofaTest/Python_test.h>


namespace sofa {

// static build of the test list
static struct Tests : public Python_test_list
{
    Tests()
    {
        static const std::string testPath = std::string(ANATOMY_TEST_PYTHON_DIR);

        // add python tests here
        addTest( "test_Tools.py", testPath);
        addTest( "test_Landmark.py", testPath);

    }
} tests;


// run test list
INSTANTIATE_TEST_CASE_P(Batch,
                        Python_test,
                        ::testing::ValuesIn(tests.list));

} // namespace sofa

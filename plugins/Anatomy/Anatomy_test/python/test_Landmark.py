import os.path

from SofaTest.Macro import *
import Sofa
import SofaTest
import Anatomy.Landmark

# data test dir
dataDir = os.path.join(os.path.dirname(os.path.abspath(__file__)),"../data")

A=[1, 0, 0]
sphere=[0,0,0]
C=[0,2,0]
D=[0,2.5,0]

def test_readXmlDataSet():
    ok = True
    set = Anatomy.Landmark.readXmlDataSet(os.path.join(dataDir, "landmarks.xml"))
    ok &= EXPECT_TRUE(set.hasLandmark("A"), "hasLandmark(A)")
    ok &= EXPECT_EQ(os.path.abspath(os.path.join(dataDir,"mesh.obj")), set.getLandmark("A").mesh, "landmark mesh")
    ok &= EXPECT_EQ([0], set.getLandmark("A").indices, "landmark mesh")
    return ok

def test_fillPositions():
    ok = True
    set = Anatomy.Landmark.readXmlDataSet(os.path.join(dataDir, "landmarks.xml"))
    set.fillPositions()
    ok &= EXPECT_VEC_EQ(A, set.getLandmark("A").position,"landmark position")
    ok &= EXPECT_VEC_EQ(sphere, set.getLandmark("sphere").position,"sphere center")
    ok &= EXPECT_VEC_EQ(C, set.getLandmark("C").position,"landmark position")
    return ok


def run():
    ok = True
    ok &= EXPECT_TRUE(test_readXmlDataSet(), "test_readXmlDataSet")
    ok &= EXPECT_TRUE(test_fillPositions(), "test_fillPositions")
    return ok

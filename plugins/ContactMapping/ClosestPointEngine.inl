/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 beta 4      *
*                (c) 2006-2009 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_ClosestPointEngine_INL
#define SOFA_ClosestPointEngine_INL

#include "ClosestPointEngine.h"
#include <sofa/core/visual/VisualParams.h>
#include <sofa/helper/kdTree.inl>
#include <sofa/helper/AdvancedTimer.h>

#include <sofa/helper/IndexOpenMP.h>
#ifdef _OPENMP
#include <omp.h>
#endif


#define BRUTEFORCE 0
#define KDTREE 1
#define IMAGE 2

namespace sofa
{

namespace component
{

namespace engine
{

template <class T>
void ClosestPointEngine< T >::reinit()
{
    if(this->f_printLog.getValue()) sout<<this->getName()<<": reinit"<<sendl;

    // display/hide method specific parameters
    if(this->f_method.getValue().getSelectedId()==KDTREE)
    {
        f_cacheSize.setDisplayed(true);
    }
    else
    {
        f_cacheSize.setDisplayed(false);
    }

    // resizing
    vf_sourceFeature.resize(f_nbFeatures.getValue());
    vf_targetFeature.resize(f_nbFeatures.getValue());
    vf_weightFeature.resize(f_nbFeatures.getValue());

    // init ROI
    if (f_sourceROISize.isSet()) this->resampleROI(true);
    if (f_targetROISize.isSet()) this->resampleROI(false);

    // output 'isrigid'
    if(this->f_printLog.getValue())
    {
        if(f_sourceRigidPosition.isSet()) sout<<this->getName()<<": source is rigid"<<sendl;
        if(f_targetRigidPosition.isSet()) sout<<this->getName()<<": target is rigid"<<sendl;
    }

    // update set from vector
    rejectROI.clear();
    rejectROI.insert(this->f_rejectROI.getValue().begin(), this->f_rejectROI.getValue().end());

    // enforce update of accelerating structures
    f_targetPosition.setDirtyValue();
    update();
}

template <class T>
void ClosestPointEngine< T >::update()
{
    helper::ScopedAdvancedTimer advancedTimer("ClosestPointEngine");

    if( m_dataTracker.isDirty(f_rejectROI) )
    {
        // a set is used to speed up lookups
        rejectROI.clear();
        rejectROI.insert(this->f_rejectROI.getValue().begin(), this->f_rejectROI.getValue().end());
    }


    bool targetDirty =  m_dataTracker.isDirty( f_targetPosition )
                            || m_dataTracker.isDirty( f_targetROI )
                            || m_dataTracker.isDirty( f_method );

    bool BVDirty =  targetDirty
                        || m_dataTracker.isDirty( f_useBV )
                        || m_dataTracker.isDirty( f_offsetBV );

    updateAllInputsIfDirty();

    cleanDirty();

    unsigned int nbsources = f_sourceROI.getValue().size();
    if(nbsources==0) nbsources=f_sourcePosition.getValue().size(); // ROI is unset -> use all points
    unsigned int nbtargets = f_targetROI.getValue().size();
    if(nbtargets==0) nbtargets=f_targetPosition.getValue().size(); // ROI is unset -> use all points

    {
    helper::WriteOnlyAccessor<Data<helper::vector<defaulttype::Vec2i> > > closest(f_closest);
    closest.clear();
    }

    if(!nbtargets || !nbsources) return;

    if(this->f_printLog.getValue()) sout<<this->getName()<<": update, moving target="<<targetDirty<<sendl;

    // check which feature is used
    if(f_weightPosition.getValue()==0) usePosition=false; else usePosition=true;
    if(f_weightNormal.getValue()==0 || f_sourceNormal.getValue().size()!=f_sourcePosition.getValue().size() || f_targetNormal.getValue().size()!=f_targetPosition.getValue().size()) useNormal=false; else useNormal=true;
    useFeature.resize(f_nbFeatures.getValue()); for(unsigned int i=0;i<f_nbFeatures.getValue();i++) if(vf_weightFeature[i]->getValue()==0 || vf_sourceFeature[i]->getValue().size()!=f_sourcePosition.getValue().size() || vf_targetFeature[i]->getValue().size()!=f_targetPosition.getValue().size()) useFeature[i]=false; else useFeature[i]=true;

    // check if rejection is used
    if(f_normalThreshold.getValue()<=-1 || f_sourceNormal.getValue().size()!=f_sourcePosition.getValue().size() || f_targetNormal.getValue().size()!=f_targetPosition.getValue().size()) useNormalRejection=false; else useNormalRejection=true;

    // update bounding volume if needed
    if( f_useBV.getValue() && BVDirty )
    {
        updateTargetBoundingVolume();
        if(this->f_printLog.getValue()) sout<<this->getName()<<": update Bounding Volume"<<sendl;
    }

    udateOffsetedPosition();

    switch(this->f_method.getValue().getSelectedId())
    {
    case KDTREE:
        kdTreeMethod(targetDirty && !d_KdTree.getParent() /*not a linked Data*/);
        break;
#ifdef SOFA_HAVE_IMAGE
    case IMAGE:
        imageMethod(targetDirty);
        break;
#endif
    default: // BRUTEFORCE
        bruteForceMethod();
        break;
    }

//    sout<<"update closest points"<<sendl;
}

#ifdef SOFA_HAVE_IMAGE
template <class T>
void ClosestPointEngine< T >::imageMethod(const bool /*targetDirty*/)
{
    //    const unsigned int nbsources = f_sourceROI.getValue().size();
    //    const unsigned int nbtargets = f_targetROI.getValue().size();
    //    helper::WriteOnlyAccessor<Data<helper::vector<defaulttype::Vec2i> > > closest(f_closest);

    //@todo
}
#endif


template <class T>
void ClosestPointEngine< T >::kdTreeMethod(const bool targetDirty)
{
    const ROI& sourceROI = f_sourceROI.getValue();
    const ROI& targetROI = f_targetROI.getValue();
    const VecCoord& targetPosition = f_targetPosition.getValue();

    unsigned int nbsources = sourceROI.size();    if(nbsources==0) nbsources=sourcePosition->size();
    unsigned int nbtargets = targetROI.size();    if(nbtargets==0) nbtargets=targetPosition.size();

    const bool useBV = f_useBV.getValue();
    const unsigned int cacheSize = f_cacheSize.getValue();
    const bool printLog = this->f_printLog.getValue();

    if(useNormal==true) serr<<this->getName()<<": normals not supported in KdTree method"<<sendl;
    if(f_nbFeatures.getValue()!=0) serr<<this->getName()<<": features not supported in KdTree method"<<sendl;

    // resize cache data
    if(cacheSize>1)
    {
        closests.resize(nbsources);
        if(cacheThresh_max.size()!=nbsources) {distanceSet emptyset; closests.fill(emptyset);} // source ROI as changed -> clear cache
        cacheThresh_max.resize(nbsources);
        cacheThresh_min.resize(nbsources);
        previousPosition.resize(nbsources);
    }
    else
    {
        closests.clear();
        cacheThresh_max.clear();
        cacheThresh_min.clear();
        previousPosition.clear();
    }


    const KDT& KdTree = d_KdTree.getValue();


    // update kdtree if target has changed
    if(targetDirty || KdTree.isEmpty())
    {
        helper::WriteAccessor<Data<KDT>> KdTreeWA( d_KdTree );

        if(targetROI.size())  KdTreeWA->build(targetPosition,targetROI);
        else KdTreeWA->build(targetPosition);
        distanceSet emptyset;
        closests.fill(emptyset);
        if(printLog) sout<<this->getName()<<": updated KdTree and cleared cache"<<sendl;
    }


    helper::vector<bool> incache;
    if(printLog) { incache.resize(nbsources); incache.fill(false); }  // use for stats
    closest.resize(nbsources);

#ifdef _OPENMP
#pragma omp parallel for if (this->d_parallel.getValue() && nbsources>0)
#endif
    for(sofa::helper::IndexOpenMP<unsigned int>::type ir=0;ir<nbsources;ir++)
    {
        closest[ir]=nbtargets; // set to ignored state
        int i = sourceROI.size()?sourceROI[ir]:ir;
        bool inBV=useBV?isInBoundingVolume(i):true;
        if(inBV)
        {
            unsigned int c=0;
            if(cacheSize<=1) c=KdTree.getClosest((*sourcePosition)[i],targetPosition);
            else
            {
                bool inc = KdTree.getNClosestCached(closests[ir], cacheThresh_max[ir], cacheThresh_min[ir], previousPosition[ir], (*sourcePosition)[i], targetPosition, cacheSize);
                if(printLog) incache[ir]=inc;
                c=closests[ir].begin()->second;
            }
            if(!isRejected(i,c))
                closest[ir]=c;
        }
    }

    // update output pairs from 'closest' vector
    helper::WriteOnlyAccessor<Data<helper::vector<defaulttype::Vec2i> > > closestPairs(f_closest);
    const bool revertPairs =  d_revertPairs.getValue();
    const bool sourceROIsize = sourceROI.size();
    for(unsigned int ir=0;ir<nbsources;ir++)
        if(closest[ir]!=nbtargets)
        {
            if( revertPairs )
                closestPairs.push_back(defaulttype::Vec2i(closest[ir],sourceROIsize?sourceROI[ir]:ir));
            else
                closestPairs.push_back(defaulttype::Vec2i(sourceROIsize?sourceROI[ir]:ir,closest[ir]));
        }

    if(printLog)  // stats
    {
        unsigned int nbcache=0;     for(unsigned int ir=0;ir<nbsources;ir++) if(closest[ir]!=nbtargets) if(incache[ir]) nbcache++;
        sout<<this->getName()<<": \t"<<((nbsources-closestPairs.size())*100)/nbsources<<"%\t rejected";
        if(closestPairs.size()) sout<<", \t"<<(nbcache*100)/closestPairs.size()<<"%\t in cache";
        sout<<sendl;
    }
}

template <class T>
void ClosestPointEngine< T >::bruteForceMethod()
{
    const ROI& sourceROI = f_sourceROI.getValue();
    const ROI& targetROI = f_targetROI.getValue();
    const VecCoord& targetPosition = f_targetPosition.getValue();

    unsigned int nbsources = sourceROI.size();    if(nbsources==0) nbsources=sourcePosition->size();
    unsigned int nbtargets = targetROI.size();    if(nbtargets==0) nbtargets=targetPosition.size();

    const bool useBV = f_useBV.getValue();

    closest.resize(nbsources);

#ifdef _OPENMP
#pragma omp parallel for if (this->d_parallel.getValue() && nbsources>0)
#endif
    for(sofa::helper::IndexOpenMP<unsigned int>::type ir=0;ir<nbsources;ir++)
    {
        closest[ir]=nbtargets; // set to ignored state
        int i = sourceROI.size()?sourceROI[ir]:ir;
        bool inBV=useBV?isInBoundingVolume(i):true;
        if(inBV)
        {
            unsigned int c=targetROI.size()?targetROI[0]:0;
            Real dmin=getDistance(i,c);
            for(unsigned int jr=1;jr<nbtargets;jr++)
            {
                int j = targetROI.size()?targetROI[jr]:jr;
                Real d=getDistance(i,j);
                if(d<dmin)
                {
                    dmin=d;
                    c=j;
                }
            }
            if(!isRejected(i,c))
                closest[ir]=c;
        }
    }

    // update output pairs from 'closest' vector
    helper::WriteOnlyAccessor<Data<helper::vector<defaulttype::Vec2i> > > closestPairs(f_closest);
    const bool revertPairs =  d_revertPairs.getValue();
    const bool sourceROIsize = sourceROI.size();
    for(unsigned int ir=0;ir<nbsources;ir++)
        if(closest[ir]!=nbtargets)
        {
            if( revertPairs )
                closestPairs.push_back(defaulttype::Vec2i(closest[ir],sourceROIsize?sourceROI[ir]:ir));
            else
                closestPairs.push_back(defaulttype::Vec2i(sourceROIsize?sourceROI[ir]:ir,closest[ir]));
        }
}

template <class T>
inline typename ClosestPointEngine< T >::Real ClosestPointEngine< T >::getDistance(const unsigned int i,const unsigned int j) const
{
    Real d = 0;
    if(usePosition==true) d+=f_weightPosition.getValue()*((*sourcePosition)[i]-f_targetPosition.getValue()[j]).norm2();
    if(useNormal==true) d+=f_weightNormal.getValue()*((*sourceNormal)[i]-f_targetNormal.getValue()[j]).norm2();
    for(unsigned int c=0;c<f_nbFeatures.getValue();c++) if(useFeature[c]==true) d+=vf_weightFeature[c]->getValue()*(vf_sourceFeature[c]->getValue()[i]-vf_targetFeature[c]->getValue()[j])*(vf_sourceFeature[c]->getValue()[i]-vf_targetFeature[c]->getValue()[j]);
    return d;
}


template <class T>
inline bool ClosestPointEngine< T >::isRejected(const unsigned int i,const unsigned int j) const
{
    if(rejectROI.find(j)!=rejectROI.end()) return true;
    if(getDistance(i,j)>f_distanceThreshold.getValue()*f_distanceThreshold.getValue()) return true;
    if(!useNormalRejection) return false;
    if(dot((*sourceNormal)[i],f_targetNormal.getValue()[j])<f_normalThreshold.getValue()) return true;
    return false;
}



template <class T>
void ClosestPointEngine< T >::udateOffsetedPosition()
{
    helper::ReadAccessor<Data<VecCoord> > rasourcePosition(f_sourcePosition);
    helper::ReadAccessor<Data<VecCoord> > rasourceNormal(f_sourceNormal);
    const ROI& sourceROI = f_sourceROI.getValue();
    unsigned int nbsources = sourceROI.size();
    if(nbsources==0) nbsources=rasourcePosition.size();

    // offset to put the original source points to their current position, and then to the original target point positions
    if(f_sourceRigidPosition.isSet() || f_targetRigidPosition.isSet())
    {
        rigidCoord offset= rigidType::mult(rigidType::inverse(f_targetRigidPosition.getValue()),f_sourceRigidPosition.getValue());

        offseted_sourcePosition.resize(rasourcePosition.size());
        if(rasourcePosition.size())
            for(unsigned int i=0;i<nbsources;i++)
            {
                int index = sourceROI.size()?sourceROI[i]:i;
                offseted_sourcePosition[index] = offset.projectPoint(rasourcePosition[index]);
            }
        sourcePosition=&offseted_sourcePosition;

        offseted_sourceNormal.resize(rasourceNormal.size());
        if(rasourceNormal.size())
            for(unsigned int i=0;i<nbsources;i++)
            {
                int index = sourceROI.size()?sourceROI[i]:i;
                offseted_sourceNormal[index] = offset.projectVector(rasourceNormal[index]);
            }
        sourceNormal=&offseted_sourceNormal;
    }
    else
    {
        sourcePosition=&rasourcePosition.ref();
        sourceNormal=&rasourceNormal.ref();
    }
}


template <class T>
inline bool ClosestPointEngine< T >::isInBoundingVolume(const unsigned int i) const
{
    BVCoord c;
    getBVCoord(c,(*sourcePosition)[i]);
    for( unsigned i=0 ; i<c.size() ; ++i ) { if(c[i]<BV[0][i]) return false; if(c[i]>BV[1][i]) return false; }
    return true;
}


template <class T>
inline void ClosestPointEngine< T >::getBVCoord(BVCoord& c, const Coord& p) const
{
    for( unsigned i=0 ; i<3 ; ++i ) c[i]=p[i]; // classic bounding box

    // edges
    c[3]=p[0]+p[1]; // +-(1,1,0)
    c[4]=p[0]-p[1]; // +-(1,-1,0)
    c[5]=p[0]+p[2]; // +-(1,0,1)
    c[6]=p[0]-p[1]; // +-(1,0,-1)
    c[7]=p[1]+p[2]; // +-(0,1,1)
    c[8]=p[1]-p[2]; // +-(0,1,-1)
}


template <class T>
void ClosestPointEngine< T >::updateTargetBoundingVolume()
{
    helper::ReadAccessor<Data<VecCoord> > targetPosition(f_targetPosition);

    if( !targetPosition.size() )
    {
        getBVCoord(BV[0],Coord());
        BV[1]=BV[0];
        return;
    }
    else
    {
        BVCoord c;

        getBVCoord(BV[0],targetPosition[0]);
        BV[1]=BV[0];

        for( size_t j=1 ; j<targetPosition.size() ; ++j )
        {
            getBVCoord(c,targetPosition[j]);
            for( unsigned i=0 ; i<c.size() ; ++i )
                if( BV[0][i] > c[i] ) BV[0][i] = c[i];  else if( BV[1][i] < c[i] ) BV[1][i] = c[i];
        }
    }

    // inflate
    Real offset=f_offsetBV.getValue(), offsetr2=offset*sqrt(2);
    for( unsigned i=0 ; i<3 ; ++i ) { BV[0][i]-=offset; BV[1][i]+=offset; }
    for( unsigned i=3 ; i<9 ; ++i ) { BV[0][i]-=offsetr2; BV[1][i]+=offsetr2; }
}





template <class T>
void ClosestPointEngine< T >::resampleROI(const bool useSource)
{
    unsigned int nb = useSource?f_sourceROISize.getValue():f_targetROISize.getValue();
    if(nb==0) return;
    helper::WriteAccessor<Data<helper::vector<unsigned int> > > roi(useSource?f_sourceROI:f_targetROI);
    roi.clear();

    helper::ReadAccessor<Data<VecCoord> > pos(useSource?f_sourcePosition:f_targetPosition);
    if(nb>=pos.size()) {for(unsigned int i=0;i<pos.size();++i) roi.push_back(i); return;}

    std::vector<Real> distances(pos.size(),std::numeric_limits<Real>::max());
    std::vector<unsigned int> voronoi(pos.size());

    // farthest point sampling
    roi.push_back(0); // add first point
    updateVoronoi( distances, voronoi, pos.ref(), 0);
    while(roi.size()<nb)
    {
        Real dmax=0; unsigned int imax;
        for (unsigned int i=0; i<distances.size(); i++) if(distances[i]>dmax) {dmax=distances[i]; imax=i;}
        if(dmax==0) break;
        else roi.push_back(imax);
        updateVoronoi( distances, voronoi, pos.ref(), imax);
    }

    if (this->f_printLog.getValue()) { if(useSource) sout<<this->getName()<<": resampling of "<<nb<<" source points done"<<sendl; else sout<<this->getName()<<": resampling of "<<nb<<" target points done"<<sendl; }
}


template <class T>
void ClosestPointEngine< T >::updateVoronoi(std::vector<Real>& distances, std::vector<unsigned int>& voronoi, const VecCoord& pos, const unsigned int index)
{
    for (unsigned int i=0; i< pos.size(); i++)
    {
        Real d=(pos[i] - pos[index]).norm2();  // @todo: use distance including normals and features ?
        if(d<distances[i]) { distances[i]=d; voronoi[i]=index;}
    }
}




template <class T>
void ClosestPointEngine< T >::draw(const core::visual::VisualParams* vparams)
{
    if (drawMode.getValue() == 0) return;

    helper::ReadAccessor<Data<VecCoord> > sourcePosition(f_sourcePosition);
    helper::ReadAccessor<Data<VecCoord> > targetPosition(f_targetPosition);

    helper::ReadAccessor<Data<helper::vector<defaulttype::Vec2i> > > closest(f_closest);
    size_t sourceIndex = 0, targetIndex = 1;
    if( d_revertPairs.getValue() )
    {
        sourceIndex = 1;
        targetIndex = 0;
    }

    std::vector< defaulttype::Vector3 > points;
    for (unsigned int i=0; i<closest.size(); i++)
    {
        points.push_back(f_sourceRigidPosition.getValue().projectPoint(sourcePosition[closest[i][sourceIndex]]));
        points.push_back(f_targetRigidPosition.getValue().projectPoint(targetPosition[closest[i][targetIndex]]));
    }

    const defaulttype::Vec<4,float> c(0,1,0.5,1);
    if (showArrowSize.getValue()==0 || drawMode.getValue() == 1)	vparams->drawTool()->drawLines(points, 1, c);
    else if (drawMode.getValue() == 2)	for (unsigned int i=0;i<points.size()/2;++i) vparams->drawTool()->drawCylinder(points[2*i+1], points[2*i], showArrowSize.getValue(), c);
    else if (drawMode.getValue() == 3)	for (unsigned int i=0;i<points.size()/2;++i) vparams->drawTool()->drawArrow(points[2*i+1], points[2*i], showArrowSize.getValue(), c);
}



} // namespace engine
} // namespace component
} // namespace sofa

#endif // SOFA_ClosestPointEngine_INL

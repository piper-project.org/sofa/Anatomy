#ifndef SOFA_COMPONENT_PenetrationForceField_H
#define SOFA_COMPONENT_PenetrationForceField_H
#include "initContactMapping.h"
#include <sofa/core/behavior/ForceField.h>
#include <sofa/defaulttype/Mat.h>
#include <SofaEigen2Solver/EigenSparseMatrix.h>

namespace sofa
{
namespace component
{
namespace forcefield
{

/**
    piecewise linear stiffness for oriented distances

    - when penetration (=orientated distance) is negative, @hardStiffness is used
    - when penetration is above @proximity, a null stiffness is used
    - when penetration is between 0 and @proximity, stiffness is linearly interpolated between @hardStiffness and 0

    @author: Benjamin Gilles

  */
template<class TDataTypes>
class PenetrationForceField : public core::behavior::ForceField<TDataTypes>
{
public:
    SOFA_CLASS(SOFA_TEMPLATE(PenetrationForceField, TDataTypes), SOFA_TEMPLATE(core::behavior::ForceField, TDataTypes));

    typedef TDataTypes DataTypes;
    typedef core::behavior::ForceField<TDataTypes> Inherit;
    typedef typename DataTypes::VecCoord VecCoord;
    typedef typename DataTypes::VecDeriv VecDeriv;
    typedef typename DataTypes::Coord Coord;
    typedef typename DataTypes::Deriv Deriv;
    typedef typename Coord::value_type Real;
    typedef core::objectmodel::Data<VecCoord> DataVecCoord;
    typedef core::objectmodel::Data<VecDeriv> DataVecDeriv;
    enum { N=DataTypes::deriv_total_size };

	// TODO why bother with VecDeriv instead of just SReal vec ?
    Data< SReal > hardStiffness; ///< stiffness when position <0
    Data< SReal > separatingStiffness; ///< stiffness when velocity >0
    Data< SReal > proximity; ///< position value above which stiffness=0
    Data< SReal > damping; ///< uniform damping
    Data< helper::vector<defaulttype::Vec4f> > colors;
    Data< bool > computeColors;

    virtual void init();

    /// Compute matrices
    virtual void reinit();
    virtual void reset();


    /// Return a pointer to the compliance matrix
    virtual const sofa::defaulttype::BaseMatrix* getComplianceMatrix(const core::MechanicalParams*);

    virtual void addKToMatrix( sofa::defaulttype::BaseMatrix * matrix, SReal kFact, unsigned int &offset );

    virtual void addBToMatrix( sofa::defaulttype::BaseMatrix * matrix, SReal bFact, unsigned int &offset );

    /// addForce does nothing when this component is processed like a compliance.
    virtual void addForce(const core::MechanicalParams *, DataVecDeriv &, const DataVecCoord &, const DataVecDeriv &);

    /// addDForce does nothing when this component is processed like a compliance.
    virtual void addDForce(const core::MechanicalParams *, DataVecDeriv &, const DataVecDeriv &);

    virtual SReal getPotentialEnergy(const core::MechanicalParams* /*mparams*/, const DataVecCoord& /*x*/) const
    {
        return 0;
    }

protected:
    PenetrationForceField( core::behavior::MechanicalState<DataTypes> *mm = NULL);

    Real getStiffness(const Coord& p,const Deriv& v) const;
    const defaulttype::Vec4f& getColor(const Coord& p,const Deriv& v) const;

    typedef linearsolver::EigenBaseSparseMatrix<typename DataTypes::Real> matrix_type;
    matrix_type matC; ///< compliance matrix

    typedef linearsolver::EigenSparseMatrix<TDataTypes,TDataTypes> block_matrix_type;
    block_matrix_type matK; ///< stiffness matrix (Negative S.D.)
    block_matrix_type matB; ///< damping matrix (Negative S.D.)
};

}
}
}

#endif // SOFA_COMPONENT_PenetrationForceField_H



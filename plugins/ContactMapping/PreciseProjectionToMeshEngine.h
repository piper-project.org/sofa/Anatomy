/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_PreciseProjectionToMeshEngine_H
#define SOFA_COMPONENT_PreciseProjectionToMeshEngine_H

#include "initContactMapping.h"
#include "projectToMesh.h"
#include <sofa/core/DataEngine.h>
#include <sofa/defaulttype/VecTypes.h>
#include <sofa/core/topology/BaseMeshTopology.h>
#include <sofa/helper/AdvancedTimer.h>
#include <sofa/helper/OptionsGroup.h>


#include <sofa/helper/IndexOpenMP.h>
#ifdef _OPENMP
#include <omp.h>
#endif

namespace sofa
{

namespace component
{

namespace engine
{

/**
 * Projects points on a mesh (all primivitives are tested)
 * @author Benjamin Gilles
 * @date 2016
 */
template <class DataTypes>
class PreciseProjectionToMeshEngine : public core::DataEngine
{
public:
    typedef core::DataEngine Inherited;
    SOFA_CLASS(SOFA_TEMPLATE(PreciseProjectionToMeshEngine, DataTypes), Inherited);

    typedef typename DataTypes::Coord    InCoord;
    typedef typename DataTypes::Deriv    InDeriv;
    typedef typename DataTypes::VecCoord InVecCoord;
    typedef typename DataTypes::VecDeriv InVecDeriv;
    typedef typename DataTypes::Real     Real;

    typedef typename core::topology::BaseMeshTopology::SeqEdges SeqEdges;
    typedef typename core::topology::BaseMeshTopology::Triangle Triangle;
    typedef typename core::topology::BaseMeshTopology::SeqTriangles SeqTriangles;
    typedef typename core::topology::BaseMeshTopology::SeqQuads SeqQuads;

    typedef helper::ProjectToMesh<InCoord> ProjectToMesh;
    typedef typename ProjectToMesh::ClosestStruct ClosestStruct;

    virtual std::string getTemplateName() const    { return templateName(this);    }
    static std::string templateName(const PreciseProjectionToMeshEngine<DataTypes>* = NULL) { return DataTypes::Name();   }

    Data< InVecCoord > d_source;
    Data< InVecCoord > d_target;
    Data< SeqTriangles > d_triangles;  ///<triangles
    Data< SeqQuads > d_quads;  ///<quads
    Data< SeqEdges > d_edges;  ///<edges
    Data< bool > d_parallel;		///< use openmp ?

    Data< helper::vector<ClosestStruct> > d_closests; ///< store closest point on mesh from each parent1 point
    Data< InVecCoord > d_projections;

protected:

    PreciseProjectionToMeshEngine():   Inherited()
      , d_source(initData(&d_source,"sourcePosition","source dofs to project"))
      , d_target(initData(&d_target,"targetPosition","mesh dofs"))
      , d_triangles(initData(&d_triangles,SeqTriangles(),"triangles","mesh triangles"))
      , d_quads(initData(&d_quads,SeqQuads(),"quads","mesh quads"))
      , d_edges(initData(&d_edges,SeqEdges(),"edges","mesh edges"))
      , d_parallel(initData(&d_parallel, true, "parallel", "use openmp parallelisation?"))
      , d_closests(initData(&d_closests,"output","closest primitives with weights, normal, distance"))
      , d_projections(initData(&d_projections,"projections","computed projections"))
    {
        this->addAlias(&d_target, "position");
        this->addAlias(&d_closests, "closests");
    }

    virtual ~PreciseProjectionToMeshEngine() {}

public:

    virtual void init()
    {
        addInput(&d_source);
        addInput(&d_target);
        addInput(&d_triangles);
        addInput(&d_quads);
        addInput(&d_edges);

        addOutput(&d_closests);
        addOutput(&d_projections);

        reinit();
        Inherited::init();
    }

    virtual void reinit()
    {
        Inherited::reinit();
        d_closests.setDirtyValue();
        update();
    }


    virtual void update()
    {
        helper::ScopedAdvancedTimer advancedTimer("PreciseProjectionToMeshEngine");

        updateAllInputsIfDirty();

        cleanDirty();

        const InVecCoord& source = d_source.getValue();
        const InVecCoord& target = d_target.getValue();
        const SeqTriangles& triangles = d_triangles.getValue();
        const SeqQuads& quads = d_quads.getValue();
        const SeqEdges& edges = d_edges.getValue();

        std::vector<ClosestStruct>& closests = *d_closests.beginWriteOnly();
        helper::WriteOnlyAccessor<Data< InVecCoord > > projections(d_projections);

        closests.resize(source.size());
        projections.resize(source.size());

        ProjectToMesh projectToMesh;
        projectToMesh.init(triangles,quads,edges);

#ifdef _OPENMP
#pragma omp parallel for if (this->d_parallel.getValue())
#endif
            for(sofa::helper::IndexOpenMP<size_t>::type i=0;i<source.size();i++)
            {
                projectToMesh.getClosest(closests[i],source[i],target,triangles,quads,edges);
                projections[i]=source[i]-closests[i].dist*closests[i].u;
            }

        d_closests.endEdit();

        sout<<"updated"<<sendl;
    }


};


#if defined(SOFA_EXTERN_TEMPLATE) && !defined(SOFA_COMPONENT_MAPPING_PreciseProjectionToMeshEngine_CPP)
#ifndef SOFA_FLOAT
extern template class SOFA_ContactMapping_API PreciseProjectionToMeshEngine< sofa::defaulttype::Vec3dTypes >;
#endif
#ifndef SOFA_DOUBLE
extern template class SOFA_ContactMapping_API PreciseProjectionToMeshEngine< sofa::defaulttype::Vec3fTypes >;
#endif
#endif

} // namespace engine

} // namespace component

} // namespace sofa

#endif //SOFA_COMPONENT_PreciseProjectionToMeshEngine_H

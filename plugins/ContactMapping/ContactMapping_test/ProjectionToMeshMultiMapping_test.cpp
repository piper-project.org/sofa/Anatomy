/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU General Public License as published by the Free  *
* Software Foundation; either version 2 of the License, or (at your option)   *
* any later version.                                                          *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    *
* more details.                                                               *
*                                                                             *
* You should have received a copy of the GNU General Public License along     *
* with this program; if not, write to the Free Software Foundation, Inc., 51  *
* Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.                   *
*******************************************************************************
*                            SOFA :: Applications                             *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/

/* Matthieu Nesme, 2015 */
#include <SofaTest/MultiMapping_test.h>
#include <sofa/simulation/graph/DAGSimulation.h>
#include <sofa/defaulttype/VecTypes.h>
#include <SofaBaseTopology/MeshTopology.h>
#include "../ProjectionToMeshMultiMapping.h"

namespace sofa {
namespace {

using std::cout;
using std::cerr;
using std::endl;
using namespace core;
using namespace component;
using defaulttype::Vec;
using defaulttype::Mat;
using sofa::helper::vector;


/**  Test suite for ProjectionToMeshMultiMapping.
  */
template <typename _ProjectionToMeshMultiMapping>
struct ProjectionToMeshMultiMappingTest : public MultiMapping_test<_ProjectionToMeshMultiMapping>
{

    typedef _ProjectionToMeshMultiMapping ProjectionToMeshMultiMapping;

    typedef typename ProjectionToMeshMultiMapping::In InDataTypes;
    typedef typename InDataTypes::VecCoord InVecCoord;
    typedef typename InDataTypes::VecDeriv InVecDeriv;
    typedef typename InDataTypes::Coord InCoord;
    typedef typename InDataTypes::Deriv InDeriv;
    typedef container::MechanicalObject<InDataTypes> InMechanicalObject;
    typedef typename InMechanicalObject::ReadVecCoord  ReadInVecCoord;
    typedef typename InMechanicalObject::WriteVecCoord WriteInVecCoord;
    typedef typename InMechanicalObject::WriteVecDeriv WriteInVecDeriv;
    typedef typename InDataTypes::Real InReal;


    typedef typename ProjectionToMeshMultiMapping::Out OutDataTypes;
    typedef typename OutDataTypes::VecCoord OutVecCoord;
    typedef typename OutDataTypes::VecDeriv OutVecDeriv;
    typedef typename OutDataTypes::Coord OutCoord;
    typedef typename OutDataTypes::Deriv OutDeriv;
    typedef container::MechanicalObject<OutDataTypes> OutMechanicalObject;
    typedef typename OutMechanicalObject::WriteVecCoord WriteOutVecCoord;
    typedef typename OutMechanicalObject::WriteVecDeriv WriteOutVecDeriv;
    typedef typename OutMechanicalObject::ReadVecCoord ReadOutVecCoord;
    typedef typename OutMechanicalObject::ReadVecDeriv ReadOutVecDeriv;

    component::topology::MeshTopology *inTopology2;

    void setupScene() {
        this->MultiMapping_test<ProjectionToMeshMultiMapping>::setupScene(2); // 2 parents, 1 child

        inTopology2 = modeling::addNew<component::topology::MeshTopology>(this->parents[1]).get();
    }


    bool test()
    {
        this->setupScene();

        ProjectionToMeshMultiMapping* mapping = static_cast<ProjectionToMeshMultiMapping*>( this->mapping );

        vector< InVecCoord > incoords(2);
        // mesh1 - a single point
        incoords[0].resize(1);
        InDataTypes::set( incoords[0][0], 0,0,0 );

        // mesh2 - a triangle
        incoords[1].resize(3);
        InDataTypes::set( incoords[1][0], -1,0,0 );
        InDataTypes::set( incoords[1][1], 1,-1,0 );
        InDataTypes::set( incoords[1][2], 1,1,0 );
        inTopology2->addTriangle(0,1,2);

        InVecCoord normals;
        normals.push_back(InCoord(0,0,1));
        normals.push_back(InCoord(0,0,1));
        normals.push_back(InCoord(0,0,1));
        mapping->d_normals2.setValue(normals);

        // expected child positions
        OutVecCoord expectedoutcoords(1);
        OutDataTypes::set( expectedoutcoords[0], 0, 0, 0 );

        return this->runTest(incoords,expectedoutcoords);
    }

};


// Define the list of types to instanciate. We do not necessarily need to test all combinations.
using testing::Types;
typedef Types<
mapping::ProjectionToMeshMultiMapping<defaulttype::Vec3Types,defaulttype::Vec3Types>
> DataTypes; // the types to instanciate.

// Test suite for all the instanciations
TYPED_TEST_CASE(ProjectionToMeshMultiMappingTest, DataTypes);
TYPED_TEST( ProjectionToMeshMultiMappingTest , test )
{
    ASSERT_TRUE(this->test());
}


} // namespace
} // namespace sofa

import Sofa
from SofaTest import gtest
from SofaPython.Tools import listToStr as concat
from SofaPython.Tools import listListToStr as lconcat

TESTALLPRIMITIVES = False

def createSceneAndController(rootNode):
    rootNode.createObject('RequiredPlugin', pluginName="ContactMapping")

    # add meshes to be tested and store them in (topo,expected side) pairs
    global topoSidePairs
    topoSidePairs=list()
    topoSidePairs.append( (rootNode.createObject('MeshObjLoader', name="mesh1", filename="mesh/Armadillo_simplified.obj", translation="10 0 0",scale3d="1 1 1", triangulate=True ) , 0 ) )
    topoSidePairs.append( (rootNode.createObject('MeshObjLoader', name="mesh2", filename="mesh/Armadillo_simplified.obj", translation="0 20 0",scale3d="1 1 1", triangulate=True ) , 0 ) )
    topoSidePairs.append( (rootNode.createObject('MeshObjLoader', name="mesh3", filename="mesh/raptor_35kp.obj", rotation="90 0 0", translation="0 10 12.5",scale3d="1 1 1",triangulate=True ) , 1 ) )

    # make a curved plane
    resolution = [50,50]
    # rootNode.createObject('GridMeshCreator', name="plane", resolution=concat(resolution), trianglePattern="1", translation="-10 -10 10", scale3d="20 20 1")
    # rootNode.createObject('MeshObjLoader', name="plane", filename="mesh/sphere.obj", translation="0 0 0",scale3d="10 10 10", triangulate=True )
    pos=list()
    triangles=list()
    for iy,y in enumerate(numpy.linspace(-20,20,resolution[1],endpoint=False).tolist()):
        for ix,x in enumerate(numpy.linspace(-20,20,resolution[0],endpoint=False).tolist()):
            z=10+numpy.cos( x )*3#+numpy.cos( y )
            pos.append( [x,y,z] )
            if ix!=resolution[0]-1 and iy!=resolution[1]-1:
                q = [len(pos)-1,len(pos),len(pos)+resolution[0],len(pos)-1+resolution[0]]
                triangles.append([q[0],q[1],q[2]])
                triangles.append([q[2],q[3],q[0]])
    rootNode.createObject('MeshTopology', name='plane', position=lconcat(pos), triangles=lconcat(triangles))
    rootNode.createObject('VisualModel', name="visual2", position="@plane.position",  triangles="@plane.triangles",color="green")

    return rootNode

def bwdInitGraph(rootNode):
    for i,ts in enumerate(topoSidePairs):
        checkInsideOutside(rootNode,ts[0],ts[1])
    gtest.finish()

def checkInsideOutside(rootNode,topology,expectedSide):

    suffix = topology.name
    node = rootNode.createChild("projectNode_"+suffix)
    if TESTALLPRIMITIVES:
        proj = node.createObject('PreciseProjectionToMeshEngine', name="ProjectionToMeshEngine", template='Vec3', useRestLength=False, triangles = "@../plane.triangles", sourcePosition=topology.getLinkPath()+".position", targetPosition="@../plane.position")
    else:
        node.createObject('ClosestPointEngine', name="closestPointEngine", parallel=True, cacheSize=1, template='Vec3', sourcePosition=topology.getLinkPath()+".position", targetPosition="@../plane.position" )
        proj = node.createObject('ProjectionToMeshEngine', name="projectionToMeshEngine", template='Vec3', useRestLength=False, triangles = "@../plane.triangles", sourcePosition=topology.getLinkPath()+".position", targetPosition="@../plane.position", indexPairs="@closestPointEngine.indexPairs")
    node.init()

    proj_dist = proj.output.dist
    side = [1 if d>0 else 0 for d in proj_dist]

    # check
    for i,s in enumerate(side):
        gtest.assert_true( s==expectedSide, 'in/out test error on point '+str(i)+' of mesh '+suffix)

    # visu
    node = rootNode.createChild("visuNode_"+suffix)
    node.createObject('MeshTopology', name='topo', position=topology.getLinkPath()+".position", triangles=topology.getLinkPath()+".triangles")
    node.createObject('DataDisplay',  name="dataDisplay", position=topology.getLinkPath()+".position", pointData=concat(side))
    node.createObject('ColorMap', colorScheme="Blue to Red", showLegend="1", min="@dataDisplay.currentMin", max="@dataDisplay.currentMax")
    node.init()


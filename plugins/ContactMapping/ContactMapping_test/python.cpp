
#include <SofaTest/Python_test.h>

namespace sofa {
// these are sofa scenes
static struct Tests : public Python_test_list {
    Tests() {
        addTestDir(CONTACTMAPPING_TEST_PYTHON_DIR, "test_");
    }
} tests;


// run test list
INSTANTIATE_TEST_CASE_P(Batch,
                        Python_scene_test,
                        ::testing::ValuesIn(tests.list));

TEST_P(Python_scene_test, sofa_python_scene_tests)
{
    run(GetParam());
}


}

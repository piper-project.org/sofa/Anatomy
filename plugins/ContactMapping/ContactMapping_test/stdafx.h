#ifndef STDAFX_H
#define STDAFX_H
 
// Only for windows
#ifdef WIN32
 
#include "MultiMapping_test.h"
#include "../DistanceToMeshMultiMapping.h"
#include <sofa/simulation/graph/DAGSimulation.h>
#include <sofa/defaulttype/VecTypes.h>
//#include <sofa/component/topology/MeshTopology.h>
 
#endif // WIN32
 
#endif // STDAFX_H
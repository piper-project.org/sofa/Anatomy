/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_MAPPING_ProjectionToMeshMultiMapping_H
#define SOFA_COMPONENT_MAPPING_ProjectionToMeshMultiMapping_H

#include "initContactMapping.h"
//#include <sofa/component/component.h>
#include <sofa/core/MultiMapping.h>
#include <sofa/defaulttype/VecTypes.h>
#include <SofaEigen2Solver/EigenSparseMatrix.h>
#include <sofa/core/topology/BaseMeshTopology.h>
#include "projectToMesh.h"

namespace sofa
{

namespace component
{

namespace mapping
{

/**
 * Projecting parent1 points to parent2 (mesh).
 * C_i is the closest vertex on parent2 from P_i in parent1 (provided by closestPointEngine through indexPairs)
 * @warning see ProjectionToMeshEngine to compute "closests"
 *
 * @author Matthieu Nesme, Benjamin Gilles
 * @date 2015
 */
template <class TIn, class TOut>
class ProjectionToMeshMultiMapping : public core::MultiMapping<TIn, TOut>
{
public:
    SOFA_CLASS(SOFA_TEMPLATE2(ProjectionToMeshMultiMapping, TIn, TOut), SOFA_TEMPLATE2(core::MultiMapping, TIn, TOut));

    typedef core::MultiMapping<TIn, TOut> Inherit;
    typedef TIn In;
    typedef TOut Out;

    typedef typename In::Coord    InCoord;
    typedef typename In::Deriv    InDeriv;
    typedef typename In::VecCoord InVecCoord;
    typedef typename In::VecDeriv InVecDeriv;
    typedef typename Out::Coord   OutCoord;
    typedef typename Out::Deriv   OutDeriv;
    typedef typename Out::VecCoord OutVecCoord;
    typedef typename Out::VecDeriv OutVecDeriv;
    typedef typename OutCoord::value_type Real;

    typedef typename Inherit::InDataVecCoord InDataVecCoord;
    typedef typename Inherit::InDataVecDeriv InDataVecDeriv;
    typedef typename Inherit::InDataMatrixDeriv InDataMatrixDeriv;
    typedef typename Inherit::OutDataVecCoord OutDataVecCoord;
    typedef typename Inherit::OutDataVecDeriv OutDataVecDeriv;
    typedef typename Inherit::OutDataMatrixDeriv OutDataMatrixDeriv;

    enum {Nin = In::deriv_total_size, Nout = Out::deriv_total_size };
    typedef linearsolver::EigenSparseMatrix<In,Out> Jacobian;
    typedef linearsolver::EigenSparseMatrix<In,In> Stiffness;

    typedef typename core::topology::BaseMeshTopology::SeqEdges SeqEdges;
    typedef typename core::topology::BaseMeshTopology::SeqTriangles SeqTriangles;
    typedef typename core::topology::BaseMeshTopology::SeqQuads SeqQuads;

protected:

    ProjectionToMeshMultiMapping();

    virtual ~ProjectionToMeshMultiMapping() {}


public:

    virtual void init();
    virtual void reinit();

    virtual void apply(const core::MechanicalParams* mparams, const helper::vector<OutDataVecCoord*>& dataVecOutPos, const helper::vector<const InDataVecCoord*>& dataVecInPos);

    virtual void applyJ(const core::MechanicalParams*, const helper::vector<OutDataVecDeriv*>& outDeriv, const helper::vector<const InDataVecDeriv*>& inDeriv)
    {
        if( m_jacobian1.rows() ) // out can be empty if no distance to map
        {
            m_jacobian1.mult(*outDeriv[0], *inDeriv[0]);
            m_jacobian2.addMult(*outDeriv[0], *inDeriv[1]);
        }
    }

    virtual void applyJT(const core::MechanicalParams*, const helper::vector<InDataVecDeriv*>& outDeriv, const helper::vector<const OutDataVecDeriv*>& inDeriv)
    {
        if( m_jacobian1.rows() ) // out can be empty if no distance to map
        {
            m_jacobian1.addMultTranspose(*outDeriv[0], *inDeriv[0]);
            m_jacobian2.addMultTranspose(*outDeriv[1], *inDeriv[0]);
        }
    }
    virtual void applyJT( const core::ConstraintParams*, const helper::vector< InDataMatrixDeriv* >&, const helper::vector< const OutDataMatrixDeriv* >& ) {} // no yet implemented
    virtual void applyDJT( const core::MechanicalParams* /*mparams*/, core::MultiVecDerivId /*parentDfId*/, core::ConstMultiVecDerivId ) {} // no geometric stiffness

    virtual const helper::vector<sofa::defaulttype::BaseMatrix*>* getJs() { return &m_jacobians; }
    virtual const sofa::defaulttype::BaseMatrix* getK() { return  NULL; }


    void draw(const core::visual::VisualParams* vparams);
    virtual void updateForceMask();

    void resizeOut(size_t size);
    Data<helper::vector<defaulttype::Vec2i> > f_indexPairs;    ///< input pairs of correspondences
    Data< std::list<size_t> > d_validPairs; ///< optional list of valid index pairs


    typedef helper::ProjectToMesh<InCoord> ProjectToMesh;
    typedef typename ProjectToMesh::IndexToWeightPair IndexToWeightPair;
    typedef typename ProjectToMesh::ClosestStruct ClosestStruct;
    Data< helper::vector<ClosestStruct> > d_closests; ///< give the closest point on mesh from each parent1 point (see ProjectionToMeshEngine)

    Data<bool > d_handleBorder; ///< enable stiff (possibly sticky) projection on border?

    Data< Real > d_showObjectScale;     ///< drawing size
    Data< defaulttype::Vec4f > d_color; ///< drawing color

protected:

    core::State<TIn>* fromModel1;   ///< DOF of the master1
    core::State<TIn>* fromModel2;   ///< DOF of the master2
    core::State<Out>* toModel;      ///< DOF of the slave

    Jacobian m_jacobian1, m_jacobian2;
    helper::vector<defaulttype::BaseMatrix*> m_jacobians; ///< Jacobian of the mapping, in a vector
};

#if defined(SOFA_EXTERN_TEMPLATE) && !defined(SOFA_COMPONENT_MAPPING_ProjectionToMeshMultiMapping_CPP)
#ifndef SOFA_FLOAT
extern template class SOFA_ContactMapping_API ProjectionToMeshMultiMapping< sofa::defaulttype::Vec3dTypes, sofa::defaulttype::Vec3dTypes >;
#endif
#ifndef SOFA_DOUBLE
extern template class SOFA_ContactMapping_API ProjectionToMeshMultiMapping< sofa::defaulttype::Vec3fTypes, sofa::defaulttype::Vec3fTypes >;
#endif
#endif

} // namespace mapping

} // namespace component

} // namespace sofa

#endif //SOFA_COMPONENT_MAPPING_ProjectionToMeshMultiMapping_H

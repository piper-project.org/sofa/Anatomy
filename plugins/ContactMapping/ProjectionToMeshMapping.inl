/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_MAPPING_ProjectionToMeshMapping_INL
#define SOFA_COMPONENT_MAPPING_ProjectionToMeshMapping_INL

#include "ProjectionToMeshMapping.h"
#include <sofa/core/visual/VisualParams.h>
#include <sofa/helper/AdvancedTimer.h>

namespace sofa
{

namespace component
{

namespace mapping
{



template <class TIn, class TOut>
ProjectionToMeshMapping< TIn, TOut >::ProjectionToMeshMapping()
    : Inherit()
    , f_indexPairs(initData(&f_indexPairs, "indexPairs", "input list of closest point pairs (parent index, target mesh index)"))
    , d_validPairs(initData(&d_validPairs, "validPairs", "Set of valid index pairs"))
    , d_closests(initData(&d_closests,"closests","ClosestStruct computed by ProjectionToMeshEngine"))
    , d_meshPositions(initData(&d_meshPositions, "meshPositions", "Vertices position of the target mesh"))
    , d_handleBorder(initData(&d_handleBorder, false, "handleBorder", "enable stiff (possibly sticky) projection on border?"))
    , d_showObjectScale(initData(&d_showObjectScale, Real(0), "showObjectScale", "Scale for object display"))
    , d_color(initData(&d_color, defaulttype::Vec4f(1,1,0,1), "showColor", "Color for object display"))
{
    this->addAlias(&d_meshPositions, "position");
    // init jacobian
    m_jacobians.push_back(&m_jacobian);
}



template <class TIn, class TOut>
void ProjectionToMeshMapping< TIn, TOut >::resizeOut(size_t size)
{
    if((unsigned)this->toModel->getSize()!=size)
    {
//        sout<< "resizeOut="<<size<<sendl;

        this->toModel->resize(size);
        m_jacobian.resizeBlocks(size, this->fromModel->getSize());
    }
    else
    {
        m_jacobian.compressedMatrix.setZero();
    }
}


template <class TIn, class TOut>
void ProjectionToMeshMapping< TIn, TOut>::init()
{
    // check inputs
    if (d_closests.getValue().size() != f_indexPairs.getValue().size()) { serr << "have you computed 'closests' with a 'BaseProjectionToMeshEngine'?" << sendl; exit(EXIT_FAILURE); }

    reinit();
    Inherit::init();
}


template <class TIn, class TOut>
void ProjectionToMeshMapping< TIn, TOut>::reinit()
{
    this->toModel->resize(0);
    resizeOut(d_validPairs.getValue().empty() ? f_indexPairs.getValue().size() : d_validPairs.getValue().size());

    Inherit::reinit();
}



template <class TIn, class TOut>
void ProjectionToMeshMapping< TIn, TOut >::apply(const core::MechanicalParams*, OutDataVecCoord& outPosData, const InDataVecCoord& inPosData)
{
    helper::ScopedAdvancedTimer advancedTimer("ProjectionToMeshMapping");

    const helper::vector<defaulttype::Vec2i>& indexPairs = f_indexPairs.getValue();
    const helper::vector<ClosestStruct>& closests = d_closests.getValue();
    bool handleBorder = d_handleBorder.getValue();

    helper::IndexPairIterator indexPairIterator( indexPairs.size(), d_validPairs.getValue(), d_validPairs.isSet() );

    resizeOut(indexPairIterator.size());
    if(!indexPairIterator.size()) return;

    typename Jacobian::CompressedMatrix& J1 = m_jacobian.compressedMatrix;

    const InVecCoord& inPos = inPosData.getValue();
    const InVecCoord& meshPositions = d_meshPositions.getValue();
    OutVecCoord& outPos = *outPosData.beginWriteOnly();
    typename Jacobian::Block J;

    for ( ; indexPairIterator.count()<indexPairIterator.size() ; ++indexPairIterator )
    {
        const size_t i = indexPairIterator.get();
        const int iP = indexPairs[i][0];
        outPos[i] = inPos[iP] - closests[i].u*closests[i].dist;

        if( this->maskTo->isActivated() && !this->maskTo->getEntry(indexPairIterator.count()) ) continue;

        // compute jacobians
        if(handleBorder)   // select jacobian type (point/line/plane) based on the true projection to allow stiff projection on border
        {
            if(closests[i].indexWeightSet.size()==1) // projection to a point -> invariant when moving in any direction
                J.identity();
            else if(closests[i].indexWeightSet.size()==2)   // projection to a edge -> invariant when moving in the plane orthogonal to the edge
            {
                //get edge direction
                typename std::set<IndexToWeightPair>::iterator iwit=closests[i].indexWeightSet.begin();
                InCoord v = meshPositions[iwit->first];
                iwit++; v -= meshPositions[iwit->first];
                v.normalize();
                J.identity();
                J-= dyad(v,v);
            }
            else// projection to a triangle -> invariant when moving in normal direction
                J = dyad(closests[i].n,closests[i].n);
        }
        else // use the jacobian type (line/plane) of the primitive
        {
            if(closests[i].isPlane)   // projection to a triangle -> invariant when moving in normal
                J = dyad(closests[i].n,closests[i].n);
            else // projection to a edge -> invariant when moving in the plane orthogonal to the edge
            {
                J.identity();
                J -= dyad(closests[i].n,closests[i].n);
            }
        }


        for(unsigned j=0; j<Nout; j++)
        {
            unsigned row = i*Nout+j;
            J1.startVec( row );
            for(unsigned k=0; k<Nin; k++ )
            {
                if( j == k ) J1.insertBack( row, i*Nin+k ) =  1-J[j][k];
                else J1.insertBack( row, i*Nin+k ) =  -J[j][k];
            }
        }
    }

    outPosData.endEdit();

    J1.finalize();
}


template <class TIn, class TOut>
void ProjectionToMeshMapping< TIn, TOut >::draw(const core::visual::VisualParams* vparams)
{
    if( !vparams->displayFlags().getShowMechanicalMappings() ) return;
    const Real& showObjectScale =  d_showObjectScale.getValue();
    if( showObjectScale == 0 ) return;

    const OutVecCoord& outPos = this->toModel->read(core::ConstVecCoordId::position())->getValue();

    helper::vector< defaulttype::Vector3 > points;
    for(size_t i=0; i<outPos.size(); i++ ) points.push_back( defaulttype::Vector3( Out::getCPos(outPos[i]) ) );
    vparams->drawTool()->drawSpheres( points, showObjectScale, d_color.getValue() );
}



template <class TIn, class TOut>
void ProjectionToMeshMapping<TIn, TOut>::updateForceMask()
{
    const helper::vector<defaulttype::Vec2i>& indexPairs = f_indexPairs.getValue();
    const std::list<size_t>& validPairs = d_validPairs.getValue();

    for( helper::IndexPairIterator indexPairIterator( indexPairs.size(), validPairs, d_validPairs.isSet() ) ; indexPairIterator.count()<indexPairIterator.size() ; ++indexPairIterator )
    {
        if( this->maskTo->getEntry(indexPairIterator.count()) )
        {
            const size_t i = indexPairIterator.get();
            const int iP = indexPairs[i][0];
            this->maskFrom->insertEntry( iP );
        }
    }
}



} // namespace mapping

} // namespace component

} // namespace sofa

#endif // SOFA_COMPONENT_MAPPING_ProjectionToMeshMapping_INL


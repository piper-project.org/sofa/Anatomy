/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_MAPPING_ClosestKeepNegativeEngine_H
#define SOFA_COMPONENT_MAPPING_ClosestKeepNegativeEngine_H

#include "initContactMapping.h"
#include <sofa/core/DataEngine.h>
#include <sofa/defaulttype/VecTypes.h>
#include <sofa/core/topology/BaseMeshTopology.h>
#include "projectToMesh.h"

namespace sofa
{

namespace component
{

namespace engine
{

/**
 * Filtering a vector of ClosestStruct (typically the output of a ProjectToMeshEngine)
 * by keeping only the Negative distances
 *
 * @author Matthieu Nesme
 * @date 2017
 */
template <class DataTypes>
class ClosestKeepNegativeEngine : public core::DataEngine
{
public:
    SOFA_CLASS(SOFA_TEMPLATE(ClosestKeepNegativeEngine, DataTypes), core::DataEngine);


    typedef typename DataTypes::Coord InCoord;
    typedef typename DataTypes::Real Real;
    typedef helper::ProjectToMesh<InCoord> ProjectToMesh;
    typedef typename ProjectToMesh::IndexToWeightPair IndexToWeightPair;
    typedef typename ProjectToMesh::ClosestStruct ClosestStruct;
    typedef typename core::topology::BaseMeshTopology::index_type index_type;

    virtual std::string getTemplateName() const { return templateName(this); }
    static std::string templateName(const ClosestKeepNegativeEngine<DataTypes>* = NULL) { return DataTypes::Name(); }

protected:

    ClosestKeepNegativeEngine();

    virtual ~ClosestKeepNegativeEngine() {}

public:

    virtual void init();
    virtual void reinit();

    virtual void update();

    Data< helper::vector<ClosestStruct> > i_input; ///< closest target point from each source point

    Data<helper::vector<defaulttype::Vec2i> > f_indexPairs;    ///< input pairs of correspondences
    Data< std::list<size_t> > d_validPairs; ///< optional list of valid index pairs

    Data< std::list<size_t> > o_output; ///< list of kept indices

};

#if defined(SOFA_EXTERN_TEMPLATE) && !defined(SOFA_COMPONENT_MAPPING_ClosestKeepNegativeEngine_CPP)
#ifndef SOFA_FLOAT
extern template class SOFA_ContactMapping_API ClosestKeepNegativeEngine< sofa::defaulttype::Vec3dTypes >;
#endif
#ifndef SOFA_DOUBLE
extern template class SOFA_ContactMapping_API ClosestKeepNegativeEngine< sofa::defaulttype::Vec3fTypes >;
#endif
#endif

} // namespace engine

} // namespace component

} // namespace sofa

#endif //SOFA_COMPONENT_MAPPING_ClosestKeepNegativeEngine_H

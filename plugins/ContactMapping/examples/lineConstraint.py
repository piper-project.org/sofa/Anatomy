import Sofa

import SofaPython.Tools
import Compliant.StructuralAPI
import ContactMapping.StructuralAPI

# Note the target spline has been generated using the SplineEngine from Anatomy plugin
# For this scene not to depend on Anatomy the spline points are hard-coded

nbLink = 10
linkSize = 2

def createElasticLine(node, contactObject, color=[1,1,1,1]):
    contactObject.node = node
    linePositions = []
    for i in xrange(nbLink):
        linePositions.append([0, -1.*linkSize * i, 0])
    contactObject.dofs = node.createObject("MechanicalObject", template="Vec3", name="dofs", position=SofaPython.Tools.listListToStr(linePositions), showObject=True, drawMode=1, showObjectScale=0.5, showColor=SofaPython.Tools.listToStr(color))
    node.createObject("UniformMass", name="mass", mass="1")
    node.createObject("FixedConstraint", indices="0")
    edges = ""
    for i in xrange(nbLink-1):
        edges += str(i) + " " + str(i+1) + " "
    contactObject.topology = node.createObject("MeshTopology", name="topology", position="@dofs.position", edges=edges)
    node.createObject("MeshSpringForceField", name="ff", linesStiffness=1e6, drawMode=1)

def createSceneAndController(rootNode):
    
    rootNode.createObject('RequiredPlugin', pluginName = 'Compliant')
    rootNode.createObject('RequiredPlugin', pluginName = 'ContactMapping')
    #rootNode.createObject('RequiredPlugin', pluginName = 'Anatomy')
    
    rootNode.createObject('VisualStyle', displayFlags="showForceFields")
    rootNode.createObject('CompliantImplicitSolver', name='odesolver', stabilization="true")
    
    rootNode.createObject('SequentialSolver', name='numsolver', iterations=250, precision=1e-14, iterateOnBilaterals=True)
    rootNode.createObject('LDLTResponse', name='response')
    
    # create the line
    line1Node = rootNode.createChild("line1")
    line1ContactObject = ContactMapping.StructuralAPI.ContactObject()
    createElasticLine(line1Node, line1ContactObject, [1,1,1,1])
    
    line2Node = rootNode.createChild("line2")
    line2ContactObject = ContactMapping.StructuralAPI.ContactObject()
    createElasticLine(line2Node, line2ContactObject, [1,1,0.5,1])
        
    # create line shape target
    lineShapeContactObject = ContactMapping.StructuralAPI.ContactObject()
    targetNode = rootNode.createChild("targetShape")
    lineShapeContactObject.node = targetNode
    splinePoints = [[0.0, 0.0, 0.0], [0.17997600138187408, -0.20000001788139343, 0.0], [0.35980796813964844, -0.3999999761581421, 0.0], [0.5393519401550293, -0.6000000238418579, 0.0], [0.7184640169143677, -0.800000011920929, 0.0], [0.8969999551773071, -1.0, 0.0], [1.0748159885406494, -1.1999999284744263, 0.0], [1.2517681121826172, -1.4000000953674316, 0.0], [1.427712082862854, -1.6000001430511475, 0.0], [1.602504014968872, -1.8000001907348633, 0.0], [1.7760000228881836, -2.0, 0.0], [1.9480558633804321, -2.1999998092651367, 0.0], [2.11852765083313, -2.4000000953674316, 0.0], [2.2872719764709473, -2.5999999046325684, 0.0], [2.454144239425659, -2.8000001907348633, 0.0], [2.61899995803833, -3.000000476837158, 0.0], [2.78169584274292, -3.200000286102295, 0.0], [2.9420881271362305, -3.4000000953674316, 0.0], [3.100032329559326, -3.6000001430511475, 0.0], [3.2553839683532715, -3.8000001907348633, 0.0], [3.4079999923706055, -4.000000476837158, 0.0], [3.5577361583709717, -4.200000286102295, 0.0], [3.7044477462768555, -4.400000095367432, 0.0], [3.847991704940796, -4.599999904632568, 0.0], [3.9882235527038574, -4.799999713897705, 0.0], [4.125, -5.0, 0.0], [4.258175849914551, -5.199999809265137, 0.0], [4.387608528137207, -5.40000057220459, 0.0], [4.513152122497559, -5.600000381469727, 0.0], [4.6346635818481445, -5.799999713897705, 0.0], [4.751999855041504, -6.000000953674316, 0.0], [4.865015983581543, -6.200000286102295, 0.0], [4.973567962646484, -6.400000095367432, 0.0], [5.077512264251709, -6.600000381469727, 0.0], [5.176703929901123, -6.800000190734863, 0.0], [5.270999908447266, -7.0, 0.0], [5.360256195068359, -7.199999809265137, 0.0], [5.444328308105469, -7.400000095367432, 0.0], [5.523071765899658, -7.600000381469727, 0.0], [5.596343994140625, -7.799999237060547, 0.0], [5.664000034332275, -8.0, 0.0], [5.725895881652832, -8.19999885559082, 0.0], [5.781888484954834, -8.399999618530273, 0.0], [5.831831455230713, -8.600000381469727, 0.0], [5.875583648681641, -8.800000190734863, 0.0], [5.912999629974365, -9.0, 0.0], [5.943935871124268, -9.199999809265137, 0.0], [5.968247413635254, -9.399998664855957, 0.0], [5.9857916831970215, -9.59999942779541, 0.0], [5.996423721313477, -9.800000190734863, 0.0], [6.0, -10.0, 0.0], [5.996423721313477, -10.199999809265137, 0.0], [5.98579216003418, -10.399999618530273, 0.0], [5.96824836730957, -10.59999942779541, 0.0], [5.943935871124268, -10.800000190734863, 0.0], [5.912999629974365, -11.000000953674316, 0.0], [5.875583648681641, -11.199999809265137, 0.0], [5.831831455230713, -11.400001525878906, 0.0], [5.781887531280518, -11.600000381469727, 0.0], [5.725895881652832, -11.800000190734863, 0.0], [5.664000034332275, -12.000000953674316, 0.0], [5.596343994140625, -12.200000762939453, 0.0], [5.523071765899658, -12.40000057220459, 0.0], [5.444328308105469, -12.600000381469727, 0.0], [5.360256195068359, -12.800000190734863, 0.0], [5.270999908447266, -13.0, 0.0], [5.176703929901123, -13.200000762939453, 0.0], [5.077512264251709, -13.40000057220459, 0.0], [4.973567962646484, -13.600000381469727, 0.0], [4.865015983581543, -13.80000114440918, 0.0], [4.751999855041504, -14.0, 0.0], [4.634664535522461, -14.19999885559082, 0.0], [4.5131516456604, -14.400001525878906, 0.0], [4.387607574462891, -14.600000381469727, 0.0], [4.258175849914551, -14.80000114440918, 0.0], [4.125, -15.000000953674316, 0.0], [3.9882240295410156, -15.200000762939453, 0.0], [3.847992420196533, -15.40000057220459, 0.0], [3.7044484615325928, -15.59999942779541, 0.0], [3.5577359199523926, -15.80000114440918, 0.0], [3.4079999923706055, -16.0, 0.0], [3.2553839683532715, -16.200000762939453, 0.0], [3.100032329559326, -16.399999618530273, 0.0], [2.9420881271362305, -16.600000381469727, 0.0], [2.7816965579986572, -16.799999237060547, 0.0], [2.618999481201172, -17.0, 0.0], [2.454143762588501, -17.200000762939453, 0.0], [2.2872719764709473, -17.399999618530273, 0.0], [2.118528127670288, -17.60000228881836, 0.0], [1.9480562210083008, -17.799999237060547, 0.0], [1.7760002613067627, -18.000001907348633, 0.0], [1.6025035381317139, -18.200000762939453, 0.0], [1.4277117252349854, -18.400001525878906, 0.0], [1.251767873764038, -18.60000228881836, 0.0], [1.0748159885406494, -18.799999237060547, 0.0], [0.8970001935958862, -19.0, 0.0], [0.7184643745422363, -19.200000762939453, 0.0], [0.5393515229225159, -19.400001525878906, 0.0], [0.35980767011642456, -19.600000381469727, 0.0], [0.17997583746910095, -19.799999237060547, 0.0], [0.0, -20.0, 0.0]]
    #shapePoints = [[0,0,0],[3*linkSize, -0.5*linkSize*nbLink, 0], [0, -1.*linkSize*nbLink, 0]]
    #global splineEngine
    #splineEngine = targetNode.createObject("SplineEngine", template="Vec3", name="spline", useBezier=True, initSpline=SofaPython.Tools.listListToStr(shapePoints), ds=0.01)
    #lineShapeContactObject.topology = targetNode.createObject("MeshTopology", name="topology", position="@spline.position", edges="@spline.edges", drawEdges=True)
    edges = ""
    for i in xrange(len(splinePoints)-1):
        edges += str(i)+" "+str(i+1)+" "
    lineShapeContactObject.topology = targetNode.createObject("MeshTopology", name="topology", position=SofaPython.Tools.listListToStr(splinePoints), edges=edges, drawEdges=False)
    lineShapeContactObject.visual = targetNode.createObject("VisualModel", position="@topology.position", edges="@topology.edges", color="0 1 0 1")
    #lineShapeContactObject.dofs = targetNode.createObject("MechanicalObject", template="Vec3", name="dofs", position="@spline.position", showObject=True, drawMode=1, showObjectScale=0.05, showColor="0 1 0 1")
    #targetNode.createObject("FixedConstraint", fixAll=True)
    
    # constraint - restLength > 0
    ContactMapping.StructuralAPI.Contact("line1Constraint", line1ContactObject, lineShapeContactObject, compliance=1e-3, isCompliance=False, restLength=0.2, useTangentPlaneProjectionMethod=False, rejectBorder=False, isObject2Polyline=True, N=nbLink)
    # constraint - restLength == 0
    ContactMapping.StructuralAPI.Contact("line2Constraint", line2ContactObject, lineShapeContactObject, compliance=1e-3, isCompliance=False, restLength=0., useTangentPlaneProjectionMethod=False, rejectBorder=False, isObject2Polyline=True, N=nbLink)
    
#def bwdInitGraph(node):
    #print splineEngine.position
    
    
import Sofa
import sys

def createSceneAndController(rootNode):
    rootNode.createObject('RequiredPlugin', pluginName="ContactMapping")

    rootNode.createObject('BackgroundSetting',color='1 1 1')
    rootNode.createObject('OrderIndependentTransparencyManager')

    rootNode.createObject('MeshObjLoader', name='cube', filename='mesh/cube.obj', scale=2 )
    rootNode.createObject('VisualModel', src="@cube", color="5E-1 8E-1 5E-1 5E-1")

    # one direction
    rootNode.createObject('MeshObjLoader', name='sphere', filename='mesh/sphere.obj', translation="3 3 3", triangulate='1')
    global engine
    engine = rootNode.createObject('RayMeshIntersectionEngine', name="engine", template="Vec3", sourcePosition="@sphere.position", method=1, directions="-3 -3 -3",
                          targetPosition="@cube.position", triangles="@cube.triangles", quads="@cube.quads", maximizeDistances=False )
    rootNode.createObject("MechanicalObject", name="visu", template="Vec3", position="@engine.projections", showColor="1 0 0 1", showObject=True, drawMode=1, showObjectScale=0.05)

    # rootNode.createObject('VisualModel', src="@sphere", color="1 0 0 1")
    n = rootNode.createChild("Interpolation")
    n.createObject('ClosestPointInterpolationEngine', name="interp", template="Vec3", pointData="0 0 0 0 0 1 0 0", defaultValue=-100, closests="@../engine.closests")
    n.createObject('MeshTopology', name='topology', position="@../sphere.position", triangles="@../sphere.triangles")
    n.createObject('DataDisplay', name="dataDisplay", pointData="@interp.values", maximalRange=False, position="@topology.position")
    n.createObject('ColorMap', colorScheme="Blue to Red", showLegend=True, min="@dataDisplay.currentMin", max="@dataDisplay.currentMax", legendTitle="Score", legendRangeScale=1 )

    # multiple directions
    rootNode.createObject('MeshObjLoader', name='sphere2', filename='mesh/sphere.obj', translation="0 0 0", triangulate='1')
    rootNode.createObject('RayMeshIntersectionEngine', name="engine2", template="Vec3", sourcePosition="@sphere2.position", method=1, directions="@sphere2.position",
                          targetPosition="@cube.position", triangles="@cube.triangles", quads="@cube.quads", maximizeDistances=False )
    rootNode.createObject("MechanicalObject", name="visu2", template="Vec3", position="@engine2.projections", showColor="0 0 1 1", showObject=True, drawMode=1, showObjectScale=0.05)

    rootNode.createObject('VisualModel', src="@sphere2", color="0 0 1 1")
    n = rootNode.createChild("Interpolation2")
    n.createObject('ClosestPointInterpolationEngine', name="interp", template="Vec3", pointData="0 0 0 0 0 1 0 0", defaultValue=-100, closests="@../engine2.closests")
    n.createObject('MeshTopology', name='topology', position="@../sphere2.position", triangles="@../sphere2.triangles")
    n.createObject('DataDisplay', name="dataDisplay", pointData="@interp.values", maximalRange=False, position="@topology.position")
    n.createObject('ColorMap', colorScheme="Blue to Red", showLegend=True, min="@dataDisplay.currentMin", max="@dataDisplay.currentMax", legendTitle="Score", legendRangeScale=1 )

    return rootNode


def bwdInitGraph(node):
    print engine.output.indexWeights
    sys.stdout.flush()

import Sofa

def createScene(rootNode):
    rootNode.createObject('RequiredPlugin', pluginName="Compliant")
    rootNode.createObject('RequiredPlugin', pluginName="ContactMapping")
    
    rootNode.createObject('VisualStyle', displayFlags='showMechanicalMappings')

    rootNode.dt=0.03
    rootNode.gravity=[0,0,-9]

    rootNode.createObject('CompliantAttachButtonSetting', compliance=0.001)
    
    rootNode.createObject('CompliantImplicitSolver', name="odesolver", stabilization=True)
#    rootNode.createObject('CgSolver', name="numsolver", iterations =250, precision=1e-14)
    rootNode.createObject('SequentialSolver', name="numsolver", iterations =250, precision=1e-14)
#    rootNode.createObject('LUSolver', name="numsolver")

#    rootNode.createObject('EulerImplicit',rayleighStiffness='0',rayleighMass='0')
#    rootNode.createObject('CGLinearSolver', iterations=250, tolerance=1e-14, threshold=1e-14)

    plane = rootNode.createChild("plane")
    plane.createObject('RegularGrid', nx=10, ny=10, nz=1, xmin=-10, xmax=10, ymin=-10, ymax=10, zmin=0, zmax=0)
#    triangles = plane.createChild("triangles")
#    triangles.createObject('TriangleSetTopologyContainer', name="Container")
#    triangles.createObject('TriangleSetTopologyModifier', name="Topology, Modifier")
#    triangles.createObject('Quad2TriangleTopologicalMapping')
    plane.createObject('MechanicalObject', template="Vec3d", name="dof", position="@RegularGrid.position")
    plane.createObject('NormalsFromPoints', name="NormalsFromPoints", template="Vec3d", position="@dof.position", triangles="@RegularGrid.triangles", quads="@RegularGrid.quads")
    plane.createObject('UniformMass', name="Mass", totalmass="1")
    plane.createObject('FixedConstraint', fixAll="True")
    planeVisu = plane.createChild("visu")
    planeVisu.createObject('OglModel')
    planeVisu.createObject('IdentityMapping')
    
    cube = rootNode.createChild("cube")
    cube.createObject('RegularGrid', name="topology", nx=5, ny=5, nz=5, xmin=-0.5, xmax=0.5, ymin=-0.5, ymax=0.5, zmin=1.2, zmax=2.2)
    cube.createObject('MechanicalObject', template="Vec3d", name="dof", position="@topology.position")
    cube.createObject('BoxROI', template="Vec3d", name="contactRoi", box="-0.6 -0.6 1.1 0.6 0.6 1.21", drawBoxes=True)
    cube.createObject('UniformMass', name="Mass", totalmass=1)
    cube.createObject('HexahedronFEMForceField', name="FEM", youngModulus=100, poissonRatio=0)
    cube.createObject('UniformVelocityDampingForceField',dampingCoefficient='0.01')

    distance = cube.createChild("distance")
    distance.createObject('ClosestPointEngine', name="cp", sourcePosition="@/cube/dof.position", targetPosition="@/plane/RegularGrid.position", sourceROI="@/cube/contactRoi.indices",  printLog=False,drawMode="0")
    distance.createObject('MechanicalObject', template="Vec1d", name="dof")
    distance.createObject('ProjectionToVertexPlaneEngine', name="pm",
                        sourcePosition="@/cube/dof.position",
                        targetPosition="@/plane/RegularGrid.position",
                        indexPairs="@cp.indexPairs" ,
                        normals2="@plane/NormalsFromPoints.normals",
                        useRestLength=True, restLength=0.2 )
    distance.createObject('DistanceToMeshMultiMapping', template="Vec3d,Vec1d", name="mapping", input="@/cube/. @/plane/.", output="@.", indexPairs="@cp.indexPairs", closests="@pm.output", printLog=False)
#    distance.createObject('PenetrationForceField', hardStiffness="10", separatingStiffness="1.", proximity="0.1",damping="0",isCompliance='0')
    distance.createObject('UniformCompliance', compliance=1E-10,isCompliance='1')
    distance.createObject('UnilateralConstraint')
#    distance.createObject('ConstraintValue')
#    distance.createObject('Stabilization')
    plane.addChild(distance)

    cubeVisu = cube.createChild("visu")
    cubeVisu.createObject('OglModel', color="green")
    cubeVisu.createObject('IdentityMapping')
    
    return rootNode

import Sofa

def createScene(rootNode):
    rootNode.gravity=[0,0,0]
    rootNode.createObject('RequiredPlugin', pluginName="Compliant")
    rootNode.createObject('RequiredPlugin', pluginName="ContactMapping")
    
    rootNode.createObject('VisualStyle', displayFlags='showMechanicalMappings')

    rootNode.createObject('CompliantAttachButtonSetting', compliance=0.001)
    
    rootNode.createObject('CompliantImplicitSolver', name="odesolver", stabilization=True)
#    rootNode.createObject('SequentialSolver', name="numsolver", iterations =250, precision=1e-14)
    rootNode.createObject('LDLTSolver', name="numsolver")

    plane = rootNode.createChild("plane")
    plane.createObject('MechanicalObject', template="Vec3d", name="dof", position="0 0 0  2 0 0  0 2 0", showObject="True")
    plane.createObject('MeshTopology', name="Topology", triangles="0 1 2")
    plane.createObject('NormalsFromPoints', name="NormalsFromPoints", template="Vec3d", position="@dof.position", triangles="@Topology.triangles")	
    plane.createObject('UniformMass', name="Mass", totalmass="1")
    plane.createObject('FixedConstraint', fixAll="True")
    
    planeVisu = plane.createChild("visu")
    planeVisu.createObject('OglModel', color="red")
    planeVisu.createObject('IdentityMapping')
    
    point = rootNode.createChild("point")
    point.createObject('MechanicalObject', template="Vec3d", name="dof", position="0.5 0.5 -1  0.75 0.75 -1", showObject=True, showObjectScale=0.05, drawMode=1)
    point.createObject('UniformMass', name="Mass", totalmass=1)
    distance = point.createChild("distance")
    
    distance.createObject('ClosestPointEngine', name="cp", sourcePosition="@/point/dof.position", targetPosition="@/plane/dof.position", printLog=False,drawMode="1")
    distance.createObject('MechanicalObject', template="Vec1d", name="dof")
    distance.createObject('ProjectionToVertexPlaneEngine', name="pm", printLog=True,
                            sourcePosition="@/point/dof.position", targetPosition="@/plane/dof.position",
                            indexPairs="@cp.indexPairs" ,
                            normals2="@plane/NormalsFromPoints.normals",
                            useRestLength=True )
    distance.createObject('DistanceToMeshMultiMapping', template="Vec3d,Vec1d", name="mapping", input="@/point/. @/plane/.", output="@.", indexPairs="@cp.indexPairs", closests="@pm.output", printLog=True)
    distance.createObject('UniformCompliance', compliance=0)
    distance.createObject('Stabilization')
    plane.addChild(distance)
    
    return rootNode

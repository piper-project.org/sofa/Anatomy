import Sofa

def createCube(parentNode, color):
    cube = parentNode.createChild("cube_"+color)
    cube.createObject('RegularGrid', name="topology", nx=5, ny=5, nz=5, xmin=-0.5, xmax=0.5, ymin=-0.5, ymax=0.5, zmin=1.2, zmax=2.2)
    cube.createObject('MechanicalObject', template="Vec3d", name="dof", position="@topology.position")
    cube.createObject('BoxROI', template="Vec3d", name="contactRoi", box="-0.6 -0.6 1.1 0.6 0.6 1.21", drawBoxes=True)
    cube.createObject('UniformMass', name="Mass", totalmass=1)
    cube.createObject('HexahedronFEMForceField', name="FEM", youngModulus=100, poissonRatio=0)
    cube.createObject('UniformVelocityDampingForceField',dampingCoefficient='0.01')
    cubeVisu = cube.createChild("visu")
    cubeVisu.createObject('OglModel', color=color)
    cubeVisu.createObject('IdentityMapping')
    return cube

def createScene(rootNode):
    rootNode.createObject('RequiredPlugin', pluginName="Compliant")
    rootNode.createObject('RequiredPlugin', pluginName="ContactMapping")
    
    rootNode.createObject('VisualStyle', displayFlags='showMechanicalMappings')

    rootNode.dt=0.03
    rootNode.gravity=[0,0,-9]

    rootNode.createObject('CompliantAttachButtonSetting', compliance=0.001)
    
    rootNode.createObject('CompliantImplicitSolver', name="odesolver", stabilization=True)
    rootNode.createObject('CgSolver', name="numsolver", iterations =250, precision=1e-14)
#    rootNode.createObject('SequentialSolver', name="numsolver", iterations =250, precision=1e-14)
#    rootNode.createObject('LUSolver', name="numsolver")

#    rootNode.createObject('EulerImplicit',rayleighStiffness='0',rayleighMass='0')
#    rootNode.createObject('CGLinearSolver', iterations=250, tolerance=1e-14, threshold=1e-14)

    plane = rootNode.createChild("plane")
    plane.createObject('RegularGrid', nx=10, ny=10, nz=1, xmin=-10, xmax=10, ymin=-10, ymax=10, zmin=0, zmax=0)
#    triangles = plane.createChild("triangles")
#    triangles.createObject('TriangleSetTopologyContainer', name="Container")
#    triangles.createObject('TriangleSetTopologyModifier', name="Topology, Modifier")
#    triangles.createObject('Quad2TriangleTopologicalMapping')
    plane.createObject('MeshBoundaryROI', name="ROI", triangles="@RegularGrid.triangles", quads="@RegularGrid.quads")
    plane.createObject('MechanicalObject', template="Vec3d", name="dof", position="@RegularGrid.position")
    plane.createObject('NormalsFromPoints', name="NormalsFromPoints", template="Vec3d", position="@dof.position", triangles="@RegularGrid.triangles", quads="@RegularGrid.quads")
    plane.createObject('UniformMass', name="Mass", totalmass="1")
    plane.createObject('FixedConstraint', fixAll="True")
    planeVisu = plane.createChild("visu")
    planeVisu.createObject('OglModel')
    planeVisu.createObject('IdentityMapping')
    
    # Green cube useTangentPlaneProjectionMethod=True
    cube_green = createCube(rootNode, "green")
    distance_green = cube_green.createChild("distance")
    distance_green.createObject('ClosestPointEngine', name="cp", sourcePosition="@/cube_green/dof.position", targetPosition="@/plane/RegularGrid.position", sourceROI="@/cube_green/contactRoi.indices", rejectROI="@/plane/ROI.indices" , printLog=False,drawMode="0")
    distance_green.createObject('MechanicalObject', template="Vec1d", name="dof")
    distance_green.createObject('ProjectionToVertexPlaneEngine', name="pm",
                                sourcePosition="@/cube_green/dof.position",
                                targetPosition="@/plane/RegularGrid.position",
                                indexPairs="@cp.indexPairs" ,
                                normals2="@plane/NormalsFromPoints.normals",
                                useRestLength=True, restLength=0.2 )
    distance_green.createObject('DistanceToMeshMultiMapping', template="Vec3d,Vec1d", name="mapping", input="@/cube_green/. @/plane/.", output="@.", indexPairs="@cp.indexPairs", closests="@pm.output", printLog=True)
#    distance_green.createObject('PenetrationForceField', hardStiffness="10", separatingStiffness="1.", proximity="0.1",damping="0",isCompliance='0')
    distance_green.createObject('UniformCompliance', compliance=1E-10,isCompliance='1', resizable=True)
#    distance_green.createObject('UnilateralConstraint')
#    distance_green.createObject('ConstraintValue')
#    distance_green.createObject('Stabilization')
    plane.addChild(distance_green)

    # Red cube useTangentPlaneProjectionMethod=False
    cube_red = createCube(rootNode, "red")
    distance_red = cube_red.createChild("distance")
    distance_red.createObject('ClosestPointEngine', name="cp", sourcePosition="@/cube_red/dof.position", targetPosition="@/plane/RegularGrid.position", sourceROI="@/cube_red/contactRoi.indices", printLog=False,drawMode="0")
    distance_red.createObject('MechanicalObject', template="Vec1d", name="dof")
    distance_red.createObject('ProjectionToMeshEngine', name="pm",
                            sourcePosition="@/cube_red/dof.position",
                            targetPosition="@/plane/RegularGrid.position",
                            indexPairs="@cp.indexPairs" ,
                            quads2="@plane/RegularGrid.quads",
                            useRestLength=True, restLength=0.2 )
    distance_red.createObject('ClosestRejectROIEngine', name="closestRejectROIEngine", closests="@pm.output", rejectROI="@/plane/ROI.indices"  )

    distance_red.createObject('DistanceToMeshMultiMapping', template="Vec3d,Vec1d", name="mapping", input="@/cube_red/. @/plane/.", output="@.", indexPairs="@cp.indexPairs", validPairs="@closestRejectROIEngine.output", closests="@pm.output", printLog=True)
    distance_red.createObject('UniformCompliance', compliance=1E-10,isCompliance='1', resizable=True)
    plane.addChild(distance_red)


    return rootNode

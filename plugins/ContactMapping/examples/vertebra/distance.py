import Sofa

import Compliant.StructuralAPI
import ContactMapping.StructuralAPI

def createScene(rootNode):
    rootNode.createObject('RequiredPlugin', pluginName="ContactMapping")
    rootNode.createObject('RequiredPlugin', pluginName="Compliant")
    
    rootNode.createObject('VisualStyle', displayFlags='showMechanicalMappings')
    
    L3 = Compliant.StructuralAPI.RigidBody(rootNode, "L3")
    L3.setFromMesh("Third_lumbar_vertebra.obj")
    L3.addCollisionMesh("Third_lumbar_vertebra.obj")
    L3.collision.addVisualModel()
    
    L4 = Compliant.StructuralAPI.RigidBody(rootNode, "L4")
    L4.setFromMesh("Fourth_lumbar_vertebra.obj")
    L4.addCollisionMesh("Fourth_lumbar_vertebra.obj")
    L4.collision.addVisualModel()
    
    contact = ContactMapping.StructuralAPI.Contact("contact", L4.collision, L3.collision, compliance=0, restLength=0, N=10, bilateral=False, useTangentPlaneProjectionMethod=False)
    #contact = ContactMapping.StructuralAPI.Contact("contact", L3.collision, L4.collision, compliance=0, restLength=0, N=10, bilateral=False)
    
    return rootNode
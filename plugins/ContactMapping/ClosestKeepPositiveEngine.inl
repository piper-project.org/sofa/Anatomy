/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/

#include "ClosestKeepPositiveEngine.h"



namespace sofa
{

namespace component
{

namespace engine
{




template<class T>
ClosestKeepPositiveEngine<T>::ClosestKeepPositiveEngine()
    : Inherit1()
    , i_input(initData(&i_input,"input","closest primitives with weights, normal, distance"))
    , f_indexPairs (initData(&f_indexPairs, "indexPairs", "input list of closest point pairs (parent1 index, parent2 index)"))
    , d_validPairs(initData(&d_validPairs, "validPairs", "Set of valid index pairs"))
    , o_output(initData(&o_output,"output","list of kept indices"))
{
    this->addAlias(&i_input, "closests");
}




template<class T>
void ClosestKeepPositiveEngine<T>::init()
{
    addInput(&i_input);
    addInput(&f_indexPairs);
    addInput(&d_validPairs);

    addOutput(&o_output);

    reinit();
    Inherit1::init();
}


template<class T>
void ClosestKeepPositiveEngine<T>::reinit()
{
    Inherit1::reinit();

    o_output.setDirtyValue();
//    update();
}



template<class T>
void ClosestKeepPositiveEngine<T>::update()
{
    const std::vector<ClosestStruct>& closests = i_input.getValue();
    const helper::vector<defaulttype::Vec2i>& indexPairs = f_indexPairs.getValue();
    const std::list<size_t>& validPairs = d_validPairs.getValue();

    cleanDirty();

    std::list<size_t>& output = *o_output.beginWriteOnly();
    output.clear();

    for ( helper::IndexPairIterator indexPairIterator( indexPairs.size(), validPairs, d_validPairs.isSet() ) ;
          indexPairIterator.count()<indexPairIterator.size() ;
          ++indexPairIterator )
    {
        const size_t i = indexPairIterator.get();
        if( closests[i].dist >= 0 )
            output.push_back(i);
    }

    o_output.endEdit();
}



} // namespace engine

} // namespace component

} // namespace sofa


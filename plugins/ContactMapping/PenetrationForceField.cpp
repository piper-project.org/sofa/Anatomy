#include "PenetrationForceField.inl"
#include <sofa/defaulttype/VecTypes.h>
#include <sofa/core/ObjectFactory.h>

namespace sofa
{
namespace component
{
namespace forcefield
{

using namespace sofa::defaulttype;

// Register in the Factory
int PenetrationForceFieldClass = core::RegisterObject("piece-wise linear stiffness for oriented distances")
#ifndef SOFA_FLOAT
        .add< PenetrationForceField< Vec1dTypes > >(true)
#endif
#ifndef SOFA_DOUBLE
        .add< PenetrationForceField< Vec1fTypes > >()
#endif
        ;

SOFA_DECL_CLASS(PenetrationForceField)

#ifndef SOFA_FLOAT
template class SOFA_ContactMapping_API PenetrationForceField<Vec1dTypes>;
#endif
#ifndef SOFA_DOUBLE
template class SOFA_ContactMapping_API PenetrationForceField<Vec1fTypes>;
#endif

}
}
}

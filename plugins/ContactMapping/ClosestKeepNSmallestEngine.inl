/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/

#include "ClosestKeepNSmallestEngine.h"



namespace sofa
{

namespace component
{

namespace engine
{




template<class T>
ClosestKeepNSmallestEngine<T>::ClosestKeepNSmallestEngine()
    : Inherit1()
    , i_input(initData(&i_input,"input","closest primitives with weights, normal, distance"))
    , d_keepNSmallest(initData(&d_keepNSmallest, 0u, "keepNSmallest", "Keep only the N smallest outputs (all if =0)"))
    , d_bilateral(initData(&d_bilateral, true, "bilateral", "if true, absolute ouput values are used when keeping the N smallest ones"))
    , f_indexPairs (initData(&f_indexPairs, "indexPairs", "input list of closest point pairs (parent1 index, parent2 index)"))
    , d_validPairs(initData(&d_validPairs, "validPairs", "Set of valid index pairs"))
    , o_output(initData(&o_output,"output","list of kept indices"))
{
    this->addAlias(&i_input, "closests");
}




template<class T>
void ClosestKeepNSmallestEngine<T>::init()
{
    addInput(&i_input);
    addInput(&d_keepNSmallest);
    addInput(&d_bilateral);
    addInput(&f_indexPairs);
    addInput(&d_validPairs);

    addOutput(&o_output);

    reinit();
    Inherit1::init();
}


template<class T>
void ClosestKeepNSmallestEngine<T>::reinit()
{
    Inherit1::reinit();

    o_output.setDirtyValue();
//    update();
}



template<class T>
void ClosestKeepNSmallestEngine<T>::update()
{
    const std::vector<ClosestStruct>& closests = i_input.getValue();
    unsigned keepNSmallest = d_keepNSmallest.getValue();
    bool bilateral = d_bilateral.getValue();
    const helper::vector<defaulttype::Vec2i>& indexPairs = f_indexPairs.getValue();
    const std::list<size_t>& validPairs = d_validPairs.getValue();

    cleanDirty();

    std::list<size_t>& output = *o_output.beginWriteOnly();

    output.clear();


    typedef std::pair<Real,index_type> DistanceToPoint;
    typedef std::multiset<DistanceToPoint> DistanceSet;
    DistanceSet dset; // ordered set of contacts (only valid if d_keepNSmallest)

    // sort by distance
    helper::IndexPairIterator indexPairIterator( indexPairs.size(), validPairs, d_validPairs.isSet() );
    for ( ; indexPairIterator.count()<indexPairIterator.size() ; ++indexPairIterator ) {
        const size_t i = indexPairIterator.get();

        Real d = closests[i].dist;
        if(bilateral) d=-fabs(d); // to give the smallest key to the most violated
        dset.insert(DistanceToPoint(d,i)); // keep adding pairs in the sorted set, even if there are more than d_keepNSmallest
    }

    // the set is sorted, so the n-th first are the smallest keys (the most violated)
    std::size_t count=0;
    for(typename DistanceSet::iterator it=dset.begin(); it!=dset.end() && count<keepNSmallest ; it++,count++)
        output.push_back(it->second);

    o_output.endEdit();
}



} // namespace engine

} // namespace component

} // namespace sofa


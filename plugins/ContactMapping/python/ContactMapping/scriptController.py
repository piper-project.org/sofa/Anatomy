import Sofa

import SofaPython.Tools

class IgnorePenetratedVertex(Sofa.PythonScriptController) :
    def onLoaded(self,node):
        self.dofs = node.getObject("dofs")
        self.closest = node.getObject("ClosestPointEngine")

    def bwdInitGraph(self,node):
        indexList = []
        for index,dist in enumerate(self.dofs.position):
            if dist[0] < 0:
                indexList.append(index)
        self.closest.rejectROI = SofaPython.Tools.listToStr(indexList)

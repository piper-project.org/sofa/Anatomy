/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2016 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Plugins                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef BINDING_VECCLOSESTSTRUCTDATA_H
#define BINDING_VECCLOSESTSTRUCTDATA_H

#include <SofaPython/PythonMacros.h>
#include "../projectToMesh.h"

SP_DECLARE_CLASS_TYPE(VecClosestStructData)

typedef sofa::helper::ProjectToMesh<sofa::defaulttype::Vector3> ProjectToMesh;
typedef ProjectToMesh::ClosestStruct ClosestStruct;
typedef sofa::helper::vector<ClosestStruct> VecClosestStruct;


#endif // BINDING_VECCLOSESTSTRUCTDATA_H

/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2016 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Plugins                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#include "Binding_VecClosestStructData.h"
#include <SofaPython/Binding_Data.h>
#include <sofa/defaulttype/DataTypeInfo.h>

using namespace sofa::core::objectmodel;


SP_CLASS_ATTR_GET(VecClosestStructData,isPlane)(PyObject *self, void*)
{
    VecClosestStruct* obj=((PyPtr<VecClosestStruct>*)self)->object;
    if (!obj)
    {
        PyErr_BadArgument();
        return 0;
    }
    Data<VecClosestStruct>* data = down_cast<Data<VecClosestStruct> >( ((PyPtr<BaseData>*)self)->object );
    const VecClosestStruct& o = data->getValue();
    int nbRows = o.size();
    PyObject *rows = PyList_New(nbRows);
    for (int i=0; i<nbRows; i++)    PyList_SetItem(rows,i,PyInt_FromLong((long)(o[i].isPlane)));
    return rows;
}

SP_CLASS_ATTR_SET(VecClosestStructData,isPlane)(PyObject *self, PyObject * args, void*)
{
    Data<VecClosestStruct>* data = down_cast<Data<VecClosestStruct> >( ((PyPtr<BaseData>*)self)->object );
    if (PyList_Size(args)==0) { data->read(""); return true; }  // check list emptyness
    if(PyList_Check(args))
        if (!PyList_Check(PyList_GetItem(args,0))) // check it is a one-dimension array
        {
            VecClosestStruct* o = data->beginEdit();
            int nbRows = o->size();
            int newSize = PyList_Size(args);
            for (int i=0; i<nbRows && i<newSize; i++) // no resize allowed
            {
                PyObject *listElt = PyList_GetItem(args,i);
                (*o)[i].isPlane = (bool)PyFloat_AsDouble(listElt);
            }
            data->endEdit();
        }
    return 0;
}



SP_CLASS_ATTR_GET(VecClosestStructData,dist)(PyObject *self, void*)
{
    VecClosestStruct* obj=((PyPtr<VecClosestStruct>*)self)->object;
    if (!obj)
    {
        PyErr_BadArgument();
        return 0;
    }
    Data<VecClosestStruct>* data = down_cast<Data<VecClosestStruct> >( ((PyPtr<BaseData>*)self)->object );
    const VecClosestStruct& o = data->getValue();
    int nbRows = o.size();
    PyObject *rows = PyList_New(nbRows);
    for (int i=0; i<nbRows; i++)    PyList_SetItem(rows,i,PyFloat_FromDouble((double)(o[i].dist)));
    return rows;
}

SP_CLASS_ATTR_SET(VecClosestStructData,dist)(PyObject *self, PyObject * args, void*)
{
    Data<VecClosestStruct>* data = down_cast<Data<VecClosestStruct> >( ((PyPtr<BaseData>*)self)->object );
    if (PyList_Size(args)==0) { data->read(""); return true; }  // check list emptyness
    if(PyList_Check(args))
        if (!PyList_Check(PyList_GetItem(args,0))) // check it is a one-dimension array
        {
            VecClosestStruct* o = data->beginEdit();
            int nbRows = o->size();
            int newSize = PyList_Size(args);
            for (int i=0; i<nbRows && i<newSize; i++) // no resize allowed
            {
                PyObject *listElt = PyList_GetItem(args,i);
                (*o)[i].dist = (ProjectToMesh::Real)PyFloat_AsDouble(listElt);
            }
            data->endEdit();
        }
    return 0;
}


SP_CLASS_ATTR_GET(VecClosestStructData,u)(PyObject *self, void*)
{
    VecClosestStruct* obj=((PyPtr<VecClosestStruct>*)self)->object;
    if (!obj)
    {
        PyErr_BadArgument();
        return 0;
    }
    Data<VecClosestStruct>* data = down_cast<Data<VecClosestStruct> >( ((PyPtr<BaseData>*)self)->object );
    const VecClosestStruct& o = data->getValue();
    int nbRows = o.size();
    int rowWidth = ProjectToMesh::dim;
    PyObject *rows = PyList_New(nbRows);
    for (int i=0; i<nbRows; i++)
    {
        PyObject *row = PyList_New(rowWidth);
        for (int j=0; j<rowWidth; j++) PyList_SetItem(row,j,PyFloat_FromDouble((double)(o[i].u[j])));
        PyList_SetItem(rows,i,row);
    }
    return rows;
}

SP_CLASS_ATTR_SET(VecClosestStructData,u)(PyObject *self, PyObject * args, void*)
{
    Data<VecClosestStruct>* data = down_cast<Data<VecClosestStruct> >( ((PyPtr<BaseData>*)self)->object );
    if (PyList_Size(args)==0) { data->read(""); return true; }  // check list emptyness
    if(PyList_Check(args))
        if (PyList_Check(PyList_GetItem(args,0))) // check it is a two-dimension array
        {
            VecClosestStruct* o = data->beginEdit();
            int nbRows = o->size();
            int rowWidth = ProjectToMesh::dim;
            int newSize = PyList_Size(args);
            for (int i=0; i<nbRows && i<newSize; i++) // no resize allowed
            {
                PyObject *row = PyList_GetItem(args,i);
                for (int j=0; j<rowWidth && j<PyList_Size(row); j++)
                {
                    PyObject *listElt = PyList_GetItem(row,j);
                    (*o)[i].u[j] = (ProjectToMesh::Real)PyFloat_AsDouble(listElt);
                }
            }
            data->endEdit();
        }
    return 0;
}


SP_CLASS_ATTR_GET(VecClosestStructData,n)(PyObject *self, void*)
{
    VecClosestStruct* obj=((PyPtr<VecClosestStruct>*)self)->object;
    if (!obj)
    {
        PyErr_BadArgument();
        return 0;
    }
    Data<VecClosestStruct>* data = down_cast<Data<VecClosestStruct> >( ((PyPtr<BaseData>*)self)->object );
    const VecClosestStruct& o = data->getValue();
    int nbRows = o.size();
    int rowWidth = ProjectToMesh::dim;
    PyObject *rows = PyList_New(nbRows);
    for (int i=0; i<nbRows; i++)
    {
        PyObject *row = PyList_New(rowWidth);
        for (int j=0; j<rowWidth; j++) PyList_SetItem(row,j,PyFloat_FromDouble((double)(o[i].n[j])));
        PyList_SetItem(rows,i,row);
    }
    return rows;
}

SP_CLASS_ATTR_SET(VecClosestStructData,n)(PyObject *self, PyObject * args, void*)
{
    Data<VecClosestStruct>* data = down_cast<Data<VecClosestStruct> >( ((PyPtr<BaseData>*)self)->object );
    if (PyList_Size(args)==0) { data->read(""); return true; }  // check list emptyness
    if(PyList_Check(args))
        if (PyList_Check(PyList_GetItem(args,0))) // check it is a two-dimension array
        {
            VecClosestStruct* o = data->beginEdit();
            int nbRows = o->size();
            int rowWidth = ProjectToMesh::dim;
            int newSize = PyList_Size(args);
            for (int i=0; i<nbRows && i<newSize; i++) // no resize allowed
            {
                PyObject *row = PyList_GetItem(args,i);
                for (int j=0; j<rowWidth && j<PyList_Size(row); j++)
                {
                    PyObject *listElt = PyList_GetItem(row,j);
                    (*o)[i].n[j] = (ProjectToMesh::Real)PyFloat_AsDouble(listElt);
                }
            }
            data->endEdit();
        }
    return 0;
}


SP_CLASS_ATTR_GET(VecClosestStructData,indexWeights)(PyObject *self, void*)
{
    VecClosestStruct* obj=((PyPtr<VecClosestStruct>*)self)->object;
    if (!obj)
    {
        PyErr_BadArgument();
        return 0;
    }
    Data<VecClosestStruct>* data = down_cast<Data<VecClosestStruct> >( ((PyPtr<BaseData>*)self)->object );
    const VecClosestStruct& o = data->getValue();
    int nbRows = o.size();
    PyObject *rows = PyList_New(nbRows);
    for (int i=0; i<nbRows; i++)
    {
        int rowWidth = o[i].indexWeightSet.size();
        PyObject *row = PyList_New(rowWidth);
        unsigned j=0;
        for(std::set<ProjectToMesh::IndexToWeightPair>::iterator it=o[i].indexWeightSet.begin(); it!=o[i].indexWeightSet.end(); it++)
        {
            PyObject *elm = PyList_New(2);
            PyList_SetItem(elm,0,PyInt_FromLong((long)(it->first)));
            PyList_SetItem(elm,1,PyFloat_FromDouble((double)(it->second)));
            PyList_SetItem(row,j,elm);
            j++;
        }
        PyList_SetItem(rows,i,row);
    }
    return rows;
}

SP_CLASS_ATTR_SET(VecClosestStructData,indexWeights)(PyObject *self, PyObject * args, void*)
{
    Data<VecClosestStruct>* data = down_cast<Data<VecClosestStruct> >( ((PyPtr<BaseData>*)self)->object );
    if (PyList_Size(args)==0) { data->read(""); return true; }  // check list emptyness
    if(PyList_Check(args))
        if (PyList_Check(PyList_GetItem(args,0)))
            if (PyList_Check(PyList_GetItem(PyList_GetItem(args,0),0))) // check it is a three-dimension array
            {
                VecClosestStruct* o = data->beginEdit();
                int nbRows = o->size();
                int newSize = PyList_Size(args);
                for (int i=0; i<nbRows && i<newSize; i++) // no resize allowed
                {
                    PyObject *row = PyList_GetItem(args,i);
                    // reinit IndexToWeightPair with provided indices (and barycentric weight)
                    (*o)[i].indexWeightSet.clear();
                    int rowWidth = PyList_Size(row);
                    for (int j=0; j<rowWidth; j++)
                    {
                        PyObject *listElt = PyList_GetItem(row,j);
                        if(PyList_Size(listElt)==2)
                        {
                            PyObject *ind = PyList_GetItem(listElt,0);
                            PyObject *w = PyList_GetItem(listElt,1);
                            (*o)[i].indexWeightSet.insert( ProjectToMesh::IndexToWeightPair( (int)PyFloat_AsDouble(ind), (ProjectToMesh::Real)PyFloat_AsDouble(w) ) );
                        }
                    }
                }
                data->endEdit();
            }
    return 0;
}



SP_CLASS_METHODS_BEGIN(VecClosestStructData)
SP_CLASS_METHODS_END

SP_CLASS_ATTRS_BEGIN(VecClosestStructData)
SP_CLASS_ATTR(VecClosestStructData,isPlane)
SP_CLASS_ATTR(VecClosestStructData,dist)
SP_CLASS_ATTR(VecClosestStructData,u)
SP_CLASS_ATTR(VecClosestStructData,n)
SP_CLASS_ATTR(VecClosestStructData,indexWeights)
SP_CLASS_ATTRS_END

SP_CLASS_TYPE_PTR_ATTR(VecClosestStructData,Data<VecClosestStruct>,Data)


/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_MAPPING_ProjectionToMeshMultiMapping_INL
#define SOFA_COMPONENT_MAPPING_ProjectionToMeshMultiMapping_INL

#include "ProjectionToMeshMultiMapping.h"
#include <sofa/core/visual/VisualParams.h>
#include <sofa/helper/AdvancedTimer.h>

namespace sofa
{

namespace component
{

namespace mapping
{



template <class TIn, class TOut>
ProjectionToMeshMultiMapping< TIn, TOut >::ProjectionToMeshMultiMapping()
    : Inherit()
    , f_indexPairs(initData(&f_indexPairs, "indexPairs", "input list of closest point pairs (parent1 index, parent2 index)"))
    , d_validPairs(initData(&d_validPairs, "validPairs", "Set of valid index pairs"))
    , d_closests(initData(&d_closests,"closests","ClosestStruct computed by ProjectionToMeshEngine"))
    , d_handleBorder(initData(&d_handleBorder, false, "handleBorder", "enable stiff (possibly sticky) projection on border?"))
    , d_showObjectScale(initData(&d_showObjectScale, Real(0), "showObjectScale", "Scale for object display"))
    , d_color(initData(&d_color, defaulttype::Vec4f(1,1,0,1), "showColor", "Color for object display"))
    , fromModel1(NULL)
    , fromModel2(NULL)
    , toModel(NULL)
{
    // init jacobian
    m_jacobians.push_back(&m_jacobian1);
    m_jacobians.push_back(&m_jacobian2);
}



template <class TIn, class TOut>
void ProjectionToMeshMultiMapping< TIn, TOut >::resizeOut(size_t size)
{
    if((unsigned)toModel->getSize()!=size)
    {
//        sout<< "resizeOut="<<size<<sendl;

        toModel->resize(size);
        m_jacobian1.resizeBlocks(size, fromModel1->getSize());
        m_jacobian2.resizeBlocks(size, fromModel2->getSize());
    }
    else
    {
        m_jacobian1.compressedMatrix.setZero();
        m_jacobian2.compressedMatrix.setZero();
    }
}


template <class TIn, class TOut>
void ProjectionToMeshMultiMapping< TIn, TOut>::init()
{
    // check inputs
    if (this->getFrom().size() != 2) { serr << "must have two parents" << sendl; exit(EXIT_FAILURE); }
    if (this->getTo().size() != 1) { serr << "must have one child" << sendl; exit(EXIT_FAILURE); }
    if (d_closests.getValue().size() != f_indexPairs.getValue().size()) { serr << "have you computed 'closests' with a 'BaseProjectionToMeshEngine'?" << sendl; exit(EXIT_FAILURE); }

    fromModel1 = this->getFromModels()[0];
    fromModel2 = this->getFromModels()[1];
    toModel = this->getToModels()[0];

    reinit();
    Inherit::init();
}


template <class TIn, class TOut>
void ProjectionToMeshMultiMapping< TIn, TOut>::reinit()
{
    toModel->resize(0);
    resizeOut(d_validPairs.getValue().empty() ? f_indexPairs.getValue().size() : d_validPairs.getValue().size());

    Inherit::reinit();
}



template <class TIn, class TOut>
void ProjectionToMeshMultiMapping< TIn, TOut >::apply(const core::MechanicalParams*, const helper::vector<OutDataVecCoord*>& dataVecOutPos, const helper::vector<const InDataVecCoord*>& dataVecInPos)
{
    helper::ScopedAdvancedTimer advancedTimer("ProjectionToMeshMultiMapping");

    const InVecCoord& inPos1 = dataVecInPos[0]->getValue();
    const InVecCoord& inPos2 = dataVecInPos[1]->getValue();
    const helper::vector<defaulttype::Vec2i>& indexPairs = f_indexPairs.getValue();
    const helper::vector<ClosestStruct>& closests = d_closests.getValue();
    bool handleBorder = d_handleBorder.getValue();



    helper::IndexPairIterator indexPairIterator( indexPairs.size(), d_validPairs.getValue(), d_validPairs.isSet() );


    resizeOut(indexPairIterator.size());
    if(!indexPairIterator.size()) return;

    typename Jacobian::CompressedMatrix& J1 = m_jacobian1.compressedMatrix;
    typename Jacobian::CompressedMatrix& J2 = m_jacobian2.compressedMatrix;

    OutVecCoord& outPos = *dataVecOutPos[0]->beginWriteOnly();
    typename Jacobian::Block J;

    for ( ; indexPairIterator.count()<indexPairIterator.size() ; ++indexPairIterator )
    {
        const size_t i = indexPairIterator.get();
        const int iP = indexPairs[i][0];
        outPos[i] = inPos1[iP] - closests[i].u*closests[i].dist;

        if( this->maskTo[0]->isActivated() && !this->maskTo[0]->getEntry(indexPairIterator.count()) ) continue;

        // compute jacobians
        if(handleBorder)   // select jacobian type (point/line/plane) based on the true projection to allow stiff projection on border
        {
            if(closests[i].indexWeightSet.size()==1) // projection to a point -> invariant when moving in any direction
                J.identity();
            else if(closests[i].indexWeightSet.size()==2)   // projection to a edge -> invariant when moving in the plane orthogonal to the edge
            {
                //get edge direction
                typename std::set<IndexToWeightPair>::iterator iwit=closests[i].indexWeightSet.begin();
                InCoord v = inPos2[iwit->first];
                iwit++; v -= inPos2[iwit->first];
                v.normalize();
                J.identity();
                J-= dyad(v,v);
            }
            else// projection to a triangle -> invariant when moving in normal direction
                J = dyad(closests[i].n,closests[i].n);
        }
        else // use the jacobian type (line/plane) of the primitive
        {
            if(closests[i].isPlane)   // projection to a triangle -> invariant when moving in normal
                J = dyad(closests[i].n,closests[i].n);
            else // projection to a edge -> invariant when moving in the plane orthogonal to the edge
            {
                J.identity();
                J -= dyad(closests[i].n,closests[i].n);
            }
        }


        for(unsigned j=0; j<Nout; j++)
        {
            unsigned row = i*Nout+j;
            J1.startVec( row );
            for(unsigned k=0; k<Nin; k++ )
            {
                if( j == k ) J1.insertBack( row, i*Nin+k ) =  1-J[j][k];
                else J1.insertBack( row, i*Nin+k ) =  -J[j][k];
            }

            J2.startVec( row );
            for(typename std::set<IndexToWeightPair>::iterator iwit=closests[i].indexWeightSet.begin(); iwit!=closests[i].indexWeightSet.end(); iwit++)
                for(unsigned k=0; k<Nin; k++ )
                    J2.insertBack( row, iwit->first*Nin+k ) = J[j][k]*iwit->second;
        }
    }

    dataVecOutPos[0]->endEdit();

    J1.finalize();
    J2.finalize();
}


template <class TIn, class TOut>
void ProjectionToMeshMultiMapping< TIn, TOut >::draw(const core::visual::VisualParams* vparams)
{
    if( !vparams->displayFlags().getShowMechanicalMappings() ) return;
    const Real& showObjectScale =  d_showObjectScale.getValue();
    if( showObjectScale == 0 ) return;

    const OutVecCoord& outPos = this->toModel->read(core::ConstVecCoordId::position())->getValue();

    helper::vector< defaulttype::Vector3 > points;
    for(size_t i=0; i<outPos.size(); i++ ) points.push_back( defaulttype::Vector3( Out::getCPos(outPos[i]) ) );
    vparams->drawTool()->drawSpheres( points, showObjectScale, d_color.getValue() );
}



template <class TIn, class TOut>
void ProjectionToMeshMultiMapping<TIn, TOut>::updateForceMask()
{
    const helper::vector<ClosestStruct>& closests = d_closests.getValue();
    const helper::vector<defaulttype::Vec2i>& indexPairs = f_indexPairs.getValue();
    const std::list<size_t>& validPairs = d_validPairs.getValue();

    for( helper::IndexPairIterator indexPairIterator( indexPairs.size(), validPairs, d_validPairs.isSet() ) ; indexPairIterator.count()<indexPairIterator.size() ; ++indexPairIterator )
    {
        if( this->maskTo[0]->getEntry(indexPairIterator.count()) )
        {
            const size_t i = indexPairIterator.get();
            const int iP = indexPairs[i][0];
            this->maskFrom[0]->insertEntry( iP );
            for(typename std::set<IndexToWeightPair>::iterator iwit=closests[i].indexWeightSet.begin(); iwit!=closests[i].indexWeightSet.end(); iwit++)
                this->maskFrom[1]->insertEntry(iwit->first);
        }
    }
}



} // namespace mapping

} // namespace component

} // namespace sofa

#endif // SOFA_COMPONENT_MAPPING_ProjectionToMeshMultiMapping_INL


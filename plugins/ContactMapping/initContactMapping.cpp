/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Plugins                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#include "initContactMapping.h"

#ifdef SOFA_HAVE_SOFAPYTHON
    #include <SofaPython/PythonFactory.h>
    #include "python/Binding_VecClosestStructData.h"
#endif

namespace sofa
{

namespace component
{

//Here are just several convenient functions to help user to know what contains the plugin

extern "C" {
    SOFA_ContactMapping_API void initExternalModule();
    SOFA_ContactMapping_API const char* getModuleName();
    SOFA_ContactMapping_API const char* getModuleVersion();
    SOFA_ContactMapping_API const char* getModuleLicense();
    SOFA_ContactMapping_API const char* getModuleDescription();
    SOFA_ContactMapping_API const char* getModuleComponentList();
}

void initExternalModule()
{
    static bool first = true;
    if (first)
    {
        first = false;

#ifdef SOFA_HAVE_SOFAPYTHON
        if( PythonFactory::s_sofaPythonModule ) // add the module only if the Sofa module exists (SofaPython is loaded)
        {
            // adding new bindings for Data<Vector<ClosestStruct>>
            SP_ADD_CLASS_IN_FACTORY(VecClosestStructData,sofa::core::objectmodel::Data<VecClosestStruct>)
        }
#endif
    }
}



const char* getModuleName()
{
    return "ContactMapping";
}

const char* getModuleVersion()
{
    return "0.1";
}

const char* getModuleLicense()
{
    return "private";
}


const char* getModuleDescription()
{
    return "Contact mapping";
}

const char* getModuleComponentList()
{
    return "";
}



}

}

SOFA_LINK_CLASS(DistanceToMeshMapping)
SOFA_LINK_CLASS(DistanceToMeshMultiMapping)
SOFA_LINK_CLASS(ClosestPointEngine)

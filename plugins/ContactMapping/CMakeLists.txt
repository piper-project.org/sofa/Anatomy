cmake_minimum_required(VERSION 2.8.12)
project(ContactMapping)

set(CONTACTMAPPING_MAJOR_VERSION 0)
set(CONTACTMAPPING_MINOR_VERSION 1)
set(CONTACTMAPPING_VERSION ${CONTACTMAPPING_MAJOR_VERSION}.${CONTACTMAPPING_MINOR_VERSION})

set(HEADER_FILES
    initContactMapping.h
    DistanceToMeshMapping.h
    DistanceToMeshMapping.inl
    DistanceToMeshMultiMapping.h
    DistanceToMeshMultiMapping.inl
    ProjectionToMeshMapping.h
    ProjectionToMeshMapping.inl
    ProjectionToMeshMultiMapping.h
    ProjectionToMeshMultiMapping.inl
    PenetrationForceField.h
    PenetrationForceField.inl
    ClosestPointEngine.h
    ClosestPointEngine.inl
    ProjectionToMeshEngine.h
    ProjectionToMeshEngine.inl
    RayMeshIntersectionEngine.h
    PreciseProjectionToMeshEngine.h
    TargetFromProjectionEngine.h
    TargetInterpolationEngine.h
    ClosestPointInterpolationEngine.h
    projectToMesh.h
    ClosestRejectROIEngine.h
    ClosestRejectROIEngine.inl
    ClosestKeepNSmallestEngine.h
    ClosestKeepNSmallestEngine.inl
    ClosestKeepPositiveEngine.h
    ClosestKeepPositiveEngine.inl
    ClosestKeepNegativeEngine.h
    ClosestKeepNegativeEngine.inl
    ValidPairEngine.h
)

set(SOURCE_FILES
    initContactMapping.cpp
    DistanceToMeshMapping.cpp
    DistanceToMeshMultiMapping.cpp
    ProjectionToMeshMapping.cpp
    ProjectionToMeshMultiMapping.cpp
    PenetrationForceField.cpp    
    ClosestPointEngine.cpp
    ProjectionToMeshEngine.cpp
    RayMeshIntersectionEngine.cpp
    PreciseProjectionToMeshEngine.cpp
    TargetFromProjectionEngine.cpp
    TargetInterpolationEngine.cpp
    ClosestPointInterpolationEngine.cpp
    projectToMesh.cpp
    ClosestRejectROIEngine.cpp
    ClosestKeepNSmallestEngine.cpp
    ClosestKeepPositiveEngine.cpp
    ClosestKeepNegativeEngine.cpp
    ValidPairEngine.cpp
)

set(PYTHON_FILES
    python/ContactMapping/__init__.py
    python/ContactMapping/StructuralAPI.py
    python/ContactMapping/sml.py
    python/ContactMapping/scriptController.py
)

set(README_FILES
    ContactMapping.txt
)


find_package(SofaPython QUIET)
if( SofaPython_FOUND )
    list(APPEND SOURCE_FILES python/Binding_VecClosestStructData.cpp)
    list(APPEND HEADER_FILES python/Binding_VecClosestStructData.h)
endif()

find_package(SofaMisc REQUIRED)

if( SofaPython_FOUND )
    # Config files and install rules for pythons scripts
    sofa_set_python_directory(${PROJECT_NAME} "python")
endif()

add_library(${PROJECT_NAME} SHARED ${HEADER_FILES} ${SOURCE_FILES} ${README_FILES} ${PYTHON_FILES})


set(CONTACTMAPPING_COMPILER_FLAGS "-DSOFA_BUILD_CONTACTMAPPING")

if( SofaPython_FOUND )
    target_link_libraries(${PROJECT_NAME} SofaPython)
    set(CONTACTMAPPING_COMPILER_FLAGS "${CONTACTMAPPING_COMPILER_FLAGS} -DSOFA_HAVE_SOFAPYTHON")
endif()

find_package(image QUIET)
if(image_FOUND)
    target_link_libraries(${PROJECT_NAME} image)
    set(CONTACTMAPPING_COMPILER_FLAGS "${CONTACTMAPPING_COMPILER_FLAGS} -DSOFA_HAVE_IMAGE")
else()
    message(STATUS "ContactMapping: could not find image")
endif()


target_include_directories(${PROJECT_NAME} PUBLIC "$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>")
target_include_directories(${PROJECT_NAME} PUBLIC "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/../>")
target_include_directories(${PROJECT_NAME} PUBLIC "$<INSTALL_INTERFACE:include>")
set_target_properties(${PROJECT_NAME} PROPERTIES VERSION ${CONTACTMAPPING_VERSION})
set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_FLAGS "${CONTACTMAPPING_COMPILER_FLAGS}")

target_link_libraries(${PROJECT_NAME} SofaEigen2Solver)

add_definitions("-DSOFA_BUILD_ContactMapping")

if(SOFA_BUILD_TESTS)
    find_package(SofaTest QUIET)
    if(SofaTest_FOUND)
        add_subdirectory(ContactMapping_test)
    endif()
endif()

## Install rules for the library; CMake package configurations files
sofa_create_package(${PROJECT_NAME} ${CONTACTMAPPING_VERSION} ${PROJECT_NAME} ${PROJECT_NAME})

## Install rules for the resources
install(DIRECTORY examples/ DESTINATION share/sofa/plugins/${PROJECT_NAME})


/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_MAPPING_ValidPairEngine_H
#define SOFA_COMPONENT_MAPPING_ValidPairEngine_H

#include "initContactMapping.h"
#include <sofa/core/DataEngine.h>
#include <sofa/defaulttype/VecTypes.h>
#include <sofa/core/topology/BaseMeshTopology.h>
#include "projectToMesh.h"

namespace sofa
{

namespace component
{

namespace engine
{

/**
 * Output valid pairs from a list of pairs and a set of valid pair indices.
 *
 * (only?) Useful to debug.
 *
 * @author Matthieu Nesme
 * @date 2017
 */
class ValidPairEngine : public core::DataEngine
{
public:
    SOFA_CLASS(ValidPairEngine, core::DataEngine);

protected:

    ValidPairEngine()
      : Inherit1()
      , i_input (initData(&i_input, "input", "input list of closest point pairs (parent1 index, parent2 index)"))
      , d_validPairs(initData(&d_validPairs, "validPairs", "Set of valid index pairs"))
      , o_output(initData(&o_output,"output","list of valid pairs"))
  {
      this->addAlias(&i_input, "indexPairs");
  }

    virtual ~ValidPairEngine() {}

public:

    virtual void init()
    {
        addInput(&i_input);
        addInput(&d_validPairs);

        addOutput(&o_output);

        reinit();
        Inherit1::init();
    }



    virtual void reinit()
    {
        Inherit1::reinit();

        o_output.setDirtyValue();
    }

    virtual void update()
    {
        const helper::vector<defaulttype::Vec2i>& indexPairs = i_input.getValue();
        const std::list<size_t>& validPairs = d_validPairs.getValue();

        cleanDirty();

        helper::vector<defaulttype::Vec2i>& output = *o_output.beginWriteOnly();
        output.clear();

        for ( helper::IndexPairIterator indexPairIterator( indexPairs.size(), validPairs, d_validPairs.isSet() ) ;
              indexPairIterator.count()<indexPairIterator.size() ;
              ++indexPairIterator )
        {
            const size_t i = indexPairIterator.get();
             output.push_back( indexPairs[i] );
        }

        o_output.endEdit();
    }

    Data<helper::vector<defaulttype::Vec2i>> i_input;    ///< input pairs of correspondences
    Data< std::list<size_t> > d_validPairs; ///< optional list of valid index pairs
    Data<helper::vector<defaulttype::Vec2i>> o_output; ///< list of kept indices

};

} // namespace engine

} // namespace component

} // namespace sofa

#endif //SOFA_COMPONENT_MAPPING_ValidPairEngine_H
